package com.cs.chef;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class ChildsViewHolder extends ChildViewHolder {

    TextView filter_child_name;
    boolean filter = true;
    Context context;

    public ChildsViewHolder(final View itemView, Context context) {
        super(itemView);
        this.context = context;
        filter_child_name = (TextView) itemView
                .findViewById(R.id.filter_child_name);
    }


    public void setSubitemname(String name) {

        filter_child_name.setText(name);
        filter_child_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (filter){
                    filter = false;
                    filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                    filter_child_name.setTextColor(context.getResources().getColor(R.color.white));
                } else {
                    filter = true;
                    filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.chlid_unselected));
                    filter_child_name.setTextColor(context.getResources().getColor(R.color.blue));
                }



            }
        });
    }


}
