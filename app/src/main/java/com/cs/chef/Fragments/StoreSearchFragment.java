package com.cs.chef.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Adapter.StoresAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.ActiveOrders;
import com.cs.chef.Models.Banners;
import com.cs.chef.Models.Brands;
import com.cs.chef.Models.Filter;
import com.cs.chef.Models.StorePaginationList;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.GPSTracker;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.chef.Constants.currentLatitude;
import static com.cs.chef.Constants.currentLongitude;

public class StoreSearchFragment extends Fragment {

    View rootView;
    EditText inputsearch;
    public static String searchTex;
    int catSearchId;
    private RecyclerView brandsListView, storeslistview;
    String inputStr;
    TextView clearbtn;
    RelativeLayout clear_layout;
    private StoresAdapter mStoreAdapter;
    private RecyclerView storesListView, bannerListView;
    int pos = 0;
    ArrayList<StorePaginationList> storePaginationLists = new ArrayList<>();
    public static ArrayList<StoresList.BannerResult> bannerList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> storesList = new ArrayList<>();
    public static ArrayList<StoresList.FilterCategory> filtercatList = new ArrayList<>();
    //    public static ArrayList<StoresList.StoresDetails> itsnewList = new ArrayList<>();
//    public static ArrayList<StoresList.StoresDetails> promotedList = new ArrayList<>();
//    public static ArrayList<StoresList.StoresDetails> threekmList = new ArrayList<>();
    public static ArrayList<StoresList.StoreCategories> somthingList = new ArrayList<>();
    ArrayList<ActiveOrders.Data> data = new ArrayList<>();
    public static boolean filter = false;
    public static int filterdistance = 40, storedistance;

    public static ArrayList<Filter> filterslist = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> filter_main_list = new ArrayList<>();
    public static ArrayList<Brands> brandslist = new ArrayList<>();
    public static ArrayList<Banners> bannerslist = new ArrayList<>();

    ArrayList<Integer> seletedbrands = new ArrayList<>();
    ArrayList<String> seletedbanners = new ArrayList<>();
    ArrayList<Integer> filterseletedbrands = new ArrayList<>();
    GPSTracker gps;
    String TAG = "TAGF";
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    private Context context;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    SharedPreferences LocationPrefs;
    SharedPreferences.Editor LocationPrefsEditor;
    String location_status;
    String locality_name, latitute, longitude, str_search_text = "";
    EditText search_text;
    ImageView search_btn;
    String language;
    TextView mlanguage;

    int page_no = 1, page_size = 10;

    int lastVisibleItem, no_of_rows, lastvisibleposition = 8;


    public static int REQUEST_CODE_AUTOCOMPLETE = 1;

    public static boolean testing = true;

    SharedPreferences LocationPrefe;
    String LocationStatus;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.search_fragment, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.search_fragment_arabic, container, false);
        }

//        brandsListView = (RecyclerView) rootView.findViewById(R.id.);
        clear_layout = (RelativeLayout) rootView.findViewById(R.id.clear_layout);
        storeslistview = (RecyclerView) rootView.findViewById(R.id.list_item);
        inputsearch = (EditText) rootView.findViewById(R.id.search);

        inputsearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        filterslist.clear();
        brandslist.clear();
        bannerslist.clear();
        seletedbanners.clear();
        storePaginationLists.clear();

        inputsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    str_search_text = inputsearch.getText().toString();
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (str_search_text.length() > 3) {
                            new GetStoresApi().execute();

                        }
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        storeslistview.setLayoutManager(linearLayoutManager);

//        storeslistview.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                lastVisibleItem = linearLayoutManager
//                        .findLastVisibleItemPosition();
//                Log.i("TAG", "lastvisibleitem: " + lastVisibleItem);
//
//                if (storePaginationLists.size() > 0) {
////                            for (int i = 0; i < filterslist.size(); i++) {
//                    Log.i("TAG", "lastvisibleposition: " + lastvisibleposition);
//                    Log.i("TAG", "no_of_rows: " + no_of_rows);
////                                Log.i("TAG", "filter.size: " + (filterslist.get(i).getBrands().size() - 1));
//                    if ((lastvisibleposition < lastVisibleItem) && (lastVisibleItem == (storePaginationLists.size() - 1))) {
//                        Log.i("TAG", "onScrolled: ");
//                        if ((page_no * page_size) <= no_of_rows) {
////                        if () {
//
////                        page_size = page_size + 10;
//                            page_no = page_no + 1;
//                            Log.i("TAG", "page_no: " + page_no);
//                            new GetStoresApi().execute();
//                            lastvisibleposition = lastVisibleItem;
//
//
////                        }
//                        }
//                    }
////                            }
//                }
//
//            }
//        });

        inputsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                filterslist.clear();
                storePaginationLists.clear();

                str_search_text = inputsearch.getText().toString();

                if (str_search_text.equalsIgnoreCase("")) {

                    clear_layout.setVisibility(View.GONE);
                    storeslistview.setVisibility(View.GONE);

                } else {

                    clear_layout.setVisibility(View.VISIBLE);

                }
                String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    if (str_search_text.length() > 3) {
                        new GetStoresApi().execute();

                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        clear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "onClick: ");
                inputsearch.setText("");
//                brandsListView.setVisibility(View.VISIBLE);
                storeslistview.setVisibility(View.GONE);
            }
        });

        return rootView;
    }

    private class GetStoresApi extends AsyncTask<String, Integer, String> {

        String inputStr;
//        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            filterslist.clear();
            storePaginationLists.clear();
            inputStr = prepareGetStoresJSON();
//            filterslist.clear();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.progress_bar_alert;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(false);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the progressDialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth * 0.45;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {

                        StoresList stores = response.body();
                        if (stores.getStatus()) {

                            storesList = stores.getData().getStoresDetails();
                            bannerList = stores.getData().getBannerResult();
                            somthingList = stores.getData().getStoreCategories();
                            filtercatList = stores.getData().getFilterCategory();


                            brandslist.clear();
//                            filterslist.clear();
                            bannerslist.clear();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandId())) {
                                    seletedbrands.add(storesList.get(i).getBrandId());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandId());
                                    brands.setBradNameEn(storesList.get(i).getBrandName_En());
                                    brands.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                    brands.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                    brands.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                    brands.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                            fillterstore.add(storesList.get(j));
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            seletedbanners.clear();
                            for (int i = 0; i < bannerList.size(); i++) {
                                if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                                    seletedbanners.add(bannerList.get(i).getBannerName_En());
                                    Banners banners = new Banners();
                                    banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                                    banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                                    banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                                    ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < bannerList.size(); j++) {
                                        if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                            fillterstore.add(bannerList.get(j));
                                            for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                                ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                                for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                                    if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                                    bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                                    }

                                                }

                                                banners.setBannerList(bannerlist);

                                            }
                                        }
                                    }


                                    banners.setBanners(fillterstore);
                                    bannerslist.add(banners);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            for (int l = 0; l < bannerslist.size(); l++) {
                                if (bannerslist.get(l).getBannerList() == null || bannerslist.get(l).getBannerList().size() == 0) {

                                } else {

                                    Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);

                                }
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerResult());

                            for (int p = 0; p < storesList.size(); p++) {

                                StorePaginationList storePaginationList = new StorePaginationList();

                                storePaginationList.setId(storesList.get(p).getId());
                                storePaginationList.setWeekNo(storesList.get(p).getWeekNo());
                                storePaginationList.setBranchId(storesList.get(p).getBranchId());
                                storePaginationList.setBranchName_En(storesList.get(p).getBranchName_En());
                                storePaginationList.setBranchName_Ar(storesList.get(p).getBranchName_Ar());
                                storePaginationList.setBrandId(storesList.get(p).getBrandId());
                                storePaginationList.setBrandName_En(storesList.get(p).getBrandName_En());
                                storePaginationList.setBrandName_Ar(storesList.get(p).getBrandName_Ar());
                                storePaginationList.setBranchDescription_En(storesList.get(p).getBranchDescription_En());
                                storePaginationList.setBranchDescription_Ar(storesList.get(p).getBranchDescription_Ar());
                                storePaginationList.setStarDateTime(storesList.get(p).getStarDateTime());
                                storePaginationList.setEndDateTime(storesList.get(p).getEndDateTime());
                                storePaginationList.setCurrentDateTime(storesList.get(p).getCurrentDateTime());
                                storePaginationList.setBranchCode(storesList.get(p).getBranchCode());
                                storePaginationList.setAddress(storesList.get(p).getAddress());
                                storePaginationList.setTelephoneNo(storesList.get(p).getTelephoneNo());
                                storePaginationList.setMobileNo(storesList.get(p).getMobileNo());
                                storePaginationList.setEmailId(storesList.get(p).getEmailId());
                                storePaginationList.setDeliveryDistance(storesList.get(p).getDeliveryDistance());
                                storePaginationList.setTakeAwayDistance(storesList.get(p).getTakeAwayDistance());
                                storePaginationList.setCurrentDate(storesList.get(p).getCurrentDate());
                                storePaginationList.setStoreStatus(storesList.get(p).getStoreStatus());
                                storePaginationList.setStoreLogo_En(storesList.get(p).getStoreLogo_En());
                                storePaginationList.setStoreImage_En(storesList.get(p).getStoreImage_En());
                                storePaginationList.setDistance(storesList.get(p).getDistance());
                                storePaginationList.setRating(storesList.get(p).getRating());
                                storePaginationList.setLatitude(storesList.get(p).getLatitude());
                                storePaginationList.setLongitude(storesList.get(p).getLongitude());
                                storePaginationList.setMinOrderCharges(storesList.get(p).getMinOrderCharges());
                                storePaginationList.setDeliveryCharges(storesList.get(p).getDeliveryCharges());
                                storePaginationList.setAvgPreparationTime(storesList.get(p).getAvgPreparationTime());
                                storePaginationList.setServing(storesList.get(p).getServing());
                                storePaginationList.setFutureOrderDay(storesList.get(p).getFutureOrderDay());
                                storePaginationList.setIsPromoted(storesList.get(p).getIsPromoted());
                                storePaginationList.setPopular(storesList.get(p).getPopular());
                                storePaginationList.setIsNew(storesList.get(p).getIsNew());
                                storePaginationList.setPromotedBranch_En(storesList.get(p).getPromotedBranch_En());
                                storePaginationList.setPromotedBranch_Ar(storesList.get(p).getPromotedBranch_Ar());
                                storePaginationList.setRatingCount(storesList.get(p).getRatingCount());
                                storePaginationList.setOrderTypes(storesList.get(p).getOrderTypes());
                                storePaginationList.setStoreType(storesList.get(p).getStoreType());
                                storePaginationList.setStoreType_Ar(storesList.get(p).getStoreType_Ar());
                                storePaginationList.setPaymentType(storesList.get(p).getPaymentType());
                                storePaginationList.setDiscountAmt(storesList.get(p).getDiscountAmt());
                                storePaginationList.setPriceCategory(storesList.get(p).getPriceCategory());
                                storePaginationList.setTotalRows(storesList.get(p).getTotalRows());

                                storePaginationLists.add(storePaginationList);

                            }

                            Log.i("TAG", "storepagination: " + storePaginationLists.size());

                            filterseletedbrands.clear();
                            for (int i = 0; i < storesList.size(); i++) {

//                                    if (storesList.get(i).getDiscountamt() > 0) {


                                filterseletedbrands.add(storesList.get(i).getBrandId());
                                Filter filter = new Filter();

                                filter.setBrandId(storesList.get(i).getBrandId());
                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                for (int j = 0; j < storesList.size(); j++) {
//                                    if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                    fillterstore.add(storesList.get(j));

//                                    }
                                }
                                filter.setBrands(fillterstore);
                                filterslist.add(filter);


                            }

                            Filter filter = new Filter();
//                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                            for (int m = 0; m < storePaginationLists.size(); m++){
//
//                                StoresList.StoresDetails storePaginationList = new StoresList.StoresDetails();
//
//                                storePaginationList.setId(storePaginationLists.get(m).getId());
//                                storePaginationList.setWeekNo(storePaginationLists.get(m).getWeekNo());
//                                storePaginationList.setBranchId(storePaginationLists.get(m).getBranchId());
//                                storePaginationList.setBranchName_En(storePaginationLists.get(m).getBranchName_En());
//                                storePaginationList.setBranchName_Ar(storePaginationLists.get(m).getBranchName_Ar());
//                                storePaginationList.setBrandId(storePaginationLists.get(m).getBrandId());
//                                storePaginationList.setBrandName_En(storePaginationLists.get(m).getBrandName_En());
//                                storePaginationList.setBrandName_Ar(storePaginationLists.get(m).getBrandName_Ar());
//                                storePaginationList.setBranchDescription_En(storePaginationLists.get(m).getBranchDescription_En());
//                                storePaginationList.setBranchDescription_Ar(storePaginationLists.get(m).getBranchDescription_Ar());
//                                storePaginationList.setStarDateTime(storePaginationLists.get(m).getStarDateTime());
//                                storePaginationList.setEndDateTime(storePaginationLists.get(m).getEndDateTime());
//                                storePaginationList.setCurrentDateTime(storePaginationLists.get(m).getCurrentDateTime());
//                                storePaginationList.setBranchCode(storePaginationLists.get(m).getBranchCode());
//                                storePaginationList.setAddress(storePaginationLists.get(m).getAddress());
//                                storePaginationList.setTelephoneNo(storePaginationLists.get(m).getTelephoneNo());
//                                storePaginationList.setMobileNo(storePaginationLists.get(m).getMobileNo());
//                                storePaginationList.setEmailId(storePaginationLists.get(m).getEmailId());
//                                storePaginationList.setDeliveryDistance(storePaginationLists.get(m).getDeliveryDistance());
//                                storePaginationList.setTakeAwayDistance(storePaginationLists.get(m).getTakeAwayDistance());
//                                storePaginationList.setCurrentDate(storePaginationLists.get(m).getCurrentDate());
//                                storePaginationList.setStoreStatus(storePaginationLists.get(m).getStoreStatus());
//                                storePaginationList.setStoreLogo_En(storePaginationLists.get(m).getStoreLogo_En());
//                                storePaginationList.setStoreImage_En(storePaginationLists.get(m).getStoreImage_En());
//                                storePaginationList.setDistance(storePaginationLists.get(m).getDistance());
//                                storePaginationList.setRating(storePaginationLists.get(m).getRating());
//                                storePaginationList.setLatitude(storePaginationLists.get(m).getLatitude());
//                                storePaginationList.setLongitude(storePaginationLists.get(m).getLongitude());
//                                storePaginationList.setMinOrderCharges(storePaginationLists.get(m).getMinOrderCharges());
//                                storePaginationList.setDeliveryCharges(storePaginationLists.get(m).getDeliveryCharges());
//                                storePaginationList.setAvgPreparationTime(storePaginationLists.get(m).getAvgPreparationTime());
//                                storePaginationList.setServing(storePaginationLists.get(m).getServing());
//                                storePaginationList.setFutureOrderDay(storePaginationLists.get(m).getFutureOrderDay());
//                                storePaginationList.setIsPromoted(storePaginationLists.get(m).getIsPromoted());
//                                storePaginationList.setPopular(storePaginationLists.get(m).getPopular());
//                                storePaginationList.setIsNew(storePaginationLists.get(m).getIsNew());
//                                storePaginationList.setPromotedBranch_En(storePaginationLists.get(m).getPromotedBranch_En());
//                                storePaginationList.setPromotedBranch_Ar(storePaginationLists.get(m).getPromotedBranch_Ar());
//                                storePaginationList.setRatingCount(storePaginationLists.get(m).getRatingCount());
//                                storePaginationList.setOrderTypes(storePaginationLists.get(m).getOrderTypes());
//                                storePaginationList.setStoreType(storePaginationLists.get(m).getStoreType());
//                                storePaginationList.setStoreType_Ar(storePaginationLists.get(m).getStoreType_Ar());
//                                storePaginationList.setPaymentType(storePaginationLists.get(m).getPaymentType());
//                                storePaginationList.setDiscountAmt(storePaginationLists.get(m).getDiscountAmt());
//                                storePaginationList.setPriceCategory(storePaginationLists.get(m).getPriceCategory());
//                                storePaginationList.setTotalRows(storePaginationLists.get(m).getTotalRows());
//
//                                fillterstore.add(storePaginationList);
//
//                            }

//                            filter.setBrands(fillterstore);
//                            filterslist.add(filter);

                            for (int l = 0; l < filterslist.size(); l++) {
                                Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                                no_of_rows = filterslist.get(l).getBrands().get(0).getTotalRows();
                            }

                            Log.i(TAG, "brands_size: " + brandslist.size());
                            Log.i(TAG, "filter_size " + filterslist.size());

                            if (getActivity() != null) {
                                for (int i = 0; i < filterslist.size(); i++) {
                                    mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                                    storeslistview.setAdapter(mStoreAdapter);
                                }
//                                bannersViewpage.setAdapter(mBanners);
                                storeslistview.setVisibility(View.VISIBLE);
//                                bannersViewpage.setVisibility(View.VISIBLE);
                            }


                            for (int i = 0; i < storesList.size(); i++) {

                            }
                        } else {
                            if (getActivity() != null) {
//                                if (language.equalsIgnoreCase("En")) {
//                                    String failureResponse = stores.getMessage();
//                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
//                                            getResources().getString(R.string.ok), getActivity());
//                                } else {
//                                    String failureResponse = stores.getMessageAr();
//                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
//                                            getResources().getString(R.string.ok_ar), getActivity());
//                                }
                                if (language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getActivity(), stores.getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), stores.getMessageAr(), Toast.LENGTH_SHORT).show();
                                }
                                storeslistview.setVisibility(View.GONE);
//                                bannersViewpage.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

//                    if (loaderDialog != null) {
//                        loaderDialog.dismiss();
//                    }
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    Log.i("TAG", "onFailure: " + t);
                    if (getActivity() != null) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    Log.i("TAG", "onFailure: " + t);

//                    if (loaderDialog != null) {
//                        loaderDialog.dismiss();
//                    }
                }
            });
            return null;
        }


    }

    private String prepareGetStoresJSON() {

        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
            parentObj.put("BranchId", "");
            parentObj.put("SearchText", str_search_text);
//            parentObj.put("pageNumber", page_no);
//            parentObj.put("pageSize", page_size);
            parentObj.put("VendorType", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

}
