package com.cs.chef.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.crashlytics.android.Crashlytics;
import com.cs.chef.Activities.ChangeAddressActivity;
import com.cs.chef.Activities.FilterActivity;
import com.cs.chef.Activities.MainActivity;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Activities.PromotedViewallScreenActivity;
import com.cs.chef.Activities.SignInActivity;
import com.cs.chef.Activities.SplashScreenActivity;

import com.cs.chef.Adapter.FilterCatAdapter;
import com.cs.chef.Adapter.FilterCatAdapter1;
import com.cs.chef.Adapter.StoresAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.ActiveOrders;
import com.cs.chef.Models.Banners;
import com.cs.chef.Models.Brands;
import com.cs.chef.Models.ChangeLanguageList;
import com.cs.chef.Models.Filter;
import com.cs.chef.Models.SearchFilter;
import com.cs.chef.Models.StoreMenuextenditems;
import com.cs.chef.Models.StorePaginationList;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.GPSTracker;
//import com.cs.chef.Utils.NetworkUtil;
import com.cs.chef.Utils.OnBackPressed;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.pedromassango.doubleclick.DoubleClick;
import com.pedromassango.doubleclick.DoubleClickListener;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cc.cloudist.acplibrary.ACProgressFlower;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.chef.Constants.address;
import static com.cs.chef.Constants.currentLatitude;
import static com.cs.chef.Constants.currentLongitude;
import static com.cs.chef.Constants.isCurrentLocationSelected;

//import com.cs.chef.Dialogs.SearchListDialog;

public class HomeScreenFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback, OnBackPressed {

    private TextView localityTitle;
    LinearLayout store_layout;
    ImageView locationArrow;
    //    TextView mhome_promoted_text;
    public static String locality;
    private RecyclerView storesListView, bannerListView;

    private AutoScrollViewPager bannersViewpage;
    private StoresAdapter mStoreAdapter;
    private BannersAdapter mBanners;
    //    private LinearLayout locationLayout;
//    LinearLayout promotedlayout, populerlayout, itsnewlayout, aroundlayout, bannerlayout , new_stores_layout;
    LinearLayout bannerlayout;
    //    RelativeLayout lookigsomthinglayout;
//    private RelativeLayout typesLayout;
    //    CircleIndicator defaultIndicator;
    ACProgressFlower dialog;
    SharedPreferences userPrefs;
    ArrayList<StorePaginationList> storePaginationLists = new ArrayList<>();
    public static ArrayList<StoresList.BannerResult> bannerList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> storesList = new ArrayList<>();
    public static ArrayList<StoresList.FilterCategory> filtercatList = new ArrayList<>();
    public static ArrayList<StoresList.VendorTypes> vendorTypesList = new ArrayList<>();
    //    public static ArrayList<StoresList.StoresDetails> itsnewList = new ArrayList<>();
//    public static ArrayList<StoresList.StoresDetails> promotedList = new ArrayList<>();
//    public static ArrayList<StoresList.StoresDetails> threekmList = new ArrayList<>();
    public static ArrayList<StoresList.StoreCategories> somthingList = new ArrayList<>();
    ArrayList<ActiveOrders.Data> data = new ArrayList<>();
    public static boolean filter = false;
    public static int filterdistance = 40, storedistance;

    public static ArrayList<Filter> filterslist = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> filter_main_list = new ArrayList<>();
    public static ArrayList<Brands> brandslist = new ArrayList<>();
    public static ArrayList<Banners> bannerslist = new ArrayList<>();

    ArrayList<Integer> seletedbrands = new ArrayList<>();
    ArrayList<String> seletedbanners = new ArrayList<>();
    ArrayList<Integer> filterseletedbrands = new ArrayList<>();
    GPSTracker gps;
    String TAG = "TAGF";
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    private Context context;

    //    private TextView popularText, nearbyText, newStoresText, lookingsomthingTex;
//    private TextView lookingText, offertext1, offertext2, shareText1, shareText2;
//    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    //    private LinearLayout mapLayout;
    private NestedScrollView listLayout;
    //    private ImageView mapIcon;
//    public static Double latitude, longitude;
    public static ImageView mhome_filter;
    String userId;
    AlertDialog customDialog;
    public static boolean filter_back_btn = false;

//    LinearLayout order_tracking;
//    ImageView order_status;

    private Timer timer = new Timer();

    public static ArrayList<Integer> filterid = new ArrayList<>();
    public static int vendertypeid;

    ArrayList<StoreMenuextenditems> storeMenuextended = new ArrayList<>();
    ArrayList<SearchFilter> searchFilters = new ArrayList<>();
    public static FilterCatAdapter mAdapter;
    public static FilterCatAdapter1 mAdapter1;

    private FusedLocationProviderClient mFusedLocationClient;
    View rootView;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    SharedPreferences LocationPrefs;
    SharedPreferences.Editor LocationPrefsEditor;
    String location_status;
    String locality_name, latitute, longitude;
    //    EditText search_text;
//    ImageView search_btn;
    String language;
    TextView mlanguage;

    int page_no = 1, page_size = 10;

    int lastVisibleItem, no_of_rows, lastvisibleposition = 8;


    public static int REQUEST_CODE_AUTOCOMPLETE = 1;

    public static boolean testing = true;

    SharedPreferences LocationPrefe;
    String LocationStatus;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.home_fragment, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.home_fragment_arabic, container, false);
        }

        LocationPrefs = getActivity().getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
        LocationPrefsEditor = LocationPrefs.edit();
        location_status = LocationPrefs.getString("Location_status", null);
        locality_name = LocationPrefs.getString("LocationName", "");
        latitute = LocationPrefs.getString("Latitute", "");
        longitude = LocationPrefs.getString("Longitute", "");


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

//        mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        filterslist.clear();
        brandslist.clear();
        bannerslist.clear();
        seletedbanners.clear();
        storePaginationLists.clear();


        localityTitle = (TextView) rootView.findViewById(R.id.home_location);
        store_layout = rootView.findViewById(R.id.store_layout);
        locationArrow = rootView.findViewById(R.id.location_arrow);

        mhome_filter = rootView.findViewById(R.id.home_filter);

        listLayout = (NestedScrollView) rootView.findViewById(R.id.list_layout);
        bannersViewpage = (AutoScrollViewPager) rootView.findViewById(R.id.home_banner_list);

        storesListView = (RecyclerView) rootView.findViewById(R.id.home_stores_list);

        bannerlayout = (LinearLayout) rootView.findViewById(R.id.banner_layout);

//        search_text = (EditText) rootView.findViewById(R.id.search_text);
//        search_btn = (ImageView) rootView.findViewById(R.id.search_btn);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        onBackPressed();

//        if (testing) {
//            testing = false;
//            Constants.showOneButtonAlertDialog("جمعنا لك أفضل الطباخين وأفضل الأكلات والطبخ المنزلي .. والي جاي اكثر و اكثر  اطلب من اقرب متجر لك  والف عااافية \uD83D\uDC4C",
//                    getResources().getString(R.string.app_name_ar),
//                    getResources().getString(R.string.ok_ar), getActivity());
//        }

        mlanguage = rootView.findViewById(R.id.language);

        LocationPrefe = context.getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
        LocationStatus = LocationPrefe.getString("Location_status", null);

        if (LocationStatus == null) {

            Constants.showLoadingDialog(getActivity());

            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                } else {
                    try {
                        gps = new GPSTracker(getActivity());
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    gps = new GPSTracker(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        mhome_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showloaderAlertDialog();

            }
        });


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        storesListView.setLayoutManager(linearLayoutManager);


        listLayout.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        int totalItemCount;

                        lastVisibleItem = linearLayoutManager
                                .findLastVisibleItemPosition();
                        Log.i("TAG", "lastvisibleitem: " + lastVisibleItem);

                        if (storePaginationLists.size() > 0) {
//                            for (int i = 0; i < filterslist.size(); i++) {
                            Log.i("TAG", "lastvisibleposition: " + lastvisibleposition);
                            Log.i("TAG", "no_of_rows: " + no_of_rows);
//                                Log.i("TAG", "filter.size: " + (filterslist.get(i).getBrands().size() - 1));
                            if ((lastvisibleposition < lastVisibleItem) && (lastVisibleItem == (storePaginationLists.size() - 1))) {
                                Log.i("TAG", "onScrolled: ");
                                if ((page_no * page_size) <= no_of_rows) {
//                        if () {

//                        page_size = page_size + 10;
                                    page_no = page_no + 1;
                                    Log.i("TAG", "page_no: " + page_no);
                                    new GetStoresApi().execute();
                                    lastvisibleposition = lastVisibleItem;


//                        }
                                }
                            }
//                            }
                        }
                    }
                }
            }
        });

//        storesListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                lastVisibleItem = linearLayoutManager
//                        .findLastVisibleItemPosition();
//                Log.i("TAG", "onScrolled: " + lastVisibleItem);
//
//                if (filterslist.size() > 0) {
//                    for (int i = 0; i < filterslist.size(); i++) {
//                        Log.i("TAG", "lastvisibleposition: " + lastvisibleposition);
//                        Log.i("TAG", "no_of_rows: " + no_of_rows);
//                        Log.i("TAG", "filter.size: " + (filterslist.get(i).getBrands().size() - 1));
//                        if ((lastvisibleposition < lastVisibleItem) && (lastVisibleItem == (filterslist.get(i).getBrands().size() - 1))) {
//                            Log.i("TAG", "onScrolled: " );
//                            if ((page_no * page_size) <= no_of_rows) {
////                        if () {
//
////                        page_size = page_size + 10;
//                                page_no = page_no + 1;
//                                Log.i("TAG", "page_no: " + page_no);
//                                new GetStoresApi().execute();
//                                lastvisibleposition = lastVisibleItem;
//
//
////                        }
//                            }
//                        }
//                    }
//                }
//            }
//        });

        mlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    new ChangeLanguageApi().execute();
                    if (bannerList.size() > 0) {

                        try {
                            mBanners = new BannersAdapter(getContext(), bannerslist, language);
                            bannersViewpage.setAdapter(mBanners);
                            mBanners.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    mBanners.notifyDataSetChanged();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("class", "splash");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    new ChangeLanguageApi().execute();
                    if (bannerList.size() > 0) {
                        try {
                            mBanners = new BannersAdapter(getContext(), bannerslist, language);
                            bannersViewpage.setAdapter(mBanners);
                            mBanners.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    mBanners.notifyDataSetChanged();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("class", "splash");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }

                userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
                userId = userPrefs.getString("userId", "0");
                language = languagePrefs.getString("language", "En");
                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";

                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
                Crashlytics.setString(AppLanguage, language/* string value */);
                Crashlytics.setString("Device Token", SplashScreenActivity.regId);

                if (!userId.equals("0")) {
                    Crashlytics.setString(UserId, userId/* string value */);
                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
                }

                Crashlytics.getInstance();

            }
        });

        bannersViewpage.setInterval(3000);
        bannersViewpage.setCycle(true);
        bannersViewpage.startAutoScroll();


        if (location_status != null) {

            currentLatitude = Double.valueOf(latitute);
            currentLongitude = Double.valueOf(longitude);
            localityTitle.setText("" + locality_name);

            if (filter) {
                new GetFilterApi().execute();
            } else {
                new GetStoresApi().execute();
            }

            Log.d(TAG, "canAccessLocation(): " + canAccessLocation());
            if (canAccessLocation()) {
                try {
                    gps = new GPSTracker(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        store_layout.setOnClickListener(new DoubleClick(new DoubleClickListener() {
            @Override
            public void onSingleClick(View view) {

                Log.i(TAG, "onDoubleClick: " + userId);

                showLocationChangedPopup(gps.getLatitude(), gps.getLongitude());

            }

            @Override
            public void onDoubleClick(View view) {

                userId = userPrefs.getString("userId", "0");
                if (userId.equals("0")) {
                    Intent a = new Intent(getActivity(), SignInActivity.class);
                    startActivity(a);
                } else {
                    Constants.addressboolean = false;
                    Constants.homeboolean = true;
                    Intent intent = new Intent(getContext(), ChangeAddressActivity.class);
                    startActivityForResult(intent, ADDRESS_REQUEST);
                }

                Log.i(TAG, "onDoubleClick: " + userId);

            }
        }));

//        search_text.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                filter_back_btn = true;
//                mhome_filter.setVisibility(View.GONE);
//
//                return false;
//            }
//        });


//        search_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
//
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    str_search_text = search_text.getText().toString();
//
//                    if (str_search_text.length() != 0) {
//
//                        new GetStoresApi().execute();
//
//                    }
//
//                    return true;
//                }
//
//
//                return false;
//            }
//        });

//        search_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                str_search_text = search_text.getText().toString();
//
//                if (str_search_text.length() != 0) {
//
//                    new GetStoresApi().execute();
//
//                }
//
//            }
//        });

//        LocalBroadcastManager.getInstance(context).registerReceiver(
//                mFilter, new IntentFilter("FilterUpdate"));


        if (storesList.size() > 0) {
            for (int i = 0; i < filterslist.size(); i++) {
                mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                storesListView.setAdapter(mStoreAdapter);
            }

        }
        if (bannerList.size() > 0) {
            try {
                mBanners = new BannersAdapter(getContext(), bannerslist, language);
                bannersViewpage.setAdapter(mBanners);
                mBanners.notifyDataSetChanged();
                bannersViewpage.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        if (brandslist.size() == 0) {
//            Constants.showOneButtonAlertDialog("Stores Not Availabe", getResources().getString(R.string.appname),
//                    getResources().getString(R.string.Done), getActivity());
//        }


//        if (userId.equals("0")) {
//            order_tracking.setVisibility(View.GONE);
//        } else {
//            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                new ActiveOrderApi().execute();
//            } else {
//                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//            }
//        }


        return rootView;
    }

    private String prepareChangeLangJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Flag", 1);
            parentObj.put("UserId", userId);
            language = languagePrefs.getString("language", "En");
            parentObj.put("Language", language);
            parentObj.put("deviceToken", SplashScreenActivity.regId);
            parentObj.put("DeviceType", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    @Override
    public void onBackPressed() {

        if (filter_back_btn) {

            filter_back_btn = false;
            mhome_filter.setVisibility(View.VISIBLE);
        }

    }

    private class ChangeLanguageApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangeLangJson();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(getActivity());

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeLanguageList> call = apiService.getchangelang(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeLanguageList>() {
                @Override
                public void onResponse(Call<ChangeLanguageList> call, Response<ChangeLanguageList> response) {
                    if (response.isSuccessful()) {
                        ChangeLanguageList Response = response.body();


                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    }
                }

                @Override
                public void onFailure(Call<ChangeLanguageList> call, Throwable t) {

                }
            });
            return null;
        }
    }


    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        new GetStoresApi().execute();

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }


    public void showloaderAlertDialog() {


        if (getActivity() != null) {

            mhome_filter.setVisibility(View.GONE);
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout;
            if (language.equalsIgnoreCase("En")) {
                layout = R.layout.filter_screen;
            } else {
                layout = R.layout.filter_screen_arabic;
            }
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            final TextView one = dialogView.findViewById(R.id.one);
            final TextView ten = dialogView.findViewById(R.id.ten);
            TextView done = dialogView.findViewById(R.id.done);
            TextView clear = dialogView.findViewById(R.id.clear);
            final RecyclerView menu_list = dialogView.findViewById(R.id.recycler_view);
            final RecyclerView menu_list1 = dialogView.findViewById(R.id.recycler_view1);
            final IndicatorSeekBar seekBar = dialogView.findViewById(R.id.seek_bar);
            ImageView filterback = dialogView.findViewById(R.id.filter_back);


            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            menu_list.setHasFixedSize(true);
            menu_list.setNestedScrollingEnabled(false);
            menu_list.setLayoutManager(mLayoutManager);
            mAdapter = new FilterCatAdapter(getActivity(), filtercatList, language);
            menu_list.setAdapter(mAdapter);

            RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
            menu_list1.setHasFixedSize(true);
            menu_list1.setNestedScrollingEnabled(false);
            menu_list1.setLayoutManager(mLayoutManager1);
            mAdapter1 = new FilterCatAdapter1(getActivity(), vendorTypesList, language);
            menu_list1.setAdapter(mAdapter1);

            seekBar.setProgress(filterdistance);
            seekBar.setIndicatorTextFormat(seekBar.getProgress() + " KM");
            seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
                @Override
                public void onSeeking(SeekParams seekParams) {
                    Log.i(TAG, "showloaderAlertDialog1: " + seekBar.getProgress());

                    seekBar.setIndicatorTextFormat(seekBar.getProgress() + " KM");

                    if (seekBar.getProgress() == 1) {
                        one.setVisibility(View.GONE);
                    } else {
                        one.setVisibility(View.VISIBLE);
                    }
                    if (seekBar.getProgress() == 40) {
                        ten.setVisibility(View.GONE);
                    } else {
                        ten.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });


            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (filterid != null || filterid.size() > 0) {
                        HomeScreenFragment.filter = true;
                    } else {
                        HomeScreenFragment.filter = false;
                    }
//                if (vendertypeid != null || vendertypeid.size() > 0) {
//                    HomeScreenFragment.filter = true;
//                } else {
//                    HomeScreenFragment.filter = false;
//                }
                    filterdistance = seekBar.getProgress();
                    Log.i(TAG, "filter_distance: " + filterdistance);
                    new GetFilterApi().execute();
                    mhome_filter.setVisibility(View.VISIBLE);
                    customDialog.dismiss();

                }
            });

            clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                HomeScreenFragment.filter = false;
                    filterid.clear();
//                vendertypeid.clear();
                    for (int i = 0; i < filtercatList.size(); i++) {
                        for (int j = 0; j < filtercatList.get(i).getFilters().size(); j++) {
                            filterid.add(filtercatList.get(i).getFilters().get(j).getFilterId());
                        }
                    }

//                for (int i = 0; i < vendorTypesList.size(); i++) {
//                    for (int j = 0; j < vendorTypesList.get(i).getFilters().size(); j++) {
                    vendertypeid = 2;
//                    }
//                }
                    mAdapter = new FilterCatAdapter(getActivity(), filtercatList, language);
                    menu_list.setAdapter(mAdapter);

                    mAdapter1 = new FilterCatAdapter1(getActivity(), vendorTypesList, language);
                    menu_list1.setAdapter(mAdapter1);
                    filterdistance = 40;
                    seekBar.setProgress(40);
                    Log.i(TAG, "filter_distance: " + filterdistance);
//                customDialog.dismiss();

                }
            });

            filterback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mhome_filter.setVisibility(View.VISIBLE);
                    customDialog.dismiss();

                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.95;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

//    private void flipCard() {
//        FlipAnimation flipAnimation = new FlipAnimation(listLayout, mapLayout);
//
//        if (listLayout.getVisibility() == View.GONE) {
//            flipAnimation.reverse();
//            mapIcon.setImageDrawable(getResources().getDrawable(R.drawable.home_screen_map_icon));
//        } else {
//            mapIcon.setImageDrawable(getResources().getDrawable(R.drawable.tablelist));
//        }
//        mapLayout.startAnimation(flipAnimation);
//    }

    private void setTypeface() {

    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }

    public void getGPSCoordinates() throws IOException {

        if (gps != null) {
            if (gps.canGetLocation()) {

//                currentLatitude = gps.getLatitude();
//                currentLongitude = gps.getLongitude();

                Log.i("TAG", "fused lat " + currentLatitude);
                Log.i("TAG", "fused long " + currentLongitude);

                try {

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (LocationStatus == null) {

                    location();

                }

//                if (filter) {
//                    new GetFilterApi().execute();
//                } else {
//                    new GetStoresApi().execute();
//                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
//                gps.showSettingsAlert();
            }
        }
    }

    public void location() {

        if (currentLatitude != 0) {

            Constants.closeLoadingDialog();

            getLocality();

        } else {

            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                } else {
                    try {
                        gps = new GPSTracker(getActivity());
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    gps = new GPSTracker(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getActivity());
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gps != null) {
            gps.stopUsingGPS();
        }
        Constants.closeLoadingDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gps != null) {
            gps.stopUsingGPS();
        }
        timer.cancel();
        Constants.closeLoadingDialog();
    }

//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "onBroadcastReceive: ");
//            // Get extra data included in the Intent
//            Bundle b = intent.getBundleExtra("Location");
//            Location location = (Location) b.getParcelable("Location");
//            if (location != null) {
//                if (isCurrentLocationSelected) {
//                    currentLatitude = location.getLatitude();
//                    currentLongitude = location.getLongitude();
//                    if (mMap != null) {
//                        mMap.clear();
//                        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                        mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
//                        MarkerOptions markerOptions = new MarkerOptions();
//                        markerOptions.position(latLng);
//                        mMap.addMarker(markerOptions);
//                    }
//                    getLocality();
//                }
//
//            }
//        }
//    };

    private BroadcastReceiver mFilter = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onBroadcastReceive: Filter");
            // Get extra data included in the Intent
            filterseletedbrands.clear();
            bannerslist.clear();
            seletedbanners.clear();

//            for (int j = 0; j < brandslist.size(); j++) {
//
//                for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                    if (storesList.get(i).getDiscountamt() > 0) {
//                    Filter filter = new Filter();
//                    filter.setBrandId(storesList.get(i).getBrandId());
//                    filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                    filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                    filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                    filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                    filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                    for (int l = 0; l < brandslist.size(); l++) {
//                        for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                            if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                fillterstore.add(brandslist.get(l).getBrands().get(m));
//                            }
//                        }
//                    }
//                    filter.setBrands(fillterstore);
//                    filterslist.add(filter);
//
////                                    }
//                }
//            }
//
//            for (int l = 0; l < filterslist.size(); l++) {
//                Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//            }
//
//            Log.i(TAG, "brands_size: " + brandslist.size());
//            Log.i(TAG, "filter_size " + filterslist.size());
//
//            Log.i(TAG, "filter " + filter);


            if (filter) {
                if (FilterActivity.Relavance.isChecked()) {

                    seletedbanners.clear();
                    for (int i = 0; i < bannerList.size(); i++) {

                        if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                            seletedbanners.add(bannerList.get(i).getBannerName_En());
                            Banners banners = new Banners();
                            banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                            banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                            banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                            ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                            for (int j = 0; j < bannerList.size(); j++) {
                                if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                    fillterstore.add(bannerList.get(j));
                                    for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                        ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                        for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                            if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                            bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                            }

                                        }

                                        banners.setBannerList(bannerlist);

                                    }
                                }
                            }


                            banners.setBanners(fillterstore);
                            bannerslist.add(banners);

                        }

                        Log.d(TAG, "" + brandslist.size());

                    }

                    for (int l = 0; l < bannerslist.size(); l++) {
                        Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);
                    }

                    filterseletedbrands.clear();
                    for (int i = 0; i < storesList.size(); i++) {

//                                            if (storesList.get(i).getStorestatus().equals("Open")) {
                        Filter filter = new Filter();

                        filterseletedbrands.add(storesList.get(i).getBrandId());
                        filter.setBrandId(storesList.get(i).getBrandId());
                        filter.setBradNameEn(storesList.get(i).getBrandName_En());
                        filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                        filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                        filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                        filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                        ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                        for (int j = 0; j < storesList.size(); j++) {
//                            if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                            fillterstore.add(storesList.get(j));
//                            }

                        }
                        filter.setBrands(fillterstore);
                        filter_main_list.addAll(fillterstore);
                        filterslist.add(filter);
//                                            }

                    }

                    for (int l = 0; l < filterslist.size(); l++) {
                        Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                    }
                    Log.i(TAG, "Relavence ");


                }
            }

            Log.i(TAG, "brandslistsort " + brandslist);

            for (int i = 0; i < filterslist.size(); i++) {
                mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                storesListView.setAdapter(mStoreAdapter);
            }

//            promotedList.clear();
//            threekmList.clear();
//            itsnewList.clear();
//            popularList.clear();


//            for (int j = 0; j < filterslist.size(); j++)

//            {
//                for (int k = 0; k < filterslist.get(j).getBrands().size(); k++) {

//                    if (filterslist.get(j).getBrands().get(k).getPopular()) {
//                        popularList.add(filterslist.get(j).getBrands().get(k));
//                    }
//                    if (filterslist.get(j).getBrands().get(k).getIsNew()) {
//                        itsnewList.add(filterslist.get(j).getBrands().get(k));
//                    }
//                    if (filterslist.get(j).getBrands().get(k).getIsPromoted()) {
//                        promotedList.add(filterslist.get(j).getBrands().get(k));
//                    }
//                    if (filterslist.get(j).getBrands().get(k).getDistance() <= 3) {
//                        threekmList.add(filterslist.get(j).getBrands().get(k));
//                    }

//                    if (promotedList.size() == 0) {
//                        promotedlayout.setVisibility(View.GONE);
//                    } else {
//                        promotedlayout.setVisibility(View.VISIBLE);
//                        mPromotedAdapter = new PromotedStoresAdapter(getContext(), promotedList);
//                        promotedViewPager.setAdapter(mPromotedAdapter);
//                    }
//
//                    if (popularList.size() == 0) {
//                        populerlayout.setVisibility(View.GONE);
//                    } else {
//                        populerlayout.setVisibility(View.VISIBLE);
//                        mPopularAdapter = new PopularStoresAdapter(getContext(), popularList);
//                        popuplarViewPager.setAdapter(mPopularAdapter);
//                    }
//
//                    if (threekmList.size() == 0) {
//                        aroundlayout.setVisibility(View.GONE);
//                    } else {
//                        aroundlayout.setVisibility(View.VISIBLE);
//                        mAround = new Around3kmStoresAdapter(getContext(), threekmList);
//                        aroundViewPager.setAdapter(mAround);
//                    }
//
//                    if (itsnewList.size() == 0) {
//                        itsnewlayout.setVisibility(View.GONE);
//                    } else {
//                        itsnewlayout.setVisibility(View.VISIBLE);
//                        mItsnewAdapter = new ItsNewStoresAdapter(getContext(), itsnewList);
//                        itsnewStoresViewPager.setAdapter(mItsnewAdapter);
//                    }

//            if (bannerList.size() == 0) {
//                bannerlayout.setVisibility(View.GONE);
//            } else {
            bannerlayout.setVisibility(View.VISIBLE);
            try {
                mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                bannersViewpage.setAdapter(mBanners);
                mBanners.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
//            }
//                    if (somthingList.size() == 0) {
//                        lookigsomthinglayout.setVisibility(View.GONE);
//                    } else {
//                        lookigsomthinglayout.setVisibility(View.VISIBLE);
//                        mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
//                        lookingStoresViewPager.setAdapter(mLookingAdapter);
//                    }
//
//                    shimmerPromotedLayout.stopShimmerAnimation();
//                    shimmerPopularLayout.stopShimmerAnimation();
//                    shimmerNearbyLayout.stopShimmerAnimation();
//                    shimmerNewStoresLayout.stopShimmerAnimation();
//
//                    shimmerPromotedLayout.setVisibility(View.GONE);
//                    shimmerPopularLayout.setVisibility(View.GONE);
//                    shimmerNearbyLayout.setVisibility(View.GONE);
//                    shimmerNewStoresLayout.setVisibility(View.GONE);
//
//                    promotedViewPager.setVisibility(View.VISIBLE);
//                    popuplarViewPager.setVisibility(View.VISIBLE);
//                    aroundViewPager.setVisibility(View.VISIBLE);
//                    itsnewStoresViewPager.setVisibility(View.VISIBLE);
            bannersViewpage.setVisibility(View.VISIBLE);
//                    typesLayout.setVisibility(View.VISIBLE);
//                    lookingStoresViewPager.setVisibility(View.VISIBLE);

//                }
        }


//        }
    };

    public void getLocality() {
        Location mLocation = new Location("");
        mLocation.setLatitude(currentLatitude);
        mLocation.setLongitude(currentLongitude);

        Geocoder geocoder = null;
        try {
            geocoder = new Geocoder(context, Locale.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Address found using the Geocoder.
        List<Address> addresses = null;
        String errorMessage = "";

        try {
            addresses = geocoder.getFromLocation(
                    mLocation.getLatitude(),
                    mLocation.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = context.getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = context.getString(R.string.invalid_lat_long_used);
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + mLocation.getLatitude() +
                    ", Longitude = " + mLocation.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
        } else {
            Address address = addresses.get(0);
            Log.d(TAG, "getLocality: " + address);
            if (address.getSubLocality() != null) {
                localityTitle.setText(address.getSubLocality());
                LocationPrefsEditor.putString("LocationName", address.getSubLocality());
            } else {
                localityTitle.setText(address.getLocality());
                LocationPrefsEditor.putString("LocationName", address.getLocality());
            }

            Log.d(TAG, "getLocality: " + currentLatitude);
            LocationPrefsEditor.putString("Location_status", "locationON");
            LocationPrefsEditor.putString("Latitute", String.valueOf(currentLatitude));
            LocationPrefsEditor.putString("Longitute", String.valueOf(currentLongitude));
            LocationPrefsEditor.commit();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.store_layout:
//                Constants.addressboolean = false;
//                Constants.homeboolean = true;
//                showLocationChangedPopup(gps.getLatitude(), gps.getLongitude());
//
//                break;

//            case R.id.home_map_icon:
//                flipCard();
//                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADDRESS_REQUEST && resultCode == RESULT_OK) {
            currentLatitude = data.getDoubleExtra("lat", 0);
            currentLongitude = data.getDoubleExtra("longi", 0);
            locality = data.getStringExtra("loc");
            Log.i("TAG", "currentlat: " + currentLatitude);
            if (currentLatitude == 0) {

                Log.i("TAG", "currentlat1: " + currentLatitude);

//                isCurrentLocationSelected = true;

                try {
                    gps = new GPSTracker(getActivity());
                    ChangeLocationCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {

                isCurrentLocationSelected = false;

                if (mMap != null) {
                    mMap.clear();
                    LatLng latLng = new LatLng(currentLatitude, currentLongitude);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    mMap.addMarker(markerOptions);
                }

                if (filter) {
                    new GetFilterApi().execute();
                } else {
                    new GetStoresApi().execute();
                }

            }

            if (locality == null || locality.equals("")) {
                getLocality();
            } else {
                localityTitle.setText(locality);
            }

        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (canAccessLocation()) {
            mMap = googleMap;
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.clear();
        }
//        LatLng latLng = new LatLng(latitude, longitude);
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        mMap.addMarker(markerOptions);
    }


    private class ActiveOrderApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;
        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ActiveOrders> call = apiService.getActiveOrders(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ActiveOrders>() {
                @Override
                public void onResponse(Call<ActiveOrders> call, Response<ActiveOrders> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        ActiveOrders orderItems = response.body();
                        data = orderItems.getData();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();

                                for (int i = 0; i < data.size(); i++) {
                                    if (data.get(i).getOrderStatus().equals("Close")) {

//                                        order_tracking.setVisibility(View.GONE);

                                    } else {

//                                        order_tracking.setVisibility(View.VISIBLE);

                                        if (data.get(i).getOrderStatus().equals("New")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_order_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Accepted")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_confirmed_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Ready")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_ready_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Served")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_delivered_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Delivered")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_delivered_black)
//                                                    .into(order_status);

                                        }
                                        final int finalI = i;
                                        final int finalI1 = i;
//                                        order_tracking.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                Intent a = new Intent(getActivity(), TrackOrderActivity.class);
//                                                a.putExtra("userid", data.get(finalI).getUserId());
//                                                a.putExtra("orderid", data.get(finalI).getOrderId());
//                                                a.putExtra("track_order", "home_screen");
//                                                startActivity(a);

                                        Log.i(TAG, "userid: " + data.get(finalI1).getUserId());
                                        Log.i(TAG, "orderid: " + data.get(finalI).getOrderId());
//                                            }
//                                        });
                                    }
                                }


                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {

                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());

                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ActiveOrders> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (getActivity() != null) {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("UserId", userId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private class GetStoresApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        AlertDialog loaderDialog = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
//            filterslist.clear();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(getActivity());
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            loaderDialog = dialogBuilder.create();
            loaderDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = loaderDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {

                        StoresList stores = response.body();
                        if (stores.getStatus()) {

                            if (getActivity() != null) {
                                storesListView.setVisibility(View.VISIBLE);
                                bannersViewpage.setVisibility(View.VISIBLE);
                            }
                            storesList = stores.getData().getStoresDetails();
                            bannerList = stores.getData().getBannerResult();
                            somthingList = stores.getData().getStoreCategories();
                            filtercatList = stores.getData().getFilterCategory();
                            vendorTypesList = stores.getData().getVendorTypes();

                            mhome_filter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showloaderAlertDialog();

                                }
                            });


                            brandslist.clear();
//                            filterslist.clear();
                            bannerslist.clear();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandId())) {
                                    seletedbrands.add(storesList.get(i).getBrandId());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandId());
                                    brands.setBradNameEn(storesList.get(i).getBrandName_En());
                                    brands.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                    brands.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                    brands.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                    brands.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                            fillterstore.add(storesList.get(j));
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            seletedbanners.clear();
                            for (int i = 0; i < bannerList.size(); i++) {
                                if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                                    seletedbanners.add(bannerList.get(i).getBannerName_En());
                                    Banners banners = new Banners();
                                    banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                                    banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                                    banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                                    ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < bannerList.size(); j++) {
                                        if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                            fillterstore.add(bannerList.get(j));
                                            for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                                ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                                for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                                    if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                                    bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                                    }

                                                }

                                                banners.setBannerList(bannerlist);

                                            }
                                        }
                                    }


                                    banners.setBanners(fillterstore);
                                    bannerslist.add(banners);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            for (int l = 0; l < bannerslist.size(); l++) {
                                if (bannerslist.get(l).getBannerList() == null || bannerslist.get(l).getBannerList().size() == 0) {

                                } else {

                                    Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);

                                }
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerResult());

                            for (int p = 0; p < storesList.size(); p++) {

                                StorePaginationList storePaginationList = new StorePaginationList();

                                storePaginationList.setId(storesList.get(p).getId());
                                storePaginationList.setWeekNo(storesList.get(p).getWeekNo());
                                storePaginationList.setBranchId(storesList.get(p).getBranchId());
                                storePaginationList.setBranchName_En(storesList.get(p).getBranchName_En());
                                storePaginationList.setBranchName_Ar(storesList.get(p).getBranchName_Ar());
                                storePaginationList.setBrandId(storesList.get(p).getBrandId());
                                storePaginationList.setBrandName_En(storesList.get(p).getBrandName_En());
                                storePaginationList.setBrandName_Ar(storesList.get(p).getBrandName_Ar());
                                storePaginationList.setBranchDescription_En(storesList.get(p).getBranchDescription_En());
                                storePaginationList.setBranchDescription_Ar(storesList.get(p).getBranchDescription_Ar());
                                storePaginationList.setStarDateTime(storesList.get(p).getStarDateTime());
                                storePaginationList.setEndDateTime(storesList.get(p).getEndDateTime());
                                storePaginationList.setCurrentDateTime(storesList.get(p).getCurrentDateTime());
                                storePaginationList.setBranchCode(storesList.get(p).getBranchCode());
                                storePaginationList.setAddress(storesList.get(p).getAddress());
                                storePaginationList.setTelephoneNo(storesList.get(p).getTelephoneNo());
                                storePaginationList.setMobileNo(storesList.get(p).getMobileNo());
                                storePaginationList.setEmailId(storesList.get(p).getEmailId());
                                storePaginationList.setDeliveryDistance(storesList.get(p).getDeliveryDistance());
                                storePaginationList.setTakeAwayDistance(storesList.get(p).getTakeAwayDistance());
                                storePaginationList.setCurrentDate(storesList.get(p).getCurrentDate());
                                storePaginationList.setStoreStatus(storesList.get(p).getStoreStatus());
                                storePaginationList.setStoreLogo_En(storesList.get(p).getStoreLogo_En());
                                storePaginationList.setStoreImage_En(storesList.get(p).getStoreImage_En());
                                storePaginationList.setDistance(storesList.get(p).getDistance());
                                storePaginationList.setRating(storesList.get(p).getRating());
                                storePaginationList.setLatitude(storesList.get(p).getLatitude());
                                storePaginationList.setLongitude(storesList.get(p).getLongitude());
                                storePaginationList.setMinOrderCharges(storesList.get(p).getMinOrderCharges());
                                storePaginationList.setDeliveryCharges(storesList.get(p).getDeliveryCharges());
                                storePaginationList.setAvgPreparationTime(storesList.get(p).getAvgPreparationTime());
                                storePaginationList.setServing(storesList.get(p).getServing());
                                storePaginationList.setFutureOrderDay(storesList.get(p).getFutureOrderDay());
                                storePaginationList.setIsPromoted(storesList.get(p).getIsPromoted());
                                storePaginationList.setPopular(storesList.get(p).getPopular());
                                storePaginationList.setIsNew(storesList.get(p).getIsNew());
                                storePaginationList.setPromotedBranch_En(storesList.get(p).getPromotedBranch_En());
                                storePaginationList.setPromotedBranch_Ar(storesList.get(p).getPromotedBranch_Ar());
                                storePaginationList.setRatingCount(storesList.get(p).getRatingCount());
                                storePaginationList.setOrderTypes(storesList.get(p).getOrderTypes());
                                storePaginationList.setStoreType(storesList.get(p).getStoreType());
                                storePaginationList.setStoreType_Ar(storesList.get(p).getStoreType_Ar());
                                storePaginationList.setPaymentType(storesList.get(p).getPaymentType());
                                storePaginationList.setDiscountAmt(storesList.get(p).getDiscountAmt());
                                storePaginationList.setPriceCategory(storesList.get(p).getPriceCategory());
                                storePaginationList.setTotalRows(storesList.get(p).getTotalRows());

                                storePaginationLists.add(storePaginationList);

                            }

                            Log.i("TAG", "storepagination: " + storePaginationLists.size());

                            filterseletedbrands.clear();
                            for (int i = 0; i < storesList.size(); i++) {

//                                    if (storesList.get(i).getDiscountamt() > 0) {


                                filterseletedbrands.add(storesList.get(i).getBrandId());
                                Filter filter = new Filter();

                                filter.setBrandId(storesList.get(i).getBrandId());
                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                for (int j = 0; j < storesList.size(); j++) {
//                                    if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                    fillterstore.add(storesList.get(j));

//                                    }
                                }
                                filter.setBrands(fillterstore);
                                filterslist.add(filter);


                            }

                            Filter filter = new Filter();
                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                            for (int m = 0; m < storePaginationLists.size(); m++) {

                                StoresList.StoresDetails storePaginationList = new StoresList.StoresDetails();

                                storePaginationList.setId(storePaginationLists.get(m).getId());
                                storePaginationList.setWeekNo(storePaginationLists.get(m).getWeekNo());
                                storePaginationList.setBranchId(storePaginationLists.get(m).getBranchId());
                                storePaginationList.setBranchName_En(storePaginationLists.get(m).getBranchName_En());
                                storePaginationList.setBranchName_Ar(storePaginationLists.get(m).getBranchName_Ar());
                                storePaginationList.setBrandId(storePaginationLists.get(m).getBrandId());
                                storePaginationList.setBrandName_En(storePaginationLists.get(m).getBrandName_En());
                                storePaginationList.setBrandName_Ar(storePaginationLists.get(m).getBrandName_Ar());
                                storePaginationList.setBranchDescription_En(storePaginationLists.get(m).getBranchDescription_En());
                                storePaginationList.setBranchDescription_Ar(storePaginationLists.get(m).getBranchDescription_Ar());
                                storePaginationList.setStarDateTime(storePaginationLists.get(m).getStarDateTime());
                                storePaginationList.setEndDateTime(storePaginationLists.get(m).getEndDateTime());
                                storePaginationList.setCurrentDateTime(storePaginationLists.get(m).getCurrentDateTime());
                                storePaginationList.setBranchCode(storePaginationLists.get(m).getBranchCode());
                                storePaginationList.setAddress(storePaginationLists.get(m).getAddress());
                                storePaginationList.setTelephoneNo(storePaginationLists.get(m).getTelephoneNo());
                                storePaginationList.setMobileNo(storePaginationLists.get(m).getMobileNo());
                                storePaginationList.setEmailId(storePaginationLists.get(m).getEmailId());
                                storePaginationList.setDeliveryDistance(storePaginationLists.get(m).getDeliveryDistance());
                                storePaginationList.setTakeAwayDistance(storePaginationLists.get(m).getTakeAwayDistance());
                                storePaginationList.setCurrentDate(storePaginationLists.get(m).getCurrentDate());
                                storePaginationList.setStoreStatus(storePaginationLists.get(m).getStoreStatus());
                                storePaginationList.setStoreLogo_En(storePaginationLists.get(m).getStoreLogo_En());
                                storePaginationList.setStoreImage_En(storePaginationLists.get(m).getStoreImage_En());
                                storePaginationList.setDistance(storePaginationLists.get(m).getDistance());
                                storePaginationList.setRating(storePaginationLists.get(m).getRating());
                                storePaginationList.setLatitude(storePaginationLists.get(m).getLatitude());
                                storePaginationList.setLongitude(storePaginationLists.get(m).getLongitude());
                                storePaginationList.setMinOrderCharges(storePaginationLists.get(m).getMinOrderCharges());
                                storePaginationList.setDeliveryCharges(storePaginationLists.get(m).getDeliveryCharges());
                                storePaginationList.setAvgPreparationTime(storePaginationLists.get(m).getAvgPreparationTime());
                                storePaginationList.setServing(storePaginationLists.get(m).getServing());
                                storePaginationList.setFutureOrderDay(storePaginationLists.get(m).getFutureOrderDay());
                                storePaginationList.setIsPromoted(storePaginationLists.get(m).getIsPromoted());
                                storePaginationList.setPopular(storePaginationLists.get(m).getPopular());
                                storePaginationList.setIsNew(storePaginationLists.get(m).getIsNew());
                                storePaginationList.setPromotedBranch_En(storePaginationLists.get(m).getPromotedBranch_En());
                                storePaginationList.setPromotedBranch_Ar(storePaginationLists.get(m).getPromotedBranch_Ar());
                                storePaginationList.setRatingCount(storePaginationLists.get(m).getRatingCount());
                                storePaginationList.setOrderTypes(storePaginationLists.get(m).getOrderTypes());
                                storePaginationList.setStoreType(storePaginationLists.get(m).getStoreType());
                                storePaginationList.setStoreType_Ar(storePaginationLists.get(m).getStoreType_Ar());
                                storePaginationList.setPaymentType(storePaginationLists.get(m).getPaymentType());
                                storePaginationList.setDiscountAmt(storePaginationLists.get(m).getDiscountAmt());
                                storePaginationList.setPriceCategory(storePaginationLists.get(m).getPriceCategory());
                                storePaginationList.setTotalRows(storePaginationLists.get(m).getTotalRows());

                                fillterstore.add(storePaginationList);

                            }

                            filter.setBrands(fillterstore);
                            filterslist.add(filter);

                            for (int l = 0; l < filterslist.size(); l++) {
                                Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                                no_of_rows = filterslist.get(l).getBrands().get(0).getTotalRows();
                            }

                            Log.i(TAG, "brands_size: " + brandslist.size());
                            Log.i(TAG, "filter_size " + filterslist.size());
//
//                            Log.i(TAG, "filter " + filter);
//                            if (filter) {
//                                if (FilterActivity.Discount.isChecked() && FilterActivity.Open_stores.isChecked() && FilterActivity.Relavance.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0 && storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//
//                                    Log.i(TAG, "dicount && open store && R ");
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Open_stores.isChecked() && FilterActivity.Rating.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0 && storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "dicount && open store && Rating ");
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Open_stores.isChecked() && FilterActivity.Price.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0 && storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "dicount && open store && price ");
//
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Relavance.isChecked()) {
//
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//
//                                    Log.i(TAG, "dicount && R ");
//
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Rating.isChecked()) {
//
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "dicount && Rating ");
//
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Price.isChecked()) {
//
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "dicount && price ");
//
//                                } else if (FilterActivity.Open_stores.isChecked() && FilterActivity.Relavance.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//
//                                    Log.i(TAG, "open store && relavence ");
//
//                                } else if (FilterActivity.Open_stores.isChecked() && FilterActivity.Rating.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "open store && rating ");
//
//                                } else if (FilterActivity.Open_stores.isChecked() && FilterActivity.Price.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "open store && price ");
//
//                                } else if (FilterActivity.Relavance.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                            if (storesList.get(i).getStorestatus().equals("Open")) {
//                                            Filter filter = new Filter();
//                                            filter.setBrandId(storesList.get(i).getBrandId());
//                                            filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                            filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                            filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                            filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                            filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                            for (int l = 0; l < brandslist.size(); l++) {
//                                                for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                    if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                        fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                    }
//                                                }
//                                            }
//                                            filter.setBrands(fillterstore);
//                                            filterslist.add(filter);
//
////                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//                                    Log.i(TAG, "Relavence ");
//
//                                } else if (FilterActivity.Rating.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                            if (storesList.get(i).getStorestatus().equals("Open")) {
//                                            Filter filter = new Filter();
//                                            filter.setBrandId(storesList.get(i).getBrandId());
//                                            filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                            filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                            filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                            filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                            filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                            for (int l = 0; l < brandslist.size(); l++) {
//                                                for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                    if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                        fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                    }
//                                                }
//                                            }
//                                            filter.setBrands(fillterstore);
//                                            filterslist.add(filter);
//
////                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "Rating ");
//
//                                } else if (FilterActivity.Price.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                            if (storesList.get(i).getStorestatus().equals("Open")) {
//                                            Filter filter = new Filter();
//                                            filter.setBrandId(storesList.get(i).getBrandId());
//                                            filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                            filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                            filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                            filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                            filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                            for (int l = 0; l < brandslist.size(); l++) {
//                                                for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                    if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                        fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                    }
//                                                }
//                                            }
//                                            filter.setBrands(fillterstore);
//                                            filterslist.add(filter);
//
////                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "Price ");
//
//                                }
//                            }
//
//                            Log.i(TAG, "brandslistsort " + brandslist);
//
                            if (getActivity() != null) {
                                for (int i = 0; i < filterslist.size(); i++) {
                                    mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                                    storesListView.setAdapter(mStoreAdapter);
                                }
                                bannersViewpage.setAdapter(mBanners);
                            }
//                            lookingStoresViewPager.setAdapter(mLookingAdapter);


//                            promotedList.clear();
//                            threekmList.clear();
//                            itsnewList.clear();
//                            popularList.clear();


//                            for (int j = 0; j < filterslist.size(); j++) {
//                                for (int k = 0; k < filterslist.get(j).getBrands().size(); k++) {
//
//                                    if (filterslist.get(j).getBrands().get(k).getPopular()) {
//                                        popularList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (filterslist.get(j).getBrands().get(k).getIsNew()) {
//                                        itsnewList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (filterslist.get(j).getBrands().get(k).getIsPromoted()) {
//                                        promotedList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (filterslist.get(j).getBrands().get(k).getDistance() <= 3) {
//
//                                        threekmList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (getActivity() != null) {
//                                        if (promotedList.size() == 0) {
//                                            promotedlayout.setVisibility(View.GONE);
//                                        } else {
//                                            promotedlayout.setVisibility(View.VISIBLE);
//                                            mPromotedAdapter = new PromotedStoresAdapter(getContext(), promotedList);
//                                            promotedViewPager.setAdapter(mPromotedAdapter);
//                                        }
//                                    }
//
//                                    if (getActivity() != null) {
//                                        if (popularList.size() == 0) {
//                                            populerlayout.setVisibility(View.GONE);
//                                        } else {
//                                            populerlayout.setVisibility(View.VISIBLE);
//                                            mPopularAdapter = new PopularStoresAdapter(getContext(), popularList);
//                                            popuplarViewPager.setAdapter(mPopularAdapter);
//                                        }
//                                    }
//
//                                    if (getActivity() != null) {
//                                        if (threekmList.size() == 0) {
//                                            aroundlayout.setVisibility(View.GONE);
//                                        } else {
//                                            aroundlayout.setVisibility(View.VISIBLE);
//                                            mAround = new Around3kmStoresAdapter(getContext(), threekmList);
//                                            aroundViewPager.setAdapter(mAround);
//                                        }
//                                    }
//                                    if (getActivity() != null) {
//                                        if (itsnewList.size() == 0) {
//                                            itsnewlayout.setVisibility(View.GONE);
//                                        } else {
//
//                                            itsnewlayout.setVisibility(View.VISIBLE);
//                                            mItsnewAdapter = new ItsNewStoresAdapter(getContext(), itsnewList);
//                                            itsnewStoresViewPager.setAdapter(mItsnewAdapter);
//                                        }
//                                    }

                            if (getActivity() != null) {
//                                if (bannerslist.size() == 0) {
//                                    bannerlayout.setVisibility(View.GONE);
//                                } else {
                                bannerlayout.setVisibility(View.VISIBLE);
                                try {
                                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                                    bannersViewpage.setAdapter(mBanners);
                                    mBanners.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                }
                            }
//                                    if (getActivity() != null) {
//                                        if (somthingList.size() == 0) {
//                                            lookigsomthinglayout.setVisibility(View.GONE);
//                                        } else {
//                                            lookigsomthinglayout.setVisibility(View.VISIBLE);
//                                            mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
//                                            lookingStoresViewPager.setAdapter(mLookingAdapter);
//                                        }
//                                    }
//
//                                    shimmerPromotedLayout.stopShimmerAnimation();
//                                    shimmerPopularLayout.stopShimmerAnimation();
//                                    shimmerNearbyLayout.stopShimmerAnimation();
//                                    shimmerNewStoresLayout.stopShimmerAnimation();
//
//                                    shimmerPromotedLayout.setVisibility(View.GONE);
//                                    shimmerPopularLayout.setVisibility(View.GONE);
//                                    shimmerNearbyLayout.setVisibility(View.GONE);
//                                    shimmerNewStoresLayout.setVisibility(View.GONE);
//
//                                    promotedViewPager.setVisibility(View.VISIBLE);
//                                    popuplarViewPager.setVisibility(View.VISIBLE);
//                                    aroundViewPager.setVisibility(View.VISIBLE);
//                                    itsnewStoresViewPager.setVisibility(View.VISIBLE);
                            bannersViewpage.setVisibility(View.VISIBLE);
//                                    typesLayout.setVisibility(View.VISIBLE);
//                                    lookingStoresViewPager.setVisibility(View.VISIBLE);

//                                }
//                            }
                            for (int i = 0; i < storesList.size(); i++) {

                            }
                        } else {
                            if (getActivity() != null) {
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = stores.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = stores.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                                storesListView.setVisibility(View.GONE);
                                bannersViewpage.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    Log.i(TAG, "onFailure: " + t);
                    if (getActivity() != null) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    Log.i("TAG", "onFailure: " + t);

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }


    }

    private String prepareGetStoresJSON() {

        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
            parentObj.put("BranchId", "");
            parentObj.put("SearchText", "");
            parentObj.put("pageNumber", page_no);
            parentObj.put("pageSize", page_size);
            parentObj.put("VendorType", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }


    private class GetFilterApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON1();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(getActivity());
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            loaderDialog = dialogBuilder.create();
            loaderDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = loaderDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {

                        StoresList stores = response.body();
                        if (stores.getStatus()) {

                            if (getActivity() != null) {
                                storesListView.setVisibility(View.VISIBLE);
                                bannersViewpage.setVisibility(View.VISIBLE);
                            }
                            storesList = stores.getData().getStoresDetails();
                            bannerList = stores.getData().getBannerResult();
                            somthingList = stores.getData().getStoreCategories();
                            filtercatList = stores.getData().getFilterCategory();
                            vendorTypesList = stores.getData().getVendorTypes();

                            mhome_filter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showloaderAlertDialog();

                                }
                            });


                            brandslist.clear();
                            filterslist.clear();
                            bannerslist.clear();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandId())) {
                                    seletedbrands.add(storesList.get(i).getBrandId());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandId());
                                    brands.setBradNameEn(storesList.get(i).getBrandName_En());
                                    brands.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                    brands.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                    brands.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                    brands.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
//                                            if (filterdistance >= storesList.get(i).getDistance()) {
                                            fillterstore.add(storesList.get(j));
//                                            }
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            seletedbanners.clear();
                            for (int i = 0; i < bannerList.size(); i++) {
                                if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                                    seletedbanners.add(bannerList.get(i).getBannerName_En());
                                    Banners banners = new Banners();
                                    banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                                    banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                                    banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                                    ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < bannerList.size(); j++) {
                                        if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                            fillterstore.add(bannerList.get(j));
                                            for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                                ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                                for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                                    if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                                    bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                                    }

                                                }

                                                banners.setBannerList(bannerlist);

                                            }
                                        }
                                    }


                                    banners.setBanners(fillterstore);
                                    bannerslist.add(banners);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            for (int l = 0; l < bannerslist.size(); l++) {
                                if (bannerslist.get(l).getBannerList() == null || bannerslist.get(l).getBannerList().size() == 0) {

                                } else {

                                    Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);

                                }
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerResult());

                            filterseletedbrands.clear();
                            for (int i = 0; i < storesList.size(); i++) {

//                                    if (storesList.get(i).getDiscountamt() > 0) {


                                filterseletedbrands.add(storesList.get(i).getBrandId());
                                Filter filter = new Filter();

                                filter.setBrandId(storesList.get(i).getBrandId());
                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                for (int j = 0; j < storesList.size(); j++) {
//                                    if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                    fillterstore.add(storesList.get(j));

//                                    }
                                }
                                filter.setBrands(fillterstore);
                                filterslist.add(filter);


                            }

                            for (int l = 0; l < filterslist.size(); l++) {
                                Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                            }

                            Log.i(TAG, "brands_size: " + brandslist.size());
                            Log.i(TAG, "filter_size " + filterslist.size());

                            if (getActivity() != null) {
                                if (filterslist.size() != 0) {
                                    storesListView.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < filterslist.size(); i++) {
                                        mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                                        storesListView.setAdapter(mStoreAdapter);
                                    }
                                } else {
                                    storesListView.setVisibility(View.GONE);
                                }
                                bannersViewpage.setAdapter(mBanners);
                                mBanners.notifyDataSetChanged();
                            }


                            if (getActivity() != null) {
//                                if (bannerslist.size() == 0) {
//                                    bannerlayout.setVisibility(View.GONE);
//                                } else {
                                bannerlayout.setVisibility(View.VISIBLE);
                                try {
                                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                                    bannersViewpage.setAdapter(mBanners);
                                    mBanners.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                }
                            }

                            bannersViewpage.setVisibility(View.VISIBLE);
                            for (int i = 0; i < storesList.size(); i++) {

                            }
                        } else {

                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = stores.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), getActivity());
                            } else {
                                String failureResponse = stores.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), getActivity());
                            }


                            storesList = stores.getData().getStoresDetails();
                            bannerList = stores.getData().getBannerResult();
                            somthingList = stores.getData().getStoreCategories();
                            filtercatList = stores.getData().getFilterCategory();
                            vendorTypesList = stores.getData().getVendorTypes();

                            Log.i(TAG, "store_list: " + storesList.size());

                            mhome_filter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showloaderAlertDialog();

                                }
                            });


                            brandslist.clear();
                            filterslist.clear();
                            bannerslist.clear();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandId())) {
                                    seletedbrands.add(storesList.get(i).getBrandId());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandId());
                                    brands.setBradNameEn(storesList.get(i).getBrandName_En());
                                    brands.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                    brands.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                    brands.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                    brands.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
//                                            if (filterdistance >= storesList.get(i).getDistance()) {
                                            fillterstore.add(storesList.get(j));
//                                            }
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            seletedbanners.clear();
                            for (int i = 0; i < bannerList.size(); i++) {
                                if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                                    seletedbanners.add(bannerList.get(i).getBannerName_En());
                                    Banners banners = new Banners();
                                    banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                                    banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                                    banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                                    ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < bannerList.size(); j++) {
                                        if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                            fillterstore.add(bannerList.get(j));
                                            for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                                ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                                for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                                    if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                                    bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                                    }

                                                }

                                                banners.setBannerList(bannerlist);

                                            }
                                        }
                                    }


                                    banners.setBanners(fillterstore);
                                    bannerslist.add(banners);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            for (int l = 0; l < bannerslist.size(); l++) {
                                if (bannerslist.get(l).getBannerList() == null || bannerslist.get(l).getBannerList().size() == 0) {

                                } else {

                                    Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);

                                }
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerResult());

                            filterseletedbrands.clear();
                            for (int i = 0; i < storesList.size(); i++) {

//                                    if (storesList.get(i).getDiscountamt() > 0) {


                                filterseletedbrands.add(storesList.get(i).getBrandId());
                                Filter filter = new Filter();

                                filter.setBrandId(storesList.get(i).getBrandId());
                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                for (int j = 0; j < storesList.size(); j++) {
//                                    if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                    fillterstore.add(storesList.get(j));

//                                    }
                                }
                                filter.setBrands(fillterstore);
                                filterslist.add(filter);


                            }

                            for (int l = 0; l < filterslist.size(); l++) {
                                Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                            }

                            Log.i(TAG, "brands_size: " + brandslist.size());
                            Log.i(TAG, "filter_size " + filterslist.size());

                            if (getActivity() != null) {
                                if (filterslist.size() != 0) {
                                    storesListView.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < filterslist.size(); i++) {
                                        mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                                        storesListView.setAdapter(mStoreAdapter);
                                    }
                                } else {
                                    storesListView.setVisibility(View.GONE);
                                }
                                bannersViewpage.setAdapter(mBanners);
                                mBanners.notifyDataSetChanged();
                            }


                            if (getActivity() != null) {
//                                if (bannerslist.size() == 0) {
//                                    bannerlayout.setVisibility(View.GONE);
//                                } else {
                                bannerlayout.setVisibility(View.VISIBLE);
                                try {
                                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                                    bannersViewpage.setAdapter(mBanners);
                                    mBanners.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                }
                            }

                            bannersViewpage.setVisibility(View.VISIBLE);
                            for (int i = 0; i < storesList.size(); i++) {

                            }


                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    Log.i(TAG, "onFailure: " + t);
                    if (getActivity() != null) {
                        final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }


    }

    private String prepareGetStoresJSON1() {
        JSONObject parentObj = new JSONObject();

        String filter = null;
        int venderType = 0;

        for (int i = 0; i < filterid.size(); i++) {

            if (filter != null && filter.length() > 0) {

                filter = filter + "," + filterid.get(i);

            } else {

                filter = String.valueOf(filterid.get(i));

            }
        }

        venderType = vendertypeid;

//        for (int i = 0; i < vendertypeid.size(); i++) {
//
//            if (venderType != null && venderType.length() > 0) {
//
//                venderType = venderType + "," + vendertypeid.get(i);
//
//            } else {
//
//                venderType = String.valueOf(vendertypeid.get(i));
//
//            }
//        }

        String distance = "";

        distance = String.valueOf(filterdistance);

        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
            parentObj.put("filterIds", filter);
            parentObj.put("KM", distance);
            parentObj.put("BranchId", "");
            parentObj.put("SearchText", "");
            parentObj.put("pageNumber", "");
            parentObj.put("pageSize", "");
            if (vendertypeid != 0) {
                parentObj.put("VendorType", venderType);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "onResume: " + HomeScreenFragment.filter);

        if (getActivity() != null) {
            bannersViewpage.setVisibility(View.GONE);
            storesListView.setVisibility(View.VISIBLE);
            bannersViewpage.setOffscreenPageLimit(0);
            if (bannerList.size() > 0) {
                try {
                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
                    bannersViewpage.setAdapter(mBanners);
                    mBanners.notifyDataSetChanged();
                    bannersViewpage.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (filter) {

            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new GetFilterApi().execute();

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void showLocationChangedPopup(double updatedLatitude, double updatedLongitude) {

        Location loc1 = new Location("");
        loc1.setLatitude(currentLatitude);
        loc1.setLongitude(currentLongitude);

        Location loc2 = new Location("");
        loc2.setLatitude(updatedLatitude);
        loc2.setLongitude(updatedLongitude);

        float distanceInMeters = loc1.distanceTo(loc2);

        Log.d(TAG, "distanceInMeters: " + distanceInMeters);

//        if (distanceInMeters > 300) {
        try {
            final BubbleLayout bubbleLayout;
            if (language.equalsIgnoreCase("En")) {
                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup, null);
            } else {
                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup_arabic, null);
            }
            final PopupWindow popupWindow = BubblePopupHelper.create(getActivity(), bubbleLayout);

            Button closeBtn = (Button) bubbleLayout.findViewById(R.id.close_button);
            Button changeAddressBtn = (Button) bubbleLayout.findViewById(R.id.change_location_button);
            TextView mbubble_text = (TextView) bubbleLayout.findViewById(R.id.bubble_text);

            Location mLocation = new Location("");
            mLocation.setLatitude(currentLatitude);
            mLocation.setLongitude(currentLongitude);

            Geocoder geocoder = null;
            try {
                geocoder = new Geocoder(context, Locale.getDefault());
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Address found using the Geocoder.
            List<Address> addresses = null;
            String errorMessage = "";

            try {
                addresses = geocoder.getFromLocation(
                        mLocation.getLatitude(),
                        mLocation.getLongitude(),
                        // In this sample, we get just a single address.
                        1);
            } catch (IOException ioException) {
                // Catch network or other I/O problems.
                errorMessage = context.getString(R.string.service_not_available);
                Log.e(TAG, errorMessage, ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                // Catch invalid latitude or longitude values.
                errorMessage = context.getString(R.string.invalid_lat_long_used);
                Log.e(TAG, errorMessage + ". " +
                        "Latitude = " + mLocation.getLatitude() +
                        ", Longitude = " + mLocation.getLongitude(), illegalArgumentException);
            }

            if (addresses == null || addresses.size() == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found);
                    Log.e(TAG, errorMessage);
                }
            } else {
                Address address = addresses.get(0);
                Log.d(TAG, "getLocality: " + address);
//                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

                mbubble_text.setText("" + address.getAddressLine(0));

                Log.d(TAG, "getLocality: " + currentLatitude);
            }

            closeBtn.setVisibility(View.GONE);

            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms

                    popupWindow.dismiss();

                }
            }, 2000);

            changeAddressBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userId = userPrefs.getString("userId", "0");
                    if (userId.equals("0")) {
                        Intent a = new Intent(getActivity(), SignInActivity.class);
                        startActivity(a);
                    } else {
                        Constants.addressboolean = false;
                        Constants.homeboolean = true;
                        Intent intent = new Intent(getContext(), ChangeAddressActivity.class);
                        startActivityForResult(intent, ADDRESS_REQUEST);
                    }

                    popupWindow.dismiss();
                }
            });

            int[] location = new int[2];
            locationArrow.getLocationOnScreen(location);
            bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
            popupWindow.showAtLocation(locationArrow, Gravity.NO_GRAVITY, location[0], locationArrow.getHeight() + location[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        }
    }

    public void ChangeLocationCoordinates() throws IOException {

        if (gps != null) {
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude();
                currentLongitude = gps.getLongitude();

                getLocality();
                if (filter) {
                    new GetFilterApi().execute();
                } else {
                    new GetStoresApi().execute();
                }
            }
        }
    }

    public static class NetworkUtil {

        static int TYPE_WIFI = 1;
        static int TYPE_MOBILE = 2;
        static int TYPE_NOT_CONNECTED = 0;

        public static int getConnectivityStatus(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
            return TYPE_NOT_CONNECTED;
        }

        public static String getConnectivityStatusString(Context context) {
            int conn = NetworkUtil.getConnectivityStatus(context);
            String status = null;
            if (conn == NetworkUtil.TYPE_WIFI) {
                status = "Wifi enabled";
            } else if (conn == NetworkUtil.TYPE_MOBILE) {
                status = "Mobile data enabled";


            } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
                status = "Not connected to Internet";
            }
            return status;
        }

    }

    public class BannersAdapter extends PagerAdapter {

        LayoutInflater mLayoutInflater;
        ArrayList<Banners> bannerList;
        Context context;
        String language;

        public BannersAdapter(Context context, ArrayList<Banners> bannerList, String language) {
            this.context = context;
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.bannerList = bannerList;
            this.language = language;
            notifyDataSetChanged();
        }

        @Override
        public float getPageWidth(int position) {
            float nbPages = 1.0f; // You could display partial pages using a float value
            return (1 / nbPages);
        }

        @Override
        public int getCount() {
            return bannerList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView;
            if (language.equalsIgnoreCase("En")) {
                itemView = mLayoutInflater.inflate(R.layout.banners_stores, container, false);
            } else {
                itemView = mLayoutInflater.inflate(R.layout.banners_stores, container, false);
            }

            final ImageView store_image = (ImageView) itemView.findViewById(R.id.store_image);

            store_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        if (bannerList.get(position).getBannerList().size() == 0) {

                        } else if (bannerList.get(position).getBannerList().size() > 1) {
                            Intent intent = new Intent(context, PromotedViewallScreenActivity.class);
//                        if (language.equalsIgnoreCase("En")) {
                            intent.putExtra("viewaall", "Banners");
//                        } else {
//                            intent.putExtra("viewaall", "لافتات");
//                        }
                            intent.putExtra("array", bannerList.get(position).getBannerList());
                            intent.putExtra("seletpos", position);
                            context.startActivity(intent);
//                        banners arabic لافتات
                            //                    Log.d("TAG", "bannerlistsize" + bannerList.get(0).getBannerList().size());

                        } else {

                            Intent a = new Intent(context, MenuActivity.class);
                            a.putExtra("array", bannerList.get(position).getBannerList());
                            a.putExtra("pos", 0);
                            a.putExtra("banner", "banner");
                            context.startActivity(a);
                            Constants.store_lat = bannerList.get(position).getBannerList().get(0).getLatitude();
                            Constants.store_longi = bannerList.get(position).getBannerList().get(0).getLongitude();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.IMAGE_URL + bannerList.get(position).getBannerimageEn())
                    .into(store_image);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

}
