package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuItems implements Serializable{


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data;
    @Expose
    @SerializedName("Message")
    private String message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("SCat")
        private ArrayList<SCat> scat;
        @Expose
        @SerializedName("MCatIsActive")
        private boolean mcatisactive;
        @Expose
        @SerializedName("MCatDisplaySeqId")
        private int mcatdisplayseqid;
        @Expose
        @SerializedName("MCatImage_ar")
        private String mcatimageAr;
        @Expose
        @SerializedName("MCatImage_en")
        private String mcatimageEn;
        @Expose
        @SerializedName("MCatDesc_ar")
        private String mcatdescAr;
        @Expose
        @SerializedName("MCatDesc_en")
        private String mcatdescEn;
        @Expose
        @SerializedName("MCat_ar")
        private String mcatAr;
        @Expose
        @SerializedName("MCat_en")
        private String mcatEn;
        @Expose
        @SerializedName("MCatCousineId")
        private int mcatcousineid;
        @Expose
        @SerializedName("MCatBranchId")
        private int mcatbranchid;
        @Expose
        @SerializedName("MCatBrandId")
        private int mcatbrandid;
        @Expose
        @SerializedName("MainCatId")
        private int maincatid;

        private boolean isHeader;

        public boolean isHeader() {
            return isHeader;
        }

        public void setHeader(boolean header) {
            isHeader = header;
        }

        public ArrayList<SCat> getScat() {
            return scat;
        }

        public void setScat(ArrayList<SCat> scat) {
            this.scat = scat;
        }

        public boolean getMcatisactive() {
            return mcatisactive;
        }

        public void setMcatisactive(boolean mcatisactive) {
            this.mcatisactive = mcatisactive;
        }

        public int getMcatdisplayseqid() {
            return mcatdisplayseqid;
        }

        public void setMcatdisplayseqid(int mcatdisplayseqid) {
            this.mcatdisplayseqid = mcatdisplayseqid;
        }

        public String getMcatimageAr() {
            return mcatimageAr;
        }

        public void setMcatimageAr(String mcatimageAr) {
            this.mcatimageAr = mcatimageAr;
        }

        public String getMcatimageEn() {
            return mcatimageEn;
        }

        public void setMcatimageEn(String mcatimageEn) {
            this.mcatimageEn = mcatimageEn;
        }

        public String getMcatdescAr() {
            return mcatdescAr;
        }

        public void setMcatdescAr(String mcatdescAr) {
            this.mcatdescAr = mcatdescAr;
        }

        public String getMcatdescEn() {
            return mcatdescEn;
        }

        public void setMcatdescEn(String mcatdescEn) {
            this.mcatdescEn = mcatdescEn;
        }

        public String getMcatAr() {
            return mcatAr;
        }

        public void setMcatAr(String mcatAr) {
            this.mcatAr = mcatAr;
        }

        public String getMcatEn() {
            return mcatEn;
        }

        public void setMcatEn(String mcatEn) {
            this.mcatEn = mcatEn;
        }

        public int getMcatcousineid() {
            return mcatcousineid;
        }

        public void setMcatcousineid(int mcatcousineid) {
            this.mcatcousineid = mcatcousineid;
        }

        public int getMcatbranchid() {
            return mcatbranchid;
        }

        public void setMcatbranchid(int mcatbranchid) {
            this.mcatbranchid = mcatbranchid;
        }

        public int getMcatbrandid() {
            return mcatbrandid;
        }

        public void setMcatbrandid(int mcatbrandid) {
            this.mcatbrandid = mcatbrandid;
        }

        public int getMaincatid() {
            return maincatid;
        }

        public void setMaincatid(int maincatid) {
            this.maincatid = maincatid;
        }
    }

    public static class SCat implements Serializable {
        @Expose
        @SerializedName("Sec")
        private ArrayList<Sec> sec;
        @Expose
        @SerializedName("SCatIsActive")
        private boolean scatisactive;
        @Expose
        @SerializedName("SCatDisplaySeqId")
        private int scatdisplayseqid;
        @Expose
        @SerializedName("SCatImage_ar")
        private String scatimageAr;
        @Expose
        @SerializedName("SCatImage_en")
        private String scatimageEn;
        @Expose
        @SerializedName("SCatDesc_ar")
        private String scatdescAr;
        @Expose
        @SerializedName("SCatDesc_en")
        private String scatdescEn;
        @Expose
        @SerializedName("SCat_ar")
        private String scatAr;
        @Expose
        @SerializedName("SCat_en")
        private String scatEn;
        @Expose
        @SerializedName("MCategoryId")
        private int mcategoryid;
        @Expose
        @SerializedName("SCatBranchId")
        private int scatbranchid;
        @Expose
        @SerializedName("SCatBrandId")
        private int scatbrandid;
        @Expose
        @SerializedName("SCatId")
        private int scatid;

        public ArrayList<Sec> getSec() {
            return sec;
        }

        public void setSec(ArrayList<Sec> sec) {
            this.sec = sec;
        }

        public boolean getScatisactive() {
            return scatisactive;
        }

        public void setScatisactive(boolean scatisactive) {
            this.scatisactive = scatisactive;
        }

        public int getScatdisplayseqid() {
            return scatdisplayseqid;
        }

        public void setScatdisplayseqid(int scatdisplayseqid) {
            this.scatdisplayseqid = scatdisplayseqid;
        }

        public String getScatimageAr() {
            return scatimageAr;
        }

        public void setScatimageAr(String scatimageAr) {
            this.scatimageAr = scatimageAr;
        }

        public String getScatimageEn() {
            return scatimageEn;
        }

        public void setScatimageEn(String scatimageEn) {
            this.scatimageEn = scatimageEn;
        }

        public String getScatdescAr() {
            return scatdescAr;
        }

        public void setScatdescAr(String scatdescAr) {
            this.scatdescAr = scatdescAr;
        }

        public String getScatdescEn() {
            return scatdescEn;
        }

        public void setScatdescEn(String scatdescEn) {
            this.scatdescEn = scatdescEn;
        }

        public String getScatAr() {
            return scatAr;
        }

        public void setScatAr(String scatAr) {
            this.scatAr = scatAr;
        }

        public String getScatEn() {
            return scatEn;
        }

        public void setScatEn(String scatEn) {
            this.scatEn = scatEn;
        }

        public int getMcategoryid() {
            return mcategoryid;
        }

        public void setMcategoryid(int mcategoryid) {
            this.mcategoryid = mcategoryid;
        }

        public int getScatbranchid() {
            return scatbranchid;
        }

        public void setScatbranchid(int scatbranchid) {
            this.scatbranchid = scatbranchid;
        }

        public int getScatbrandid() {
            return scatbrandid;
        }

        public void setScatbrandid(int scatbrandid) {
            this.scatbrandid = scatbrandid;
        }

        public int getScatid() {
            return scatid;
        }

        public void setScatid(int scatid) {
            this.scatid = scatid;
        }
    }

    public static class Sec implements Serializable {
        @Expose
        @SerializedName("ITM")
        private ArrayList<ITM> itm;
        @Expose
        @SerializedName("SectionName_ar")
        private String sectionnameAr;
        @Expose
        @SerializedName("SectionName_en")
        private String sectionnameEn;
        @Expose
        @SerializedName("SectionId")
        private int sectionid;

        public ArrayList<ITM> getItm() {
            return itm;
        }

        public void setItm(ArrayList<ITM> itm) {
            this.itm = itm;
        }

        public String getSectionnameAr() {
            return sectionnameAr;
        }

        public void setSectionnameAr(String sectionnameAr) {
            this.sectionnameAr = sectionnameAr;
        }

        public String getSectionnameEn() {
            return sectionnameEn;
        }

        public void setSectionnameEn(String sectionnameEn) {
            this.sectionnameEn = sectionnameEn;
        }

        public int getSectionid() {
            return sectionid;
        }

        public void setSectionid(int sectionid) {
            this.sectionid = sectionid;
        }
    }

    public static class ITM implements Serializable {
        @Expose
        @SerializedName("IPRC")
        private ArrayList<IPRC> iprc;
        @Expose
        @SerializedName("SubCategoryId")
        private int subcategoryid;
        @Expose
        @SerializedName("CategoryId")
        private int categoryid;
        @Expose
        @SerializedName("IsDeliver")
        private boolean isdeliver;
        @Expose
        @SerializedName("ItemSeq")
        private int itemseq;
        @Expose
        @SerializedName("BindingType")
        private int bindingtype;
        @Expose
        @SerializedName("ItemImage")
        private String itemimage;
        @Expose
        @SerializedName("DisplayNameId")
        private int displaynameid;
        @Expose
        @SerializedName("ItemDesc_ar")
        private String itemdescAr;
        @Expose
        @SerializedName("ItemDesc_en")
        private String itemdescEn;
        @Expose
        @SerializedName("ItemName_Ar")
        private String itemnameAr;
        @Expose
        @SerializedName("ItemName_en")
        private String itemnameEn;
        @Expose
        @SerializedName("ItemId")
        private int itemid;

        public ArrayList<IPRC> getIprc() {
            return iprc;
        }

        public void setIprc(ArrayList<IPRC> iprc) {
            this.iprc = iprc;
        }

        public int getSubcategoryid() {
            return subcategoryid;
        }

        public void setSubcategoryid(int subcategoryid) {
            this.subcategoryid = subcategoryid;
        }

        public int getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(int categoryid) {
            this.categoryid = categoryid;
        }

        public boolean getIsdeliver() {
            return isdeliver;
        }

        public void setIsdeliver(boolean isdeliver) {
            this.isdeliver = isdeliver;
        }

        public int getItemseq() {
            return itemseq;
        }

        public void setItemseq(int itemseq) {
            this.itemseq = itemseq;
        }

        public int getBindingtype() {
            return bindingtype;
        }

        public void setBindingtype(int bindingtype) {
            this.bindingtype = bindingtype;
        }

        public String getItemimage() {
            return itemimage;
        }

        public void setItemimage(String itemimage) {
            this.itemimage = itemimage;
        }

        public int getDisplaynameid() {
            return displaynameid;
        }

        public void setDisplaynameid(int displaynameid) {
            this.displaynameid = displaynameid;
        }

        public String getItemdescAr() {
            return itemdescAr;
        }

        public void setItemdescAr(String itemdescAr) {
            this.itemdescAr = itemdescAr;
        }

        public String getItemdescEn() {
            return itemdescEn;
        }

        public void setItemdescEn(String itemdescEn) {
            this.itemdescEn = itemdescEn;
        }

        public String getItemnameAr() {
            return itemnameAr;
        }

        public void setItemnameAr(String itemnameAr) {
            this.itemnameAr = itemnameAr;
        }

        public String getItemnameEn() {
            return itemnameEn;
        }

        public void setItemnameEn(String itemnameEn) {
            this.itemnameEn = itemnameEn;
        }

        public int getItemid() {
            return itemid;
        }

        public void setItemid(int itemid) {
            this.itemid = itemid;
        }
    }

    public static class IPRC implements Serializable {
        @Expose
        @SerializedName("Vart")
        private ArrayList<Vart> vart;
        @Expose
        @SerializedName("IsActive")
        private boolean isactive;
        @Expose
        @SerializedName("Caffien")
        private String caffien;
        @Expose
        @SerializedName("Protein")
        private String protein;
        @Expose
        @SerializedName("DietaryFiber")
        private String dietaryfiber;
        @Expose
        @SerializedName("TotalCarboHydrate")
        private String totalcarbohydrate;
        @Expose
        @SerializedName("Sodium")
        private String sodium;
        @Expose
        @SerializedName("Cholestrol")
        private String cholestrol;
        @Expose
        @SerializedName("TransFat")
        private String transfat;
        @Expose
        @SerializedName("TotalFat")
        private String totalfat;
        @Expose
        @SerializedName("SaturatedFat")
        private String saturatedfat;
        @Expose
        @SerializedName("Calories")
        private String calories;
        @Expose
        @SerializedName("ItemSizeId")
        private int itemsizeid;
        @Expose
        @SerializedName("ItemTypeId")
        private int itemtypeid;
        @Expose
        @SerializedName("ItemPrice")
        private int itemprice;
        @Expose
        @SerializedName("PriceId")
        private int priceid;

        public ArrayList<Vart> getVart() {
            return vart;
        }

        public void setVart(ArrayList<Vart> vart) {
            this.vart = vart;
        }

        public boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(boolean isactive) {
            this.isactive = isactive;
        }

        public String getCaffien() {
            return caffien;
        }

        public void setCaffien(String caffien) {
            this.caffien = caffien;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getDietaryfiber() {
            return dietaryfiber;
        }

        public void setDietaryfiber(String dietaryfiber) {
            this.dietaryfiber = dietaryfiber;
        }

        public String getTotalcarbohydrate() {
            return totalcarbohydrate;
        }

        public void setTotalcarbohydrate(String totalcarbohydrate) {
            this.totalcarbohydrate = totalcarbohydrate;
        }

        public String getSodium() {
            return sodium;
        }

        public void setSodium(String sodium) {
            this.sodium = sodium;
        }

        public String getCholestrol() {
            return cholestrol;
        }

        public void setCholestrol(String cholestrol) {
            this.cholestrol = cholestrol;
        }

        public String getTransfat() {
            return transfat;
        }

        public void setTransfat(String transfat) {
            this.transfat = transfat;
        }

        public String getTotalfat() {
            return totalfat;
        }

        public void setTotalfat(String totalfat) {
            this.totalfat = totalfat;
        }

        public String getSaturatedfat() {
            return saturatedfat;
        }

        public void setSaturatedfat(String saturatedfat) {
            this.saturatedfat = saturatedfat;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public int getItemsizeid() {
            return itemsizeid;
        }

        public void setItemsizeid(int itemsizeid) {
            this.itemsizeid = itemsizeid;
        }

        public int getItemtypeid() {
            return itemtypeid;
        }

        public void setItemtypeid(int itemtypeid) {
            this.itemtypeid = itemtypeid;
        }

        public int getItemprice() {
            return itemprice;
        }

        public void setItemprice(int itemprice) {
            this.itemprice = itemprice;
        }

        public int getPriceid() {
            return priceid;
        }

        public void setPriceid(int priceid) {
            this.priceid = priceid;
        }
    }

    public static class Vart implements Serializable {
        @Expose
        @SerializedName("VarientId")
        private int varientid;
        @Expose
        @SerializedName("Name_ar")
        private String nameAr;
        @Expose
        @SerializedName("Name_en")
        private String nameEn;

        public int getVarientid() {
            return varientid;
        }

        public void setVarientid(int varientid) {
            this.varientid = varientid;
        }

        public String getNameAr() {
            return nameAr;
        }

        public void setNameAr(String nameAr) {
            this.nameAr = nameAr;
        }

        public String getNameEn() {
            return nameEn;
        }

        public void setNameEn(String nameEn) {
            this.nameEn = nameEn;
        }
    }
}
