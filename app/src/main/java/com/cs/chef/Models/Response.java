package com.cs.chef.Models;

import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("Data")
    private Data Data;
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {

    }
}
