package com.cs.chef.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class SubitemsSearchFilter implements Parcelable {

    String price, itemimage, itemdescAr, itemdescEn, itemnameAr, itemnameEn, itemid;

    public SubitemsSearchFilter(){

    }

    public SubitemsSearchFilter(Parcel in) {
        price = in.readString();
        itemimage = in.readString();
        itemdescAr = in.readString();
        itemdescEn = in.readString();
        itemnameAr = in.readString();
        itemnameEn = in.readString();
        itemid = in.readString();
    }

    public static final Creator<StoreMenuextendsubitems> CREATOR = new Creator<StoreMenuextendsubitems>() {
        @Override
        public StoreMenuextendsubitems createFromParcel(Parcel in) {
            return new StoreMenuextendsubitems(in);
        }

        @Override
        public StoreMenuextendsubitems[] newArray(int size) {
            return new StoreMenuextendsubitems[size];
        }
    };

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getItemimage() {
        return itemimage;
    }

    public void setItemimage(String itemimage) {
        this.itemimage = itemimage;
    }

    public String getItemdescAr() {
        return itemdescAr;
    }

    public void setItemdescAr(String itemdescAr) {
        this.itemdescAr = itemdescAr;
    }

    public String getItemdescEn() {
        return itemdescEn;
    }

    public void setItemdescEn(String itemdescEn) {
        this.itemdescEn = itemdescEn;
    }

    public String getItemnameAr() {
        return itemnameAr;
    }

    public void setItemnameAr(String itemnameAr) {
        this.itemnameAr = itemnameAr;
    }

    public String getItemnameEn() {
        return itemnameEn;
    }

    public void setItemnameEn(String itemnameEn) {
        this.itemnameEn = itemnameEn;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(price);
        dest.writeString(itemimage);
        dest.writeString(itemdescAr);
        dest.writeString(itemdescEn);
        dest.writeString(itemnameAr);
        dest.writeString(itemnameEn);
        dest.writeString(itemid);
    }
}
