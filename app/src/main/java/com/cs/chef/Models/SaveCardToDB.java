package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveCardToDB {


    @Expose
    @SerializedName("StatusMessage")
    private String StatusMessage;
    @Expose
    @SerializedName("StatusCode")
    private String StatusCode;
    @Expose
    @SerializedName("DeletedDate")
    private String DeletedDate;
    @Expose
    @SerializedName("CreatedDate")
    private String CreatedDate;
    @Expose
    @SerializedName("IsActive")
    private boolean IsActive;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("Id")
    private int Id;

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getDeletedDate() {
        return DeletedDate;
    }

    public void setDeletedDate(String DeletedDate) {
        this.DeletedDate = DeletedDate;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
}
