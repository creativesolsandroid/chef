package com.cs.chef.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Banners implements Serializable {

    private String BannerNameEn;
    private String BannerNameAr;
    private String BannerimageEn;

    private ArrayList<StoresList.BannerResult> banners;

    private ArrayList<StoresList.StoreDetails> bannerList;

    public ArrayList<StoresList.BannerResult> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<StoresList.BannerResult> banner) {
        this.banners = banner;
    }

    public ArrayList<StoresList.StoreDetails> getBannerList() {
        return bannerList;
    }

    public void setBannerList(ArrayList<StoresList.StoreDetails> bannerList) {
        this.bannerList = bannerList;
    }

    public String getBannerNameEn() {
        return BannerNameEn;
    }

    public void setBannerNameEn(String BannerNameEn) {
        BannerNameEn = BannerNameEn;
    }

    public String getBannerNameAr() {
        return BannerNameAr;
    }

    public void setBannerNameAr(String bannerNameAr) {
        BannerNameAr = bannerNameAr;
    }

    public String getBannerimageEn() {
        return BannerimageEn;
    }

    public void setBrandimageEn(String bannerimageEn) {
        BannerimageEn = bannerimageEn;
    }


    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoresList.StoreDetails> distanceSort = new Comparator<StoresList.StoreDetails>() {

        public int compare(StoresList.StoreDetails s1, StoresList.StoreDetails s2) {

            Double rollno1 = (s1.getDistance());
            Double rollno2 = (s2.getDistance());

            /*For ascending order*/
            return Double.compare(rollno1,rollno2);

            /*For descending order*/
            //rollno2-rollno1;
        }};

    public static Comparator<StoresList.StoreDetails> Rating = new Comparator<StoresList.StoreDetails>() {

        public int compare(StoresList.StoreDetails s1, StoresList.StoreDetails s2) {

            Double rollno1 = Double.valueOf((s1.getRating()));
            Double rollno2 = Double.valueOf((s2.getRating()));

            /*For ascending order*/
            return Double.compare(rollno1,rollno2);

            /*For descending order*/
            //rollno2-rollno1;
        }};

    public static Comparator<StoresList.StoreDetails> Price = new Comparator<StoresList.StoreDetails>() {

        public int compare(StoresList.StoreDetails s1, StoresList.StoreDetails s2) {

            Double rollno1 = Double.valueOf((s1.getRating()));
            Double rollno2 = Double.valueOf((s2.getRating()));

            /*For ascending order*/
            return Double.compare(rollno1,rollno2);

            /*For descending order*/
            //rollno2-rollno1;
        }};

}

