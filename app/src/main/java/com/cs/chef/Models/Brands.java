package com.cs.chef.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Brands implements Serializable {


    private  int BrandId;
    private String BradNameEn;
    private String BrandNameAr;
    private String BrandimageEn;
    private String BranddescAr;
    private String BranddescEn;

    private ArrayList<StoresList.StoresDetails> brands;


    public ArrayList<StoresList.StoresDetails> getBrands() {
        return brands;
    }

    public void setBrands(ArrayList<StoresList.StoresDetails> brands) {
        this.brands = brands;
    }

    public int isBrandId() {
        return BrandId;
    }

    public void setBrandId(int brandId) {
        BrandId = brandId;
    }

    public String getBradNameEn() {
        return BradNameEn;
    }

    public void setBradNameEn(String bradNameEn) {
        BradNameEn = bradNameEn;
    }

    public String getBrandNameAr() {
        return BrandNameAr;
    }

    public void setBrandNameAr(String brandNameAr) {
        BrandNameAr = brandNameAr;
    }

    public String getBrandimageEn() {
        return BrandimageEn;
    }

    public void setBrandimageEn(String brandimageEn) {
        BrandimageEn = brandimageEn;
    }

    public String getBranddescAr() {
        return BranddescAr;
    }

    public void setBranddescAr(String branddescAr) {
        BranddescAr = branddescAr;
    }

    public String getBranddescEn() {
        return BranddescEn;
    }

    public void setBranddescEn(String branddescEn) {
        BranddescEn = branddescEn;
    }


    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoresList.StoresDetails> distanceSort = new Comparator<StoresList.StoresDetails>() {

        public int compare(StoresList.StoresDetails s1, StoresList.StoresDetails s2) {

            Double rollno1 = (s1.getDistance());
            Double rollno2 = (s2.getDistance());

            /*For ascending order*/
            return Double.compare(rollno1,rollno2);

            /*For descending order*/
            //rollno2-rollno1;
        }};

    public static Comparator<StoresList.StoresDetails> Rating = new Comparator<StoresList.StoresDetails>() {

        public int compare(StoresList.StoresDetails s1, StoresList.StoresDetails s2) {

            Double rollno1 = Double.valueOf((s1.getRating()));
            Double rollno2 = Double.valueOf((s2.getRating()));

            /*For ascending order*/
            return Double.compare(rollno1,rollno2);

            /*For descending order*/
            //rollno2-rollno1;
        }};

    public static Comparator<StoresList.StoresDetails> Price = new Comparator<StoresList.StoresDetails>() {

        public int compare(StoresList.StoresDetails s1, StoresList.StoresDetails s2) {

            Double rollno1 = Double.valueOf((s1.getRating()));
            Double rollno2 = Double.valueOf((s2.getRating()));

            /*For ascending order*/
            return Double.compare(rollno1,rollno2);

            /*For descending order*/
            //rollno2-rollno1;
        }};

}
