package com.cs.chef.Models;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;

public class StoreMenuextenditems extends ExpandableGroup<StoreMenuextendsubitems> {

    private ArrayList<StoreMenuextendsubitems> menuitem = new ArrayList<>();
    String catimage, catdescAr, catdescEn, categorynameAr, categorynameEn, categoryid;

    public StoreMenuextenditems(String title, ArrayList<StoreMenuextendsubitems> items) {
        super(title, items);
    }


    public ArrayList<StoreMenuextendsubitems> getMenuitem() {
        return menuitem;
    }

    public void setMenuitem(ArrayList<StoreMenuextendsubitems> menuitem) {
        this.menuitem = menuitem;
    }

    public String getCatimage() {
        return catimage;
    }

    public void setCatimage(String catimage) {
        this.catimage = catimage;
    }

    public String getCatdescAr() {
        return catdescAr;
    }

    public void setCatdescAr(String catdescAr) {
        this.catdescAr = catdescAr;
    }

    public String getCatdescEn() {
        return catdescEn;
    }

    public void setCatdescEn(String catdescEn) {
        this.catdescEn = catdescEn;
    }

    public String getCategorynameAr() {
        return categorynameAr;
    }

    public void setCategorynameAr(String categorynameAr) {
        this.categorynameAr = categorynameAr;
    }

    public String getCategorynameEn() {
        return categorynameEn;
    }

    public void setCategorynameEn(String categorynameEn) {
        this.categorynameEn = categorynameEn;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }
}
