package com.cs.chef.Models;

import java.io.Serializable;

public class MenuItemSearch implements Serializable {

    String item_name_en, item_name_ar, item_desc_en, item_desc_ar, item_image;
    double item_price;
    int itemid;


    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public String getItem_name_en() {
        return item_name_en;
    }

    public void setItem_name_en(String item_name_en) {
        this.item_name_en = item_name_en;
    }

    public String getItem_name_ar() {
        return item_name_ar;
    }

    public void setItem_name_ar(String item_name_ar) {
        this.item_name_ar = item_name_ar;
    }

    public String getItem_desc_en() {
        return item_desc_en;
    }

    public void setItem_desc_en(String item_desc_en) {
        this.item_desc_en = item_desc_en;
    }

    public String getItem_desc_ar() {
        return item_desc_ar;
    }

    public void setItem_desc_ar(String item_desc_ar) {
        this.item_desc_ar = item_desc_ar;
    }

    public String getItem_image() {
        return item_image;
    }

    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }

    public double getItem_price() {
        return item_price;
    }

    public void setItem_price(double item_price) {
        this.item_price = item_price;
    }

}
