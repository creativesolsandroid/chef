package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StorePaginationList implements Serializable {

        @Expose
        @SerializedName("TotalRows")
        private int TotalRows;
        @Expose
        @SerializedName("PriceCategory")
        private String PriceCategory;
        @Expose
        @SerializedName("DiscountAmt")
        private int DiscountAmt;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("StoreType_Ar")
        private String StoreType_Ar;
        @Expose
        @SerializedName("StoreType")
        private String StoreType;
        @Expose
        @SerializedName("OrderTypes")
        private String OrderTypes;
        @Expose
        @SerializedName("RatingCount")
        private int RatingCount;
        @Expose
        @SerializedName("PromotedBranch_Ar")
        private String PromotedBranch_Ar;
        @Expose
        @SerializedName("PromotedBranch_En")
        private String PromotedBranch_En;
        @Expose
        @SerializedName("IsNew")
        private boolean IsNew;
        @Expose
        @SerializedName("Popular")
        private boolean Popular;
        @Expose
        @SerializedName("IsPromoted")
        private boolean IsPromoted;
        @Expose
        @SerializedName("FutureOrderDay")
        private int FutureOrderDay;
        @Expose
        @SerializedName("Serving")
        private int Serving;
        @Expose
        @SerializedName("AvgPreparationTime")
        private int AvgPreparationTime;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("MinOrderCharges")
        private double MinOrderCharges;
        @Expose
        @SerializedName("Longitude")
        private double Longitude;
        @Expose
        @SerializedName("Latitude")
        private double Latitude;
        @Expose
        @SerializedName("Rating")
        private String Rating;
        @Expose
        @SerializedName("Distance")
        private double Distance;
        @Expose
        @SerializedName("StoreImage_En")
        private String StoreImage_En;
        @Expose
        @SerializedName("StoreLogo_En")
        private String StoreLogo_En;
        @Expose
        @SerializedName("StoreStatus")
        private String StoreStatus;
        @Expose
        @SerializedName("CurrentDate")
        private String CurrentDate;
        @Expose
        @SerializedName("TakeAwayDistance")
        private int TakeAwayDistance;
        @Expose
        @SerializedName("DeliveryDistance")
        private int DeliveryDistance;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("TelephoneNo")
        private String TelephoneNo;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("BranchCode")
        private String BranchCode;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("EndDateTime")
        private String EndDateTime;
        @Expose
        @SerializedName("StarDateTime")
        private String StarDateTime;
        @Expose
        @SerializedName("BranchDescription_Ar")
        private String BranchDescription_Ar;
        @Expose
        @SerializedName("BranchDescription_En")
        private String BranchDescription_En;
        @Expose
        @SerializedName("BrandName_Ar")
        private String BrandName_Ar;
        @Expose
        @SerializedName("BrandName_En")
        private String BrandName_En;
        @Expose
        @SerializedName("BrandId")
        private int BrandId;
        @Expose
        @SerializedName("BranchName_Ar")
        private String BranchName_Ar;
        @Expose
        @SerializedName("BranchName_En")
        private String BranchName_En;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("WeekNo")
        private int WeekNo;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getTotalRows() {
            return TotalRows;
        }

        public void setTotalRows(int TotalRows) {
            this.TotalRows = TotalRows;
        }

        public String getPriceCategory() {
            return PriceCategory;
        }

        public void setPriceCategory(String PriceCategory) {
            this.PriceCategory = PriceCategory;
        }

        public int getDiscountAmt() {
            return DiscountAmt;
        }

        public void setDiscountAmt(int DiscountAmt) {
            this.DiscountAmt = DiscountAmt;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public String getStoreType_Ar() {
            return StoreType_Ar;
        }

        public void setStoreType_Ar(String StoreType_Ar) {
            this.StoreType_Ar = StoreType_Ar;
        }

        public String getStoreType() {
            return StoreType;
        }

        public void setStoreType(String StoreType) {
            this.StoreType = StoreType;
        }

        public String getOrderTypes() {
            return OrderTypes;
        }

        public void setOrderTypes(String OrderTypes) {
            this.OrderTypes = OrderTypes;
        }

        public int getRatingCount() {
            return RatingCount;
        }

        public void setRatingCount(int RatingCount) {
            this.RatingCount = RatingCount;
        }

        public String getPromotedBranch_Ar() {
            return PromotedBranch_Ar;
        }

        public void setPromotedBranch_Ar(String PromotedBranch_Ar) {
            this.PromotedBranch_Ar = PromotedBranch_Ar;
        }

        public String getPromotedBranch_En() {
            return PromotedBranch_En;
        }

        public void setPromotedBranch_En(String PromotedBranch_En) {
            this.PromotedBranch_En = PromotedBranch_En;
        }

        public boolean getIsNew() {
            return IsNew;
        }

        public void setIsNew(boolean IsNew) {
            this.IsNew = IsNew;
        }

        public boolean getPopular() {
            return Popular;
        }

        public void setPopular(boolean Popular) {
            this.Popular = Popular;
        }

        public boolean getIsPromoted() {
            return IsPromoted;
        }

        public void setIsPromoted(boolean IsPromoted) {
            this.IsPromoted = IsPromoted;
        }

        public int getFutureOrderDay() {
            return FutureOrderDay;
        }

        public void setFutureOrderDay(int FutureOrderDay) {
            this.FutureOrderDay = FutureOrderDay;
        }

        public int getServing() {
            return Serving;
        }

        public void setServing(int Serving) {
            this.Serving = Serving;
        }

        public int getAvgPreparationTime() {
            return AvgPreparationTime;
        }

        public void setAvgPreparationTime(int AvgPreparationTime) {
            this.AvgPreparationTime = AvgPreparationTime;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public double getMinOrderCharges() {
            return MinOrderCharges;
        }

        public void setMinOrderCharges(double MinOrderCharges) {
            this.MinOrderCharges = MinOrderCharges;
        }

        public double getLongitude() {
            return Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLatitude() {
            return Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public String getRating() {
            return Rating;
        }

        public void setRating(String Rating) {
            this.Rating = Rating;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getStoreImage_En() {
            return StoreImage_En;
        }

        public void setStoreImage_En(String StoreImage_En) {
            this.StoreImage_En = StoreImage_En;
        }

        public String getStoreLogo_En() {
            return StoreLogo_En;
        }

        public void setStoreLogo_En(String StoreLogo_En) {
            this.StoreLogo_En = StoreLogo_En;
        }

        public String getStoreStatus() {
            return StoreStatus;
        }

        public void setStoreStatus(String StoreStatus) {
            this.StoreStatus = StoreStatus;
        }

        public String getCurrentDate() {
            return CurrentDate;
        }

        public void setCurrentDate(String CurrentDate) {
            this.CurrentDate = CurrentDate;
        }

        public int getTakeAwayDistance() {
            return TakeAwayDistance;
        }

        public void setTakeAwayDistance(int TakeAwayDistance) {
            this.TakeAwayDistance = TakeAwayDistance;
        }

        public int getDeliveryDistance() {
            return DeliveryDistance;
        }

        public void setDeliveryDistance(int DeliveryDistance) {
            this.DeliveryDistance = DeliveryDistance;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getTelephoneNo() {
            return TelephoneNo;
        }

        public void setTelephoneNo(String TelephoneNo) {
            this.TelephoneNo = TelephoneNo;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getBranchCode() {
            return BranchCode;
        }

        public void setBranchCode(String BranchCode) {
            this.BranchCode = BranchCode;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String CurrentDateTime) {
            this.CurrentDateTime = CurrentDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public String getStarDateTime() {
            return StarDateTime;
        }

        public void setStarDateTime(String StarDateTime) {
            this.StarDateTime = StarDateTime;
        }

        public String getBranchDescription_Ar() {
            return BranchDescription_Ar;
        }

        public void setBranchDescription_Ar(String BranchDescription_Ar) {
            this.BranchDescription_Ar = BranchDescription_Ar;
        }

        public String getBranchDescription_En() {
            return BranchDescription_En;
        }

        public void setBranchDescription_En(String BranchDescription_En) {
            this.BranchDescription_En = BranchDescription_En;
        }

        public String getBrandName_Ar() {
            return BrandName_Ar;
        }

        public void setBrandName_Ar(String BrandName_Ar) {
            this.BrandName_Ar = BrandName_Ar;
        }

        public String getBrandName_En() {
            return BrandName_En;
        }

        public void setBrandName_En(String BrandName_En) {
            this.BrandName_En = BrandName_En;
        }

        public int getBrandId() {
            return BrandId;
        }

        public void setBrandId(int BrandId) {
            this.BrandId = BrandId;
        }

        public String getBranchName_Ar() {
            return BranchName_Ar;
        }

        public void setBranchName_Ar(String BranchName_Ar) {
            this.BranchName_Ar = BranchName_Ar;
        }

        public String getBranchName_En() {
            return BranchName_En;
        }

        public void setBranchName_En(String BranchName_En) {
            this.BranchName_En = BranchName_En;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

}
