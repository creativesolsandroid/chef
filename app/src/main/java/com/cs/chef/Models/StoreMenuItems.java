package com.cs.chef.Models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreMenuItems implements Serializable {


    @SerializedName("Data")
    private Data data;
    @SerializedName("Message")
    private String message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @SerializedName("Status")
    private boolean status;

    public Data getData() {

        return data;
    }

    public void setData(Data data)
    {
        this.data = data;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @SerializedName("MenuItems")
        private ArrayList<MenuItems> menuitems;

        public ArrayList<MenuItems> getMenuitems() {
            return menuitems;
        }

        public void setMenuitems(ArrayList<MenuItems> menuitems) {
            this.menuitems = menuitems;
        }
    }

    public static class MenuItems implements Serializable {
        @SerializedName("MenuItems")
        private ArrayList<MenuItem> menuitem;
        @SerializedName("CatImage")
        private String catimage;
        @SerializedName("CatDesc_Ar")
        private String catdescAr;
        @SerializedName("CatDesc_En")
        private String catdescEn;
        @SerializedName("CategoryName_Ar")
        private String categorynameAr;
        @SerializedName("CategoryName_En")
        private String categorynameEn;
        @SerializedName("CategoryId")
        private int categoryid;

        public ArrayList<MenuItem> getMenuitems() {
            return menuitem;
        }

        public void setMenuitems(ArrayList<MenuItem> menuitem) {
            this.menuitem = menuitem;
        }

        public String getCatimage() {
            return catimage;
        }

        public void setCatimage(String catimage) {
            this.catimage = catimage;
        }

        public String getCatdescAr() {
            return catdescAr;
        }

        public void setCatdescAr(String catdescAr) {
            this.catdescAr = catdescAr;
        }

        public String getCatdescEn() {
            return catdescEn;
        }

        public void setCatdescEn(String catdescEn) {
            this.catdescEn = catdescEn;
        }

        public String getCategorynameAr() {
            return categorynameAr;
        }

        public void setCategorynameAr(String categorynameAr) {
            this.categorynameAr = categorynameAr;
        }

        public String getCategorynameEn() {
            return categorynameEn;
        }

        public void setCategorynameEn(String categorynameEn) {
            this.categorynameEn = categorynameEn;
        }

        public int getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(int categoryid) {
            this.categoryid = categoryid;
        }
    }

    public static class MenuItem implements Serializable {
        @SerializedName("Price")
        private double price;
        @SerializedName("ItemImage")
        private String itemimage;
        @SerializedName("ItemDesc_Ar")
        private String itemdescAr;
        @SerializedName("ItemDesc_En")
        private String itemdescEn;
        @SerializedName("ItemName_Ar")
        private String itemnameAr;
        @SerializedName("ItemName_En")
        private String itemnameEn;
        @SerializedName("ItemId")
        private int itemid;

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getItemimage() {
            return itemimage;
        }

        public void setItemimage(String itemimage) {
            this.itemimage = itemimage;
        }

        public String getItemdescAr() {
            return itemdescAr;
        }

        public void setItemdescAr(String itemdescAr) {
            this.itemdescAr = itemdescAr;
        }

        public String getItemdescEn() {
            return itemdescEn;
        }

        public void setItemdescEn(String itemdescEn) {
            this.itemdescEn = itemdescEn;
        }

        public String getItemnameAr() {
            return itemnameAr;
        }

        public void setItemnameAr(String itemnameAr) {
            this.itemnameAr = itemnameAr;
        }

        public String getItemnameEn() {
            return itemnameEn;
        }

        public void setItemnameEn(String itemnameEn) {
            this.itemnameEn = itemnameEn;
        }

        public int getItemid() {
            return itemid;
        }

        public void setItemid(int itemid) {
            this.itemid = itemid;
        }
    }
}
