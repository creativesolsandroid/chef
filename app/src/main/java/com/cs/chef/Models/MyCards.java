package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MyCards implements Serializable {

    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("customPayment")
        private String customPayment;
        @Expose
        @SerializedName("ndc")
        private String ndc;
        @Expose
        @SerializedName("timestamp")
        private String timestamp;
        @Expose
        @SerializedName("buildNumber")
        private String buildNumber;
        @Expose
        @SerializedName("risk")
        private Risk risk;
        @Expose
        @SerializedName("customParameters")
        private CustomParameters customParameters;
        @Expose
        @SerializedName("customer")
        private Customer customer;
        @Expose
        @SerializedName("card")
        private Card card;
        @Expose
        @SerializedName("resultDetails")
        private ResultDetails resultDetails;
        @Expose
        @SerializedName("result")
        private Result result;
        @Expose
        @SerializedName("merchantTransactionId")
        private String merchantTransactionId;
        @Expose
        @SerializedName("descriptor")
        private String descriptor;
        @Expose
        @SerializedName("currency")
        private String currency;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("paymentBrand")
        private String paymentBrand;
        @Expose
        @SerializedName("paymentType")
        private String paymentType;
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("registrationId")
        private String registrationId = "";

        public String getRegistrationId() {
            return registrationId;
        }

        public void setRegistrationId(String registrationId) {
            this.registrationId = registrationId;
        }

        public String getCustomPayment() {
            return customPayment;
        }

        public void setCustomPayment(String customPayment) {
            this.customPayment = customPayment;
        }

        public String getNdc() {
            return ndc;
        }

        public void setNdc(String ndc) {
            this.ndc = ndc;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getBuildNumber() {
            return buildNumber;
        }

        public void setBuildNumber(String buildNumber) {
            this.buildNumber = buildNumber;
        }

        public Risk getRisk() {
            return risk;
        }

        public void setRisk(Risk risk) {
            this.risk = risk;
        }

        public CustomParameters getCustomParameters() {
            return customParameters;
        }

        public void setCustomParameters(CustomParameters customParameters) {
            this.customParameters = customParameters;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public Card getCard() {
            return card;
        }

        public void setCard(Card card) {
            this.card = card;
        }

        public ResultDetails getResultDetails() {
            return resultDetails;
        }

        public void setResultDetails(ResultDetails resultDetails) {
            this.resultDetails = resultDetails;
        }

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

        public String getMerchantTransactionId() {
            return merchantTransactionId;
        }

        public void setMerchantTransactionId(String merchantTransactionId) {
            this.merchantTransactionId = merchantTransactionId;
        }

        public String getDescriptor() {
            return descriptor;
        }

        public void setDescriptor(String descriptor) {
            this.descriptor = descriptor;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getPaymentBrand() {
            return paymentBrand;
        }

        public void setPaymentBrand(String paymentBrand) {
            this.paymentBrand = paymentBrand;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Risk {
        @Expose
        @SerializedName("score")
        private String score;

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }
    }

    public static class CustomParameters {
        @Expose
        @SerializedName("SHOPPER_MSDKVersion")
        private String SHOPPER_MSDKVersion;
        @Expose
        @SerializedName("SHOPPER_OS")
        private String SHOPPER_OS;
        @Expose
        @SerializedName("CTPE_DESCRIPTOR_TEMPLATE")
        private String CTPE_DESCRIPTOR_TEMPLATE;
        @Expose
        @SerializedName("SHOPPER_device")
        private String SHOPPER_device;
        @Expose
        @SerializedName("SHOPPER_MSDKIntegrationType")
        private String SHOPPER_MSDKIntegrationType;

        public String getSHOPPER_MSDKVersion() {
            return SHOPPER_MSDKVersion;
        }

        public void setSHOPPER_MSDKVersion(String SHOPPER_MSDKVersion) {
            this.SHOPPER_MSDKVersion = SHOPPER_MSDKVersion;
        }

        public String getSHOPPER_OS() {
            return SHOPPER_OS;
        }

        public void setSHOPPER_OS(String SHOPPER_OS) {
            this.SHOPPER_OS = SHOPPER_OS;
        }

        public String getCTPE_DESCRIPTOR_TEMPLATE() {
            return CTPE_DESCRIPTOR_TEMPLATE;
        }

        public void setCTPE_DESCRIPTOR_TEMPLATE(String CTPE_DESCRIPTOR_TEMPLATE) {
            this.CTPE_DESCRIPTOR_TEMPLATE = CTPE_DESCRIPTOR_TEMPLATE;
        }

        public String getSHOPPER_device() {
            return SHOPPER_device;
        }

        public void setSHOPPER_device(String SHOPPER_device) {
            this.SHOPPER_device = SHOPPER_device;
        }

        public String getSHOPPER_MSDKIntegrationType() {
            return SHOPPER_MSDKIntegrationType;
        }

        public void setSHOPPER_MSDKIntegrationType(String SHOPPER_MSDKIntegrationType) {
            this.SHOPPER_MSDKIntegrationType = SHOPPER_MSDKIntegrationType;
        }
    }

    public static class Customer {
        @Expose
        @SerializedName("ipCountry")
        private String ipCountry;
        @Expose
        @SerializedName("ip")
        private String ip;
        @Expose
        @SerializedName("email")
        private String email;

        public String getIpCountry() {
            return ipCountry;
        }

        public void setIpCountry(String ipCountry) {
            this.ipCountry = ipCountry;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class Card {
        @Expose
        @SerializedName("expiryYear")
        private String expiryYear;
        @Expose
        @SerializedName("expiryMonth")
        private String expiryMonth;
        @Expose
        @SerializedName("holder")
        private String holder;
        @Expose
        @SerializedName("last4Digits")
        private String last4Digits;
        @Expose
        @SerializedName("bin")
        private String bin;

        public String getExpiryYear() {
            return expiryYear;
        }

        public void setExpiryYear(String expiryYear) {
            this.expiryYear = expiryYear;
        }

        public String getExpiryMonth() {
            return expiryMonth;
        }

        public void setExpiryMonth(String expiryMonth) {
            this.expiryMonth = expiryMonth;
        }

        public String getHolder() {
            return holder;
        }

        public void setHolder(String holder) {
            this.holder = holder;
        }

        public String getLast4Digits() {
            return last4Digits;
        }

        public void setLast4Digits(String last4Digits) {
            this.last4Digits = last4Digits;
        }

        public String getBin() {
            return bin;
        }

        public void setBin(String bin) {
            this.bin = bin;
        }
    }

    public static class ResultDetails {
        @Expose
        @SerializedName("AuthorizeId")
        private String AuthorizeId;
        @Expose
        @SerializedName("AvsResultCode")
        private String AvsResultCode;
        @Expose
        @SerializedName("endToEndId")
        private String endToEndId;
        @Expose
        @SerializedName("BatchNo")
        private String BatchNo;
        @Expose
        @SerializedName("VerStatus")
        private String VerStatus;
        @Expose
        @SerializedName("connectorId")
        private String connectorId;
        @Expose
        @SerializedName("ConnectorTxID1")
        private String ConnectorTxID1;
        @Expose
        @SerializedName("CscResultCode")
        private String CscResultCode;

        public String getAuthorizeId() {
            return AuthorizeId;
        }

        public void setAuthorizeId(String AuthorizeId) {
            this.AuthorizeId = AuthorizeId;
        }

        public String getAvsResultCode() {
            return AvsResultCode;
        }

        public void setAvsResultCode(String AvsResultCode) {
            this.AvsResultCode = AvsResultCode;
        }

        public String getEndToEndId() {
            return endToEndId;
        }

        public void setEndToEndId(String endToEndId) {
            this.endToEndId = endToEndId;
        }

        public String getBatchNo() {
            return BatchNo;
        }

        public void setBatchNo(String BatchNo) {
            this.BatchNo = BatchNo;
        }

        public String getVerStatus() {
            return VerStatus;
        }

        public void setVerStatus(String VerStatus) {
            this.VerStatus = VerStatus;
        }

        public String getConnectorId() {
            return connectorId;
        }

        public void setConnectorId(String connectorId) {
            this.connectorId = connectorId;
        }

        public String getConnectorTxID1() {
            return ConnectorTxID1;
        }

        public void setConnectorTxID1(String ConnectorTxID1) {
            this.ConnectorTxID1 = ConnectorTxID1;
        }

        public String getCscResultCode() {
            return CscResultCode;
        }

        public void setCscResultCode(String CscResultCode) {
            this.CscResultCode = CscResultCode;
        }
    }

    public static class Result {
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("code")
        private String code;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
