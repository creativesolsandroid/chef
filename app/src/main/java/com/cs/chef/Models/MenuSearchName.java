package com.cs.chef.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuSearchName implements Serializable {

    String catname_en, catname_ar, headername_en, headername_ar;
    ArrayList<MenuItemSearch> itemsArrayList;

    public ArrayList<MenuItemSearch> getItemsArrayList() {
        return itemsArrayList;
    }

    public void setItemsArrayList(ArrayList<MenuItemSearch> itemsArrayList) {
        this.itemsArrayList= itemsArrayList;
    }

    public String getCatname_en() {
        return catname_en;
    }

    public void setCatname_en(String catname_en) {
        this.catname_en = catname_en;
    }

    public String getCatname_ar() {
        return catname_ar;
    }

    public void setCatname_ar(String catname_ar) {
        this.catname_ar = catname_ar;
    }

    public String getHeadername_en() {
        return headername_en;
    }

    public void setHeadername_en(String headername_en) {
        this.headername_en = headername_en;
    }

    public String getHeadername_ar() {
        return headername_ar;
    }

    public void setHeadername_ar(String headername_ar) {
        this.headername_ar = headername_ar;
    }
}
