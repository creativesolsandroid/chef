package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavedCards {


    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("DeletedDate")
        private String DeletedDate;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("CardBrand")
        private String CardBrand;
        @Expose
        @SerializedName("ExpireYear")
        private String ExpireYear;
        @Expose
        @SerializedName("ExpireMonth")
        private String ExpireMonth;
        @Expose
        @SerializedName("CardHolder")
        private String CardHolder;
        @Expose
        @SerializedName("Last4Digits")
        private String Last4Digits;
        @Expose
        @SerializedName("Token")
        private String Token;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getDeletedDate() {
            return DeletedDate;
        }

        public void setDeletedDate(String DeletedDate) {
            this.DeletedDate = DeletedDate;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getCardBrand() {
            return CardBrand;
        }

        public void setCardBrand(String CardBrand) {
            this.CardBrand = CardBrand;
        }

        public String getExpireYear() {
            return ExpireYear;
        }

        public void setExpireYear(String ExpireYear) {
            this.ExpireYear = ExpireYear;
        }

        public String getExpireMonth() {
            return ExpireMonth;
        }

        public void setExpireMonth(String ExpireMonth) {
            this.ExpireMonth = ExpireMonth;
        }

        public String getCardHolder() {
            return CardHolder;
        }

        public void setCardHolder(String CardHolder) {
            this.CardHolder = CardHolder;
        }

        public String getLast4Digits() {
            return Last4Digits;
        }

        public void setLast4Digits(String Last4Digits) {
            this.Last4Digits = Last4Digits;
        }

        public String getToken() {
            return Token;
        }

        public void setToken(String Token) {
            this.Token = Token;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
