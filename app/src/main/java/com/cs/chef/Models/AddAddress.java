package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddAddress {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("IsActive")
        private String isactive;
        @Expose
        @SerializedName("Longitude")
        private Double longitude;
        @Expose
        @SerializedName("Latitude")
        private Double latitude;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("AddressType")
        private String addresstype;
        @Expose
        @SerializedName("LandMark")
        private String landmark;
        @Expose
        @SerializedName("HouseName")
        private String housename;
        @Expose
        @SerializedName("HouseNo")
        private String houseno;
        @Expose
        @SerializedName("UserId")
        private String userid;
        @Expose
        @SerializedName("AddressId")
        private String addressid;

        public String getIsactive() {
            return isactive;
        }

        public void setIsactive(String isactive) {
            this.isactive = isactive;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddresstype() {
            return addresstype;
        }

        public void setAddresstype(String addresstype) {
            this.addresstype = addresstype;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getHousename() {
            return housename;
        }

        public void setHousename(String housename) {
            this.housename = housename;
        }

        public String getHouseno() {
            return houseno;
        }

        public void setHouseno(String houseno) {
            this.houseno = houseno;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getAddressid() {
            return addressid;
        }

        public void setAddressid(String addressid) {
            this.addressid = addressid;
        }
    }
}
