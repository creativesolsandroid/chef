package com.cs.chef.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SubAdditionals implements Serializable {

    @SerializedName("Data")
    private Data Data;
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @SerializedName("PricesAndAdditionals")
        private ArrayList<PricesAndAdditionals> PricesAndAdditionals;

        public ArrayList<PricesAndAdditionals> getPricesAndAdditionals() {
            return PricesAndAdditionals;
        }

        public void setPricesAndAdditionals(ArrayList<PricesAndAdditionals> PricesAndAdditionals) {
            this.PricesAndAdditionals = PricesAndAdditionals;
        }
    }

    public static class PricesAndAdditionals implements Serializable {
        @SerializedName("ItemSizePrice")
        private ArrayList<ItemSizePrice> ItemSizePrice;
        @SerializedName("IsSoldOut")
        private boolean IsSoldOut;
        @SerializedName("IsDeliver")
        private boolean IsDeliver;
        @SerializedName("ItemName_Ar")
        private String ItemName_Ar;
        @SerializedName("ItemName_En")
        private String ItemName_En;
        @SerializedName("ItemId")
        private int ItemId;

        public ArrayList<ItemSizePrice> getItemSizePrice() {
            return ItemSizePrice;
        }

        public void setItemSizePrice(ArrayList<ItemSizePrice> ItemSizePrice) {
            this.ItemSizePrice = ItemSizePrice;
        }

        public boolean getIsSoldOut() {
            return IsSoldOut;
        }

        public void setIsSoldOut(boolean IsSoldOut) {
            this.IsSoldOut = IsSoldOut;
        }

        public boolean getIsDeliver() {
            return IsDeliver;
        }

        public void setIsDeliver(boolean IsDeliver) {
            this.IsDeliver = IsDeliver;
        }

        public String getItemName_Ar() {
            return ItemName_Ar;
        }

        public void setItemName_Ar(String ItemName_Ar) {
            this.ItemName_Ar = ItemName_Ar;
        }

        public String getItemName_En() {
            return ItemName_En;
        }

        public void setItemName_En(String ItemName_En) {
            this.ItemName_En = ItemName_En;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class ItemSizePrice implements Serializable {
        @SerializedName("addgrp")
        private ArrayList<Addgrp> addgrp;
        @SerializedName("Itemprice")
        private float Itemprice;
        @SerializedName("ItemSizeName_Ar")
        private String ItemSizeName_Ar;
        @SerializedName("ItemSizeName_En")
        private String ItemSizeName_En;
        @SerializedName("priceId")
        private int priceId;

        public ArrayList<Addgrp> getAddgrp() {
            return addgrp;
        }

        public void setAddgrp(ArrayList<Addgrp> addgrp) {
            this.addgrp = addgrp;
        }

        public float getItemprice() {
            return Itemprice;
        }

        public void setItemprice(float Itemprice) {
            this.Itemprice = Itemprice;
        }

        public String getItemSizeName_Ar() {
            return ItemSizeName_Ar;
        }

        public void setItemSizeName_Ar(String ItemSizeName_Ar) {
            this.ItemSizeName_Ar = ItemSizeName_Ar;
        }

        public String getItemSizeName_En() {
            return ItemSizeName_En;
        }

        public void setItemSizeName_En(String ItemSizeName_En) {
            this.ItemSizeName_En = ItemSizeName_En;
        }

        public int getPriceId() {
            return priceId;
        }

        public void setPriceId(int priceId) {
            this.priceId = priceId;
        }
    }

    public static class Addgrp implements Serializable {
        @SerializedName("additems")
        private ArrayList<Additems> additems;
        @SerializedName("MaxSelection")
        private int MaxSelection;
        @SerializedName("MinSelection")
        private int MinSelection;
        @SerializedName("AddGrpName_Ar")
        private String AddGrpName_Ar;
        @SerializedName("AddGrpName_En")
        private String AddGrpName_En;
        @SerializedName("AddGrpId")
        private int AddGrpId;
        @SerializedName("AddGId")
        private int AddGId;

        public ArrayList<Additems> getAdditems() {
            return additems;
        }

        public void setAdditems(ArrayList<Additems> additems) {
            this.additems = additems;
        }

        public int getMaxSelection() {
            return MaxSelection;
        }

        public void setMaxSelection(int MaxSelection) {
            this.MaxSelection = MaxSelection;
        }

        public int getMinSelection() {
            return MinSelection;
        }

        public void setMinSelection(int MinSelection) {
            this.MinSelection = MinSelection;
        }

        public String getAddGrpName_Ar() {
            return AddGrpName_Ar;
        }

        public void setAddGrpName_Ar(String AddGrpName_Ar) {
            this.AddGrpName_Ar = AddGrpName_Ar;
        }

        public String getAddGrpName_En() {
            return AddGrpName_En;
        }

        public void setAddGrpName_En(String AddGrpName_En) {
            this.AddGrpName_En = AddGrpName_En;
        }

        public int getAddGrpId() {
            return AddGrpId;
        }

        public void setAddGrpId(int AddGrpId) {
            this.AddGrpId = AddGrpId;
        }

        public int getAddGId() {
            return AddGId;
        }

        public void setAddGId(int AddGId) {
            this.AddGId = AddGId;
        }
    }

    public static class Additems implements Serializable {
        @SerializedName("addprice")
        private ArrayList<Addprice> addprice;
        @SerializedName("MaxSelection")
        private int MaxSelection;
        @SerializedName("MinSelection")
        private int MinSelection;
        @SerializedName("AddtionalName_Ar")
        private String AddtionalName_Ar;
        @SerializedName("AddtionalName_en")
        private String AddtionalName_en;
        @SerializedName("AdditionalId")
        private int AdditionalId;
        private int add_qty = 0;

        public ArrayList<Addprice> getAddprice() {
            return addprice;
        }

        public void setAddprice(ArrayList<Addprice> addprice) {
            this.addprice = addprice;
        }

        public int getMaxSelection() {
            return MaxSelection;
        }

        public void setMaxSelection(int MaxSelection) {
            this.MaxSelection = MaxSelection;
        }

        public int getMinSelection() {
            return MinSelection;
        }

        public void setMinSelection(int MinSelection) {
            this.MinSelection = MinSelection;
        }

        public String getAddtionalName_Ar() {
            return AddtionalName_Ar;
        }

        public void setAddtionalName_Ar(String AddtionalName_Ar) {
            this.AddtionalName_Ar = AddtionalName_Ar;
        }

        public String getAddtionalName_en() {
            return AddtionalName_en;
        }

        public void setAddtionalName_en(String AddtionalName_en) {
            this.AddtionalName_en = AddtionalName_en;
        }

        public int getAdditionalId() {
            return AdditionalId;
        }

        public void setAdditionalId(int AdditionalId) {
            this.AdditionalId = AdditionalId;
        }

        public int getAdd_qty() {
            return add_qty;
        }

        public void setAdd_qty(int add_qty) {
            this.add_qty = add_qty;
        }
    }

    public static class Addprice implements Serializable {
        @SerializedName("Addprice")
        private float Addprice;

        public float getAddprice() {
            return Addprice;
        }

        public void setAddprice(float Addprice) {
            this.Addprice = Addprice;
        }
    }
}
