package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.chef.Activities.AdditionalActivity;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.SubAdditionals;
import com.cs.chef.R;

import java.util.ArrayList;

import static com.cs.chef.Activities.AdditionalActivity.MainOrderPrice;
import static com.cs.chef.Activities.AdditionalActivity.OrderPrice;
import static com.cs.chef.Activities.AdditionalActivity.sub_add_items;

public class SubAdditionalAdapter1 extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<SubAdditionals.Additems> data = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;
    public static Double lat, longi;
    int Qty = 0, add_price;
    float discount_amount;


    int min = 1, max = 5;
    public static float suborderPrice = 0;
    public static float mainsuborderPrice = 0;
    ArrayList<Boolean> mCheckBoxStates = new ArrayList<>();
    boolean sub_item_chk = false;

    public SubAdditionalAdapter1(Context context, ArrayList<SubAdditionals.Additems> data) {
        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        CheckBox img;
        TextView maddition, mtotal_price, mdiscount_price, max_item_can_be_selected;
        TextView mins, plus, qty;
        LinearLayout mcount;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.sub_items_additionals, null);


            holder.img = convertView
                    .findViewById(R.id.img);
            holder.maddition = convertView
                    .findViewById(R.id.additional);
            holder.mtotal_price = convertView
                    .findViewById(R.id.total_price);
            holder.mdiscount_price = convertView
                    .findViewById(R.id.discount_price);
            holder.mins = convertView
                    .findViewById(R.id.mins);
            holder.plus = convertView
                    .findViewById(R.id.plus);
            holder.qty = convertView
                    .findViewById(R.id.qty);
            holder.mcount = convertView
                    .findViewById(R.id.count);
            holder.max_item_can_be_selected = convertView
                    .findViewById(R.id.max_items_can_be_selected);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        Log.i("TAG", "Qty: " + Qty);
        Log.i("TAG", "price: " + suborderPrice);
        Log.i("TAG", "mainprice: " + mainsuborderPrice);
        Log.i("TAG", "count: " + sub_add_items);

//        suborderPrice = OrderPrice;
//        mainsuborderPrice = MainOrderPrice;

        holder.maddition.setText("" + data.get(position).getAddtionalName_en());
        for (int i = 0; i < data.get(position).getAddprice().size(); i++) {
//            int dis_count = 30;
            if (MenuActivity.banner) {
                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mdiscount_price.setVisibility(View.GONE);
//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
//                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
////                    params.addRule(RelativeLayout.CENTER_IN_PARENT);
//                    holder.mtotal_price.setLayoutParams(params); //causes layout update

                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getAddprice().get(i).getAddprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                    discount_amount = data.get(position).getAddprice().get(i).getAddprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            } else {
                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mdiscount_price.setVisibility(View.GONE);
//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
//                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//
//                    holder.mtotal_price.setLayoutParams(params); //causes layout update
                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getAddprice().get(i).getAddprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                    discount_amount = data.get(position).getAddprice().get(i).getAddprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            }

        }


        Qty = 0;
        holder.img.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {


                    if (!AdditionalActivity.max_add_selection.contains(data.get(position).getAddtionalName_en())) {
                        AdditionalActivity.max_add_selection.add(data.get(position).getAddtionalName_en());
                    }

                    Log.i("TAG", "maxsiz: " + AdditionalActivity.max_add_selection);
                    Log.i("TAG", "maxsizename: " + data.get(position).getAddtionalName_en());
                    Log.i("TAG", "maxsize: " + AdditionalActivity.max_add_selection.size());
                    Log.i("TAG", "maxaddcount: " + AdditionalActivity.max_add_to_be_selected);

                    if (AdditionalActivity.max_add_selection.size() > AdditionalActivity.max_add_to_be_selected) {

                        isChecked = false;

                        Constants.showOneButtonAlertDialog("You are done with maximum " + AdditionalActivity.max_add_to_be_selected + " " + AdditionalActivity.sub_additionals_name.getText() + ".", context.getResources().getString(R.string.app_name), "Ok", (Activity) context);

                    } else {

                        sub_item_chk = true;
                        holder.plus.setClickable(true);
                        Log.i("TAG", "chk_status1: " + sub_item_chk);


                        if (OrderPrice == 0) {

                            holder.img.setButtonDrawable(R.drawable.additional_unselected);

                            Constants.showOneButtonAlertDialog("Please select atleast any size", context.getResources().getString(R.string.app_name), "Ok", (Activity) context);

                        } else {

                            suborderPrice = OrderPrice;
                            mainsuborderPrice = MainOrderPrice;

                            holder.img.setButtonDrawable(R.drawable.additional_selected);

                            if (data.get(position).getMinSelection() == 0 && data.get(position).getMaxSelection() == 0 || data.get(position).getMinSelection() == 1 && data.get(position).getMaxSelection() == 1) {
//                        if (min == 0 && max == 0 || min == 1 && max == 1) {
                                holder.mcount.setVisibility(View.GONE);
                                int localCount = AdditionalActivity.sub_add_items.get(position);
                                AdditionalActivity.sub_add_items.set(position, (localCount + 1));
                                Qty = 1;
                                data.get(position).setAdd_qty(Qty);
                                AddItems();


                            } else {

                                holder.mcount.setVisibility(View.VISIBLE);
                                int localCount = AdditionalActivity.sub_add_items.get(position);
                                AdditionalActivity.sub_add_items.set(position, (localCount + 1));
                                Qty = 1;
                                data.get(position).setAdd_qty(Qty);
                                holder.qty.setText("" + Qty);
                                AddItems();
                                holder.max_item_can_be_selected.setText("Maximum " + data.get(position).getMaxSelection());
                            }

                            Log.i("TAG", "onCheckedChanged: ");
                        }

                    }

                } else {

                    if (AdditionalActivity.max_add_selection.contains(data.get(position).getAddtionalName_en())) {
                        AdditionalActivity.max_add_selection.remove(data.get(position).getAddtionalName_en());
                    }

                    sub_item_chk = false;
                    holder.plus.setClickable(false);

                    holder.img.setButtonDrawable(R.drawable.additional_unselected);

                    holder.mcount.setVisibility(View.GONE);

                    AdditionalActivity.sub_add_items.set(position, 0);
                    Qty = 0;
                    data.get(position).setAdd_qty(Qty);

                    AddItems();
                    holder.qty.setText("" + Qty);

                }

            }
        });


            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Log.i("TAG", "onClick: " + sub_add_items);
                    int localCount = AdditionalActivity.sub_add_items.get(position);

                    if (localCount < data.get(position).getMaxSelection()) {

                        Log.i("TAG", "add: ");
                        AdditionalActivity.sub_add_items.set(position, (localCount + 1));

                        holder.qty.setText("" + (localCount + 1));
                        data.get(position).setAdd_qty((localCount + 1));
                        AddItems();
                        Log.i("TAG", "add: ");

                    }

                    if (localCount == data.get(position).getMaxSelection()) {

                        Constants.showOneButtonAlertDialog("You are done with maximum " + data.get(position).getMaxSelection() + " " + data.get(position).getAddtionalName_en() + ".", context.getResources().getString(R.string.app_name), "Ok", (Activity) context);

                    }
                    try {
//                        DecimalFormat decimalFormat = new DecimalFormat("0.00");

//                        suborderPrice = suborderPrice * AdditionalActivity.Qty;

                        AdditionalActivity.price.setText("" + Constants.decimalFormat.format(suborderPrice));
                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(suborderPrice));
                        Log.i("TAG", "onClick: " + Constants.decimalFormat.format(suborderPrice));

//                                    AdditionalActivity.qty.setText("" + Qty);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

//            holder.plus.setClickable(true);
//            holder.plus.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
////                    if (Qty == data.get(position).getMaxSelection()){
//                    if (Qty == max) {
//
//                        holder.plus.setClickable(false);
//
//                    } else {
//
//                        holder.plus.setClickable(true);
////                        Qty = data.get(position).getMinSelection();
//                        Qty = Qty + 1;
//
//                        if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountamt() == 0) {
//                            for (int i = 0; i < data.get(position).getAddprice().size(); i++) {
//                                add_price = add_price + data.get(position).getAddprice().get(i).getAddprice();
//                            }
//                        } else {
//                            add_price = add_price + discount_amount;
//                        }
//
//                        holder.qty.setText("" + Qty);
//
//                    }
//
//                }
//            });

            holder.mins.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Integer.parseInt(holder.qty.getText().toString()) > 1) {
                        int localCount1 = AdditionalActivity.sub_add_items.get(position);
                        AdditionalActivity.sub_add_items.set(position, (localCount1 - 1));

                        holder.qty.setText("" + (localCount1 - 1));
                        data.get(position).setAdd_qty((localCount1 - 1));
                        AddItems();

                        try {

//                            suborderPrice = suborderPrice * AdditionalActivity.Qty;
                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(suborderPrice));
                            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(suborderPrice));
//                                        AdditionalActivity.qty.setText("" + Qty);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        holder.img.setButtonDrawable(R.drawable.additional_unselected);
                        holder.mcount.setVisibility(View.GONE);
                        int localCount1 = AdditionalActivity.sub_add_items.get(position);
                        AdditionalActivity.sub_add_items.set(position, (localCount1 - 1));
                        data.get(position).setAdd_qty((localCount1 - 1));
                        AddItems();

                    }

                }
            });

//        holder.mins.setClickable(true);
//        holder.mins.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (Qty == 1) {
//
//                    holder.mins.setClickable(false);
//
//                } else {
//
//                    holder.mins.setClickable(true);
//                    Qty = Qty - 1;
//
//                    if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountamt() == 0) {
//                        for (int i = 0; i < data.get(position).getAddprice().size(); i++) {
//                            add_price = add_price - data.get(position).getAddprice().get(i).getAddprice();
//                        }
//                    } else {
//                        add_price = add_price - discount_amount;
//                    }
//
//                    holder.qty.setText("" + Qty);
//
//                }
//
//            }
//        });

            notifyDataSetChanged();

        return convertView;
    }

    private void AddItems() {

        for (int i=0; i<data.size(); i++){

         if (data.get(i).getAdd_qty()!=0){

             suborderPrice = suborderPrice + (data.get(i).getAddprice().get(0).getAddprice() * data.get(i).getAdd_qty());

         }

        }

    }

//    private void AddItems(int position, int quantity) {
//        int localSize = itemQty.size();
//        ArrayList<String> localArray = new ArrayList<>();
//        localArray.addAll(itemQty);
//
//        Log.i("TAG", "additem ");
//
//        for (int i = 0; i < localSize; i++) {
//            if (localArray.get(i).equals(data.get(position).getAddtionalName_en())) {
//                Qty = Qty - 1;
//
//                if (MenuActivity.banner) {
//                    if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
//                        for (int j = 0; j < data.get(position).getAddprice().size(); j++) {
//                            suborderPrice = suborderPrice - data.get(position).getAddprice().get(j).getAddprice();
//                            mainsuborderPrice = mainsuborderPrice - data.get(position).getAddprice().get(j).getAddprice();
//                            item_add_price = item_add_price - data.get(position).getAddprice().get(j).getAddprice();
//                            dis_item_add_price = dis_item_add_price - data.get(position).getAddprice().get(j).getAddprice();
//                        }
//                    } else {
//                        for (int j = 0; j < data.get(position).getAddprice().size(); j++) {
//                            float discount_value = data.get(position).getAddprice().get(j).getAddprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
//                            float discount_amount = data.get(position).getAddprice().get(j).getAddprice() - discount_value;
//                            suborderPrice = suborderPrice - discount_amount;
//                            mainsuborderPrice = mainsuborderPrice - data.get(position).getAddprice().get(j).getAddprice();
//                            item_add_price = item_add_price - data.get(position).getAddprice().get(j).getAddprice();
//                            dis_item_add_price = dis_item_add_price - discount_amount;
//                        }
//                    }
//                } else {
//                    if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
//                        for (int j = 0; j < data.get(position).getAddprice().size(); j++) {
//                            suborderPrice = suborderPrice - data.get(position).getAddprice().get(j).getAddprice();
//                            mainsuborderPrice = mainsuborderPrice - data.get(position).getAddprice().get(j).getAddprice();
//                            item_add_price = item_add_price - data.get(position).getAddprice().get(j).getAddprice();
//                            dis_item_add_price = dis_item_add_price - data.get(position).getAddprice().get(j).getAddprice();
//                        }
//                    } else {
//                        for (int j = 0; j < data.get(position).getAddprice().size(); j++) {
//                            float discount_value = data.get(position).getAddprice().get(j).getAddprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
//                            float discount_amount = data.get(position).getAddprice().get(j).getAddprice() - discount_value;
//                            suborderPrice = suborderPrice - discount_amount;
//                            mainsuborderPrice = mainsuborderPrice - data.get(position).getAddprice().get(j).getAddprice();
//                            item_add_price = item_add_price - data.get(position).getAddprice().get(j).getAddprice();
//                            dis_item_add_price = dis_item_add_price - discount_amount;
//                        }
//                    }
//                }
//
////                itemQty.set(position, String.valueOf(0));
//                itemQty.remove(data.get(position).getAddtionalName_en());
//                String pos = Integer.toString(position);
//                boxpos.remove(pos);
//            } else {
////            Do nothing
//            }
//        }
//
//
//        Log.i("TAG", "suborderPrice: " + Constants.decimalFormat.format(suborderPrice));
//        for (int j = 0; j < quantity; j++) {
//            Qty = Qty + 1;
//            if (MenuActivity.banner) {
//                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
//                    for (int k = 0; k < data.get(position).getAddprice().size(); k++) {
//                        suborderPrice = suborderPrice + data.get(position).getAddprice().get(k).getAddprice();
//                        mainsuborderPrice = mainsuborderPrice + data.get(position).getAddprice().get(k).getAddprice();
//                        item_add_price = item_add_price + data.get(position).getAddprice().get(k).getAddprice();
//                        dis_item_add_price = dis_item_add_price + data.get(position).getAddprice().get(k).getAddprice();
//                    }
//                } else {
//                    for (int k = 0; k < data.get(position).getAddprice().size(); k++) {
//                        float discount_value = data.get(position).getAddprice().get(k).getAddprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
//                        float discount_amount = data.get(position).getAddprice().get(k).getAddprice() - discount_value;
//                        suborderPrice = suborderPrice + discount_amount;
//                        mainsuborderPrice = mainsuborderPrice + data.get(position).getAddprice().get(k).getAddprice();
//                        item_add_price = item_add_price + data.get(position).getAddprice().get(k).getAddprice();
//                        dis_item_add_price = dis_item_add_price + discount_amount;
//                    }
//                }
//            } else {
//                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
//                    for (int k = 0; k < data.get(position).getAddprice().size(); k++) {
//                        suborderPrice = suborderPrice + data.get(position).getAddprice().get(k).getAddprice();
//                        mainsuborderPrice = mainsuborderPrice + data.get(position).getAddprice().get(k).getAddprice();
//                        item_add_price = item_add_price + data.get(position).getAddprice().get(k).getAddprice();
//                        dis_item_add_price = dis_item_add_price + data.get(position).getAddprice().get(k).getAddprice();
//                    }
//                } else {
//                    for (int k = 0; k < data.get(position).getAddprice().size(); k++) {
//                        float discount_value = data.get(position).getAddprice().get(k).getAddprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
//                        float discount_amount = data.get(position).getAddprice().get(k).getAddprice() - discount_value;
//                        suborderPrice = suborderPrice + discount_amount;
//                        mainsuborderPrice = mainsuborderPrice + data.get(position).getAddprice().get(k).getAddprice();
//                        item_add_price = item_add_price + data.get(position).getAddprice().get(k).getAddprice();
//                        dis_item_add_price = dis_item_add_price + discount_amount;
//                    }
//                }
//            }
//
//            Log.i("TAG", "suborderPrice1: " + suborderPrice);
////            itemQty.set(position, data.get(position).getAddtionalName_en());
//            itemQty.add(data.get(position).getAddtionalName_en());
//            String pos = Integer.toString(position);
//            if (boxpos != null && !boxpos.contains(pos)) {
//                boxpos.add(pos);
//                Log.i("TAG", "box pos " + boxpos);
//                Log.i("TAG", "subitemqty " + itemQty);
//            }
//
////            plus.setClickable(true);
////            int qty = Integer.parseInt(mitemQty.getText().toString());
////            if (qty == 9999) {
////                plus.setClickable(false);
////            }
//        }
//
//        OrderPrice = suborderPrice;
//        MainOrderPrice = mainsuborderPrice;
//
//        Log.i("TAG", "suborderPrice2: " + suborderPrice);
//
////        itemfinalQty.add(position, String.valueOf(Qty));
////        itemfinalQty.add(position, String.valueOf(suborderPrice));
//        itemfinalQty.add(String.valueOf(Qty));
//        itemfinalprice.add(String.valueOf(suborderPrice));
//
//        if (itemfinalQty.size() == 0) {
//
//            suborderPrice = 0;
//            mainsuborderPrice = 0;
//            Qty = 0;
//            try {
////                DecimalFormat decimalFormat = new DecimalFormat("0.00");
//
//                AdditionalActivity.price.setText("" + Constants.decimalFormat.format(suborderPrice));
//                AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(suborderPrice));
////                AdditionalActivity.qty.setText("" + Qty);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else {
//            suborderPrice = suborderPrice * AdditionalActivity.Qty;
//            mainsuborderPrice = mainsuborderPrice * AdditionalActivity.Qty;
//            try {
////                DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                Log.i("TAG", "AddItems: " + Qty);
//
//                AdditionalActivity.price.setText("" + Constants.decimalFormat.format(suborderPrice));
//                AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(suborderPrice));
////                AdditionalActivity.qty.setText("" + Qty);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
}
