package com.cs.chef.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chef.Constants;
import com.cs.chef.Models.TrackOrder;
import com.cs.chef.R;

import java.util.ArrayList;

public class TrackOrderItemsAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<TrackOrder.OrderItems> data = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;
    public static Double lat, longi;

    public TrackOrderItemsAdapter(Context context, ArrayList<TrackOrder.OrderItems> data, String language) {
        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView mitem_name, mitem_count, mitem_price;
        ImageView img;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.track_order_items, null);
            } else {
                convertView = inflater.inflate(R.layout.track_order_items_arabic, null);
            }

            holder.mitem_name = convertView
                    .findViewById(R.id.item_name);
//            holder.mitem_count = convertView
//                    .findViewById(R.id.item_count);
            holder.mitem_price = convertView
                    .findViewById(R.id.price);
            holder.img = convertView
                    .findViewById(R.id.img);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.mitem_name.setText("" + data.get(position).getItemName_En() + " X " + data.get(position).getQuantity());
        } else {
            holder.mitem_name.setText("" + data.get(position).getQuantity() + " X " + data.get(position).getItemName_Ar());
        }

//        holder.mitem_count.setText("X" + data.get(position).getQuantity());

        Glide.with(context).load(Constants.ITEM_IMAGE_URL + data.get(position).getItemImage()).into(holder.img);
        if (language.equalsIgnoreCase("En")) {
            holder.mitem_price.setText("SAR " + Constants.decimalFormat.format(data.get(position).getItemPrice()));
        } else {
            holder.mitem_price.setText("" + context.getResources().getString(R.string.price_format_ar) + " " + Constants.decimalFormat.format(data.get(position).getItemPrice()));
        }


        return convertView;
    }
}
