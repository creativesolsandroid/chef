package com.cs.chef.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Activities.AddAddressActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.AddAddress;
import com.cs.chef.Models.MyAddress;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.chef.Activities.MyAddressActivity.ADD_ADDRESS_INTENT;
import static com.cs.chef.Activities.MyAddressActivity.mobile;
import static com.cs.chef.Activities.OrderHistory.userid;
import static com.cs.chef.Constants.TAG;

public class MyAddressAdapter extends BaseAdapter {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<MyAddress.Data> addressArrayList = new ArrayList<>();
    private Activity activity;
    public LayoutInflater inflater;
    String language;
    String userId;
    SharedPreferences userPrefs;


    public MyAddressAdapter(Context context, ArrayList<MyAddress.Data> addressArrayList, Activity activity, String language){
        this.context = context;
        this.activity = activity;
        this.addressArrayList = addressArrayList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return addressArrayList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        TextView addressName, addressBody, phone_no;
//        Button editButton, deleteButton;
    }

    public View getView(final int position, View itemView, ViewGroup parent) {
        final ViewHolder holder;
        if (itemView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                itemView = inflater.inflate(R.layout.list_my_address, null);
            } else {
                itemView = inflater.inflate(R.layout.list_my_address_arabic, null);
            }

            holder.addressName = (TextView) itemView.findViewById(R.id.address_name);
            holder.addressBody = (TextView) itemView.findViewById(R.id.address_body);
//            holder.deleteButton = (Button) itemView.findViewById(R.id.address_delete_button);
//            holder.editButton = (Button) itemView.findViewById(R.id.address_edit_button);
            holder.phone_no = (TextView) itemView.findViewById(R.id.phone_no);

            holder.addressName.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.addressBody.setBackgroundColor(context.getResources().getColor(R.color.white));

            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        MyAddress.Data myAddress = addressArrayList.get(position);
        holder.addressName.setText(myAddress.getAddressname());
        String address = myAddress.getHouseno() + ", "+ myAddress.getLandmark() + ", " + myAddress.getAddress();
        holder.addressBody.setText(address);

//        holder.editButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectedPosition = position;
//                startAddressActivity();
//            }
//        });
//
//        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectedPosition = position;
//                showTwoButtonAlertDialog(context.getResources().getString(R.string.delete_alert),
//                        context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.yes),
//                        context.getResources().getString(R.string.no), activity);
//            }
//        });

        holder.phone_no.setText("+" + mobile);
        return itemView;
    }

    private class DeleteAddressApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower loaderDialog;
//        AlertDialog  = null;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareDeleteAddressJson();
            loaderDialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            loaderDialog.show();

//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = activity.getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = activity.getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<AddAddress> call = apiService.addAddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AddAddress>() {
                @Override
                public void onResponse(Call<AddAddress> call, Response<AddAddress> response) {
                    if(response.isSuccessful()){
                        AddAddress addressResponse = response.body();
                        try {
                            if(addressResponse.getStatus()){
                                addressArrayList.remove(selectedPosition);
                                notifyDataSetChanged();
                            }
                            else {
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = addressResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                            context.getResources().getString(R.string.ok), activity);
                                } else {
                                    String failureResponse = addressResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name_ar),
                                            context.getResources().getString(R.string.ok_ar), activity);

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddAddress> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(loaderDialog != null){
                loaderDialog.dismiss();
            }
        }
    }

    private String prepareDeleteAddressJson(){
        JSONObject addressObj = new JSONObject();

        try {
            addressObj.put("AddressId", addressArrayList.get(selectedPosition).getAddressid());
            addressObj.put("UserId", userId);
//            addressObj.put("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
//            addressObj.put("HouseName", addressArrayList.get(selectedPosition).getAddressname());
//            addressObj.put("LandMark", addressArrayList.get(selectedPosition).getLandmark());
//            addressObj.put("AddressType", "Home");
//            addressObj.put("Address", strAddress);
//            addressObj.put("Latitude", lat);
//            addressObj.put("Longitude", longi);
            addressObj.put("IsActive", false);

            Log.d(TAG, "prepareDeleteAddressJson: "+addressObj.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return addressObj.toString();
    }

    private void startAddressActivity(){
        Intent intent = new Intent(activity, AddAddressActivity.class);
        intent.putExtra("edit", true);
        intent.putExtra("AddressId",addressArrayList.get(selectedPosition).getAddressid());
        intent.putExtra("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
        intent.putExtra("HouseName", addressArrayList.get(selectedPosition).getAddressname());
        intent.putExtra("LandMark", addressArrayList.get(selectedPosition).getLandmark());
        intent.putExtra("AddressType", "Home");
        intent.putExtra("Address", addressArrayList.get(selectedPosition).getAddress());
        intent.putExtra("Latitude",addressArrayList.get(selectedPosition).getLatitude());
        intent.putExtra("Longitude",addressArrayList.get(selectedPosition).getLongitude());
        ((Activity) context).startActivityForResult(intent, ADD_ADDRESS_INTENT);
    }

    public void showTwoButtonAlertDialog(String descriptionStr, String titleStr, String positiveStr, String negativeStr, final Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        title.setText(titleStr);
        yes.setText(positiveStr);
        no.setText(negativeStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
                String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new DeleteAddressApi().execute();
                }
                else{
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
