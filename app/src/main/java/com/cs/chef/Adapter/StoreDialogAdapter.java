package com.cs.chef.Adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.Filter;
import com.cs.chef.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StoreDialogAdapter extends RecyclerView.Adapter<StoreDialogAdapter.MyViewHolder> {
    private Context context;
    private int selectedPosition = 0;
    private ArrayList<Filter> storeArrayList = new ArrayList<>();
    private Activity activity;
    int pos = 0;
    String langauge;

    public StoreDialogAdapter(Context context, ArrayList<Filter> storeArrayList, int pos, Activity activity, String langauge){
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.langauge = langauge;
        this.storeArrayList = storeArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (langauge.equalsIgnoreCase("En")) {
             itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dialog_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dialog_list_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        if (langauge.equalsIgnoreCase("En")) {
            holder.storename.setText(storeArrayList.get(pos).getBrands().get(position).getBranchName_En());
            holder.storetype.setText(storeArrayList.get(pos).getBrands().get(position).getStoreType());
        } else {
            holder.storename.setText(storeArrayList.get(pos).getBrands().get(position).getBranchName_Ar());
            holder.storetype.setText(storeArrayList.get(pos).getBrands().get(position).getStoreType_Ar());
        }
        holder.distance.setText(priceFormat.format(storeArrayList.get(pos).getBrands().get(position).getDistance())+" KM");
        holder.rating.setText(storeArrayList.get(pos).getBrands().get(position).getRating());

        if (storeArrayList.get(pos).getBrands().get(position).getStoreStatus().equalsIgnoreCase("close")){
            holder.storestatus.setVisibility(View.VISIBLE);

            String openDateStr = storeArrayList.get(position).getBrands().get(pos).getStarDateTime();
            openDateStr = openDateStr.replace("T", " ");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
//
//        String tt = array[1];
            try {
                Date time = sdf.parse(openDateStr);
                openDateStr = sdf1.format(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.storestatus.setText("Next open at "+openDateStr);
        }
        else{
            if (storeArrayList.get(pos).getBrands().get(position).getDiscountAmt()>0){
                holder.storestatus.setVisibility(View.VISIBLE);
                holder.storestatus.setText(""+storeArrayList.get(pos).getBrands().get(position).getDiscountAmt()+"%");
            }
            else {
                holder.storestatus.setVisibility(View.GONE);
            }
            Log.d("TAG",""+storeArrayList.get(pos).getBrands().get(position).getDiscountAmt());
        }
    }

    @Override
    public int getItemCount() {

        return storeArrayList.get(pos).getBrands().size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView storename, storetype, distance, rating,storestatus;
        public MyViewHolder(View itemView) {
            super(itemView);
            storename = (TextView) itemView.findViewById(R.id.storename);
            storetype = (TextView) itemView.findViewById(R.id.storetype);
            distance = (TextView) itemView.findViewById(R.id.distance);
            rating = (TextView) itemView.findViewById(R.id.rating);
            storestatus = (TextView)itemView.findViewById(R.id.storestatus);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (storeArrayList.get(pos).getBrands().get(getAdapterPosition()).getStoreStatus().equalsIgnoreCase("Close")){
                        if (langauge.equalsIgnoreCase("En")) {
                            String message = "Store is close";
                            Constants.showOneButtonAlertDialog(message, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.Done), (Activity) context);
                        } else {
                            String message = "مغلق";
                            Constants.showOneButtonAlertDialog(message, context.getResources().getString(R.string.app_name_ar),
                                    context.getResources().getString(R.string.Done_ar), (Activity) context);
                        }
                    }else {


                        Intent a = new Intent(context, MenuActivity.class);
                        a.putExtra("array", storeArrayList.get(pos).getBrands());
                        a.putExtra("pos", getAdapterPosition());
                        a.putExtra("banner", "non_banner");
                        context.startActivity(a);

                    }

                }
            });
        }
    }
}
