package com.cs.chef.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.chef.Models.CatNames;
import com.cs.chef.Models.MenuSearchName;
import com.cs.chef.R;

import java.util.ArrayList;
import java.util.List;

import static com.cs.chef.Activities.MenuActivity.selectedcat;

public class MenuHeaderAdapter1 extends RecyclerView.Adapter<MenuHeaderAdapter1.ViewHolder> {

    ArrayList<MenuSearchName> catNames = new ArrayList<>();

    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    //    MenuGridAdapter menuGridAdapter;
    boolean dropdown = false;
    List<TextView> cardViewList = new ArrayList<>();


    public MenuHeaderAdapter1(Context context, ArrayList<MenuSearchName> catNames, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.catNames = catNames;
        this.language = language;
//        this.hotelname_Ar = hotelname_Ar;
        this.parentActivity = parentActivity;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
        View view = null;
        if (language.equalsIgnoreCase("En")) {
            view = mInflater.inflate(R.layout.menu_header, parent, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            view = mInflater.inflate(R.layout.menu_header_arabic, parent, false);
        }
        return new ViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        if (language.equalsIgnoreCase("En")) {
            if (catNames.get(position).getCatname_en().equalsIgnoreCase("")) {
                holder.title.setVisibility(View.GONE);
            } else {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText(catNames.get(position).getCatname_en());
            }
            if (holder.title.getText().toString().equalsIgnoreCase("All")) {

                holder.title.setPadding(60, 10, 60, 10);

            } else {

                holder.title.setPadding(30, 10, 30, 10);

            }

        } else {
//            holder.layout.setRotationY(180);
            if (catNames.get(position).getCatname_ar().equalsIgnoreCase("")) {
                holder.title.setVisibility(View.GONE);
            } else {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText(catNames.get(position).getCatname_ar());
            }

            if (holder.title.getText().toString().equalsIgnoreCase("الكل")) {

                holder.title.setPadding(60, 10, 60, 10);

            } else {

                holder.title.setPadding(30, 10, 30, 10);

            }
        }

        cardViewList.add(holder.title);

        if (selectedcat.equalsIgnoreCase(catNames.get(position).getCatname_en())) {
            holder.title.setBackground(context.getResources().getDrawable(R.drawable.menu_header_selected));
            holder.title.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.title.setBackground(context.getResources().getDrawable(R.drawable.menu_header_unselected));
            holder.title.setTextColor(context.getResources().getColor(R.color.gray));
        }


//        if (language.equalsIgnoreCase("En")) {
        if (position != 0) {
            final ArrayList<String> cities = new ArrayList<>();
            for (int i = 0; i < catNames.get(position).getItemsArrayList().size(); i++) {
                cities.add(catNames.get(position).getItemsArrayList().get(i).getItem_name_en());
            }

//
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_spinner_citys, cities) {
//                    public View getView(int position, View convertView, ViewGroup parent) {
//                        View v = super.getView(position, convertView, parent);
//
//                        ((TextView) v).setTextSize(0);
//                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.white));
//
//                        return v;
//                    }
//
//                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                        View v = super.getDropDownView(position, convertView, parent);
//                        v.setBackgroundResource(R.color.white);
//                        ((TextView) v).setTextSize(15);
//                        ((TextView) v).setGravity(Gravity.LEFT);
//                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.black));
//
//                        return v;
//                    }
//                };
//
//                holder.spinner.setAdapter(adapter);
//                holder.spinner.setSelection(0, false);
        }

        holder.title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                    MenuActivity.filter = true;
//                    selectedcat = catNames.get(position).getCatname_en();
//                    MenuActivity.itemsArrayList.clear();
//                    if (selectedcat.equalsIgnoreCase("all")) {
//                        for (int i = 1; i < catNames.size(); i++) {
//                            for (int j = 0; j < catNames.get(i).getItemsArrayList().size(); j++) {
//                                Items itemname = new Items();
//                                itemname.setItemid(catNames.get(i).getItemsArrayList().get(j).getItemid());
//                                itemname.setItem_name_en(catNames.get(i).getItemsArrayList().get(j).getItem_name_en());
//                                itemname.setItem_name_ar(catNames.get(i).getItemsArrayList().get(j).getItem_name_ar());
//                                itemname.setItem_desc_en(catNames.get(i).getItemsArrayList().get(j).getItem_desc_en());
//                                itemname.setItem_desc_ar(catNames.get(i).getItemsArrayList().get(j).getItem_desc_ar());
//                                itemname.setItem_price(catNames.get(i).getItemsArrayList().get(j).getItem_price());
//                                itemname.setItem_image(catNames.get(i).getItemsArrayList().get(j).getItem_image());
//
//                                Log.i("TAG", "header_itemall: " + itemname.getItem_name_en());
//                                itemsArrayList.add(itemname);
//                            }
//                        }
//                    } else {
//                        for (int i = 1; i < catNames.size(); i++) {
//                            if (catNames.get(i).getCatname_en().equalsIgnoreCase(selectedcat)) {
//                                for (int j = 0; j < catNames.get(i).getItemsArrayList().size(); j++) {
//                                    Items itemname = new Items();
//                                    itemname.setItemid(catNames.get(i).getItemsArrayList().get(j).getItemid());
//                                    itemname.setItem_name_en(catNames.get(i).getItemsArrayList().get(j).getItem_name_en());
//                                    itemname.setItem_name_ar(catNames.get(i).getItemsArrayList().get(j).getItem_name_ar());
//                                    itemname.setItem_desc_en(catNames.get(i).getItemsArrayList().get(j).getItem_desc_en());
//                                    itemname.setItem_desc_ar(catNames.get(i).getItemsArrayList().get(j).getItem_desc_ar());
//                                    itemname.setItem_price(catNames.get(i).getItemsArrayList().get(j).getItem_price());
//                                    itemname.setItem_image(catNames.get(i).getItemsArrayList().get(j).getItem_image());
//                                    Log.i("TAG", "header_item: " + itemname.getItem_name_en());
//                                    itemsArrayList.add(itemname);
//                                }
//                                break;
//                            }
//                        }
//                    }

                Intent intent = new Intent("HeaderUpdate");
                // You can also include some extra data.
//                Bundle b = new Bundle();
                intent.putExtra("pos", position);
                intent.putExtra("cat_name", catNames.get(position).getCatname_en());
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//                    menuGridAdapter = new MenuGridAdapter(context, catNames, itemsArrayList);
//                    grid_view.setAdapter(menuGridAdapter);

                for (TextView cardView : cardViewList) {
                    cardView.setBackground(context.getResources().getDrawable(R.drawable.menu_header_unselected));
                    cardView.setTextColor(context.getResources().getColor(R.color.gray));
                }
                holder.title.setBackground(context.getResources().getDrawable(R.drawable.menu_header_selected));
                holder.title.setTextColor(context.getResources().getColor(R.color.white));
//                    holder.spinner.performClick();
//                        notifyDataSetChanged();
                return true;
            }
        });

    }


    @Override
    public int getItemCount() {
        return catNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout layout;
        TextView title;
//        Spinner spinner;

//        ImageView curstImage;

        public ViewHolder(View itemView) {
            super(itemView);
//            Log.i("TAG", "curst size4 " + orderList.size());

            title = itemView.findViewById(R.id.header_name);
//            itemImage = itemView.findViewById(R.id.image1);
//            viewpager = itemView.findViewById(R.id.pager);
//            layout = itemView.findViewById(R.id.layout);
//            spinner = itemView.findViewById(R.id.city_spinner);
//            curstImage = itemView.findViewById(R.id.curst_img_layout);

//            layout.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
//
//
//            int pos = getAdapterPosition();
//
//            for (int i = 0; i < galleryArrayList.size(); i++) {
//                if (galleryArrayList.get(i).getHotalnameEn().contains(hotelname_En.get(pos))) {
//
//                    if (!city_name_en.contains(galleryArrayList.get(i).getCityNameEn())) {
//                        city_name_en.add(galleryArrayList.get(i).getCityNameEn());
//                    }
//
//                }
//            }
//
//            Log.d("TAG", "adapterpos: " + pos);
//
//            if (hotelname_En.get(pos).contains(hotelname_En.get(pos))) {
//
//                spinner.setBackgroundResource(R.drawable.gallery_footer_selected_bg);
//
//            }
//
//
//        }
    }
}
