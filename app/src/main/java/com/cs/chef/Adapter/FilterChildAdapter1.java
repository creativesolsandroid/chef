package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.util.ArrayList;

import static com.cs.chef.Fragments.HomeScreenFragment.vendertypeid;
import static com.cs.chef.Fragments.HomeScreenFragment.vendertypeid;

public class FilterChildAdapter1 extends RecyclerView.Adapter<FilterChildAdapter1.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<StoresList.VendorTypes> data = new ArrayList<>();
    private Activity activity;
    boolean filter = true;
    String language;


    public FilterChildAdapter1(Context context, ArrayList<StoresList.VendorTypes> data, String language) {
        this.context = context;
        this.activity = activity;
        this.data = data;
        this.language = language;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.child_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.child_list, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("Ar")){
            holder.filter_child_name.setRotationY(180);
        }

        if (language.equalsIgnoreCase("En")) {
            holder.filter_child_name.setText("" + data.get(position).getName_En());
        } else {
            holder.filter_child_name.setText("" + data.get(position).getName_Ar());
        }

        if (vendertypeid == data.get(position).getId()) {

            holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
            holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.white));

        } else {

            holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.chlid_unselected));
            holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.blue));
        }

        holder.filter_child_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                vendertypeid = data.get(position).getId();
                if (vendertypeid == data.get(position).getId()) {

                    holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                    holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.white));
//                    for (int j = 0; j < vendertypeid.size(); j++) {
//                        if (vendertypeid.get(j) == data.get(position).getId()) {
//                            vendertypeid.remove(j);
//                            break;
//                        }
//                    }
                } else {

                    holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.chlid_unselected));
                    holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.blue));

                }

                notifyDataSetChanged();

//                Log.i("TAG", "onClick: " + vendertypeid.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView filter_child_name;
        RelativeLayout layout;
//        RecyclerView filter_child;

        public MyViewHolder(View itemView) {
            super(itemView);
            filter_child_name = (TextView) itemView.findViewById(R.id.filter_child_name);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);


        }
    }
}
