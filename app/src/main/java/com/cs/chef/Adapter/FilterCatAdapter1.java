package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.util.ArrayList;

//public class FilterCatAdapter extends BaseAdapter {

//    private Context context;
//    private int selectedPosition = 0;
//    private ArrayList<StoresList.FilterCategory> data = new ArrayList<>();
//    private Activity activity;
//    FilterChildAdapter mAdapter;
//    String language;
//    public LayoutInflater inflater;
//
//
//    public FilterCatAdapter(Context context, ArrayList<StoresList.FilterCategory> data, String language) {
//        this.context = context;
//        this.activity = activity;
//        this.data = data;
//        this.language = language;
//        this.inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    public int getCount() {
//        return data.size();
//    }
//
//    public Object getItem(int position) {
//        return null;
//    }
//
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    public static class ViewHolder {
//        TextView filter_group_name;
//        RecyclerView filter_child;
//
//    }
//
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        if (convertView == null) {
//            holder = new ViewHolder();
//            if (language.equalsIgnoreCase("En")) {
//                convertView = inflater.inflate(R.layout.group_list, null);
//            } else {
//                convertView = inflater.inflate(R.layout.group_list_arabic, null);
//            }
//
//            holder.filter_group_name = (TextView) convertView.findViewById(R.id.filter_group_name);
//            holder.filter_child = (RecyclerView) convertView.findViewById(R.id.filter_child);
//
//
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        if (language.equalsIgnoreCase("En")) {
//            holder.filter_group_name.setText("" + data.get(position).getFilterTypeName_En());
//        } else {
//            holder.filter_group_name.setText("" + data.get(position).getFilterTypeName_Ar());
//        }
//        holder.filter_child.setLayoutManager(new GridLayoutManager(context, 3));
//        mAdapter = new FilterChildAdapter(context, data.get(position).getFilters(), language);
////        menu_list.setLayoutManager(layoutManager);
//        holder.filter_child.setAdapter(mAdapter);
//
//        return convertView;
//    }
//
//}
public class FilterCatAdapter1 extends RecyclerView.Adapter<FilterCatAdapter1.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<StoresList.VendorTypes> data = new ArrayList<>();
    private Activity activity;
    FilterChildAdapter1 mAdapter;
    String language;


    public FilterCatAdapter1(Context context, ArrayList<StoresList.VendorTypes> data, String language){
        this.context = context;
        this.activity = activity;
        this.data = data;
        this.language = language;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_list_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.filter_group_name.setText("Vendor Types");
        } else {
            holder.filter_group_name.setText("نوع المتجر");
        }
        holder.filter_child.setLayoutManager(new GridLayoutManager(context, 3));
        mAdapter = new FilterChildAdapter1(context, data, language);
//        menu_list.setLayoutManager(layoutManager);
        holder.filter_child.setAdapter(mAdapter);


    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView filter_group_name;
        RecyclerView filter_child;

        public MyViewHolder(View itemView) {
            super(itemView);
            filter_group_name = (TextView) itemView.findViewById(R.id.filter_group_name);
            filter_child = (RecyclerView) itemView.findViewById(R.id.filter_child);

        }
    }
}
