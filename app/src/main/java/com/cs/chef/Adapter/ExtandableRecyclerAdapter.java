package com.cs.chef.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cs.chef.ChildsViewHolder;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.GroupsViewHolder;
import com.cs.chef.Models.SearchFilter;
import com.cs.chef.Models.SubitemsSearchFilter;
import com.cs.chef.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;

public class ExtandableRecyclerAdapter extends ExpandableRecyclerViewAdapter<GroupsViewHolder, ChildsViewHolder> {

    Context context;
    DataBaseHelper myDbHelper;

    public ExtandableRecyclerAdapter(ArrayList<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.context = context;
        myDbHelper = new DataBaseHelper(context);
    }

    @Override
    public GroupsViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_list, parent, false);
        return new GroupsViewHolder(view);

    }

    @Override
    public ChildsViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_list, parent, false);
        return new ChildsViewHolder(view, context);
    }

    @Override
    public void onBindChildViewHolder(ChildsViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final SubitemsSearchFilter child = ((SearchFilter) group).getItems().get(childIndex);
        holder.setSubitemname(child.getItemnameEn());


    }

    @Override
    public void onBindGroupViewHolder(GroupsViewHolder holder, int flatPosition, ExpandableGroup group) {

        holder.setGenreTitle(group);

    }
}
