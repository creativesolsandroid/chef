package com.cs.chef.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.chef.Activities.AdditionalActivity;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.MyAddress;
import com.cs.chef.Models.Order;
import com.cs.chef.R;

import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class ChangeAddressAdapter extends RecyclerView.Adapter<ChangeAddressAdapter.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<MyAddress.Data> addressArrayList = new ArrayList<>();
    private ArrayList<Order> orderList = new ArrayList<>();
    private Activity activity;
    String language;
    AlertDialog customDialog = null;
    DataBaseHelper myDbHelper;
    SharedPreferences LocationPrefs;
    SharedPreferences.Editor LocationPrefsEditor;

    public ChangeAddressAdapter(Context context, ArrayList<MyAddress.Data> addressArrayList, Activity activity, String language) {
        this.context = context;
        this.activity = activity;
        this.addressArrayList = addressArrayList;
        this.language = language;
        myDbHelper = new DataBaseHelper(context);
        orderList = myDbHelper.getOrderInfo();

        LocationPrefs = context.getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
        LocationPrefsEditor = LocationPrefs.edit();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_change_address, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_change_address_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        MyAddress.Data myAddress = addressArrayList.get(position);
        holder.addressName.setText(myAddress.getAddressname());
        final String address = myAddress.getHouseno() + ", " + myAddress.getLandmark() + ", " + myAddress.getAddress();
        holder.addressBody.setText(address);

        holder.addressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.addressboolean) {

                    Location me = new Location("");
                    Location dest = new Location("");

                    me.setLatitude(addressArrayList.get(position).getLatitude());
                    me.setLongitude(addressArrayList.get(position).getLongitude());

                    dest.setLatitude(Constants.store_lat);
                    dest.setLongitude(Constants.store_longi);

                    float dist = (me.distanceTo(dest)) / 1000;
                    Log.i("TAG", "onClick: " + dist);
                    Log.i("TAG", "storelat: " + Constants.store_lat);
                    Log.i("TAG", "storelong: " + Constants.store_longi);
                    Log.i("TAG", "user: " + me);
                    Double deliveryDistance = Double.valueOf(orderList.get(0).getDeliveryDistance());
                    Log.i("TAG", "deliverydistance: " + deliveryDistance);
                    if (dist <= deliveryDistance){
                        MyAddress.Data myAddress = addressArrayList.get(position);
                        String address = myAddress.getHouseno() + ", " + myAddress.getLandmark() + ", " + myAddress.getAddress();
                        Constants.address_id = Integer.parseInt(addressArrayList.get(position).getAddressid());
                        Constants.address = address;
                        Constants.addressboolean = true;
                        Constants.user_lat = addressArrayList.get(position).getLatitude();
                        Constants.user_longi = addressArrayList.get(position).getLongitude();
                        Constants.address_name = addressArrayList.get(position).getAddressname();

                        LocationPrefsEditor.putString("LocationName", addressArrayList.get(position).getAddressname());
                        LocationPrefsEditor.putString("Location_status", "locationON");
                        LocationPrefsEditor.putString("Latitute", String.valueOf(addressArrayList.get(position).getLatitude()));
                        LocationPrefsEditor.putString("Longitute", String.valueOf(addressArrayList.get(position).getLongitude()));
                        LocationPrefsEditor.commit();

                        Intent intent = new Intent();
                        activity.setResult(RESULT_OK, intent);
                        activity.finish();
                    } else {

                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog("The distance too far from restaurant please select near restaurant", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);
                        } else {
                            Constants.showOneButtonAlertDialog("المسافة بعيده جداً عن المطعم. الرجاء اختيار مطعم أقرب", context.getResources().getString(R.string.app_name_ar), context.getResources().getString(R.string.ok_ar), activity);
                        }

                    }


                } else if (Constants.homeboolean) {
                    MyAddress.Data myAddress = addressArrayList.get(position);
                    String address = myAddress.getHouseno() + ", " + myAddress.getLandmark() + ", " + myAddress.getAddress();


                    if (myDbHelper.getTotalOrderQty() != 0 && !Constants.address.equals(address) && !Constants.address.equals("")) {

                        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = li;
                        int layout;
                        if (language.equalsIgnoreCase("En")) {
                            layout = R.layout.alert_dialog;
                        } else {
                            layout = R.layout.alert_dialog_arabic;
                        }
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if (language.equalsIgnoreCase("En")) {
                            yes.setText("Yes");
                            no.setText("No");
                            desc.setText("You have items in your card with address \"" + Constants.address + "\" do you want to clear card and add new address");
                        } else {
                            yes.setText("نعم");
                            no.setText("لا");
                            desc.setText("You have items in your card with address " + Constants.address + "do you want to clear card and add new address"); // ask ravi for correct massage
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                myDbHelper.deleteOrderTable();
                                Intent intent = new Intent();

                                LocationPrefsEditor.putString("LocationName", addressArrayList.get(position).getAddressname());
                                LocationPrefsEditor.putString("Location_status", "locationON");
                                LocationPrefsEditor.putString("Latitute", String.valueOf(addressArrayList.get(position).getLatitude()));
                                LocationPrefsEditor.putString("Longitute", String.valueOf(addressArrayList.get(position).getLongitude()));
                                LocationPrefsEditor.commit();

                                intent.putExtra("lat", addressArrayList.get(position).getLatitude());
                                intent.putExtra("longi", addressArrayList.get(position).getLongitude());
                                intent.putExtra("loc", addressArrayList.get(position).getAddressname());
                                MyAddress.Data myAddress1 = addressArrayList.get(position);
                                String address1 = myAddress1.getHouseno() + ", " + myAddress1.getLandmark() + ", " + myAddress1.getAddress();
                                Constants.address_id = Integer.parseInt(addressArrayList.get(position).getAddressid());
                                Constants.address = address1;
                                Constants.user_lat = addressArrayList.get(position).getLatitude();
                                Constants.user_lat = addressArrayList.get(position).getLongitude();
                                Constants.address_name = addressArrayList.get(position).getAddressname();
                                Constants.addressboolean = true;
                                activity.setResult(RESULT_OK, intent);
                                activity.finish();
                                customDialog.dismiss();

                            }
                        });


                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth * 0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    } else {

                        Intent intent = new Intent();

                        LocationPrefsEditor.putString("LocationName", addressArrayList.get(position).getAddressname());
                        LocationPrefsEditor.putString("Location_status", "locationON");
                        LocationPrefsEditor.putString("Latitute", String.valueOf(addressArrayList.get(position).getLatitude()));
                        LocationPrefsEditor.putString("Longitute", String.valueOf(addressArrayList.get(position).getLongitude()));
                        LocationPrefsEditor.commit();

                        intent.putExtra("lat", addressArrayList.get(position).getLatitude());
                        intent.putExtra("longi", addressArrayList.get(position).getLongitude());
                        intent.putExtra("loc", addressArrayList.get(position).getAddressname());
                        MyAddress.Data myAddress1 = addressArrayList.get(position);
                        String address1 = myAddress1.getHouseno() + ", " + myAddress1.getLandmark() + ", " + myAddress1.getAddress();
                        Constants.address_id = Integer.parseInt(addressArrayList.get(position).getAddressid());
                        Constants.address = address1;
                        Constants.user_lat = addressArrayList.get(position).getLatitude();
                        Constants.user_lat = addressArrayList.get(position).getLongitude();
                        Constants.address_name = addressArrayList.get(position).getAddressname();
                        Constants.addressboolean = true;
                        activity.setResult(RESULT_OK, intent);
                        activity.finish();
                    }

                } else {

                    MyAddress.Data myAddress = addressArrayList.get(position);
                    String address = myAddress.getHouseno() + ", " + myAddress.getLandmark() + ", " + myAddress.getAddress();
                    Constants.address_id = Integer.parseInt(addressArrayList.get(position).getAddressid());
                    Constants.address = address;
                    Constants.user_lat = addressArrayList.get(position).getLatitude();
                    Constants.user_lat = addressArrayList.get(position).getLongitude();
                    Constants.address_name = addressArrayList.get(position).getAddressname();
                    Constants.addressboolean = true;
                    activity.finish();
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return addressArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView addressIcon;
        TextView addressName, addressBody;
        RelativeLayout addressLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            addressIcon = (ImageView) itemView.findViewById(R.id.address_icon);
            addressName = (TextView) itemView.findViewById(R.id.address_name);
            addressBody = (TextView) itemView.findViewById(R.id.address_body);
            addressLayout = (RelativeLayout) itemView.findViewById(R.id.address_layout);

            addressName.setBackgroundColor(context.getResources().getColor(R.color.white));
            addressBody.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }
}
