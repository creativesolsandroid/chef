package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by PULi on 19-2-2019.
 */
public class BannerslistAdapter extends RecyclerView.Adapter<BannerslistAdapter.MyViewHolder> {

    private Context context;
    //    private int selectedPosition = 0;
    ArrayList<StoresList.StoreDetails> storesArrayList;
    private Activity activity;
    public static final String TAG = "TAG";
    int pos = 0;
    String language;

    public BannerslistAdapter(Context context, ArrayList<StoresList.StoreDetails> storesArrayList, int pos, Activity activity, String language) {
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.storesArrayList = storesArrayList;
        this.language = language;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())

                    .inflate(R.layout.home_items, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())

                    .inflate(R.layout.home_items_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        StoresList.StoreDetails storeDetails = storesArrayList.get(position);
//        holder.store_type.setText(storeDetails.getBrands().get(0).getStoreType());
//        for (int i=0; i<storesArrayList.get(position).getBrands().size(); i++) {
        if (language.equalsIgnoreCase("En")) {
            holder.store_name.setText(storeDetails.getBranchName_En());
        } else {
            holder.store_name.setText(storeDetails.getBranchName_Ar());
        }
//        holder.store_delivery_time.setText(storeDetails.getBrands().get(i).getAvgpreparationtime() + " " + context.getResources().getString(R.string.home_minutes));
        holder.store_distance.setText(priceFormat.format(storeDetails.getDistance()) + " KM");
//        holder.store_delivery_time.setText(storeDetails.getBrands().get(i).getRating());
//        holder.disscount.setText(storeDetails.getBrands().get(i).getDiscountAmt() + "%");
//        holder.storeopen.setText(storeDetails.getBrands().get(i).getStarDateTime());

        String openDateStr = storesArrayList.get(position).getStarDateTime();
        openDateStr = openDateStr.replace("T", " ");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
//
//        String tt = array[1];
        try {
            Date time = sdf.parse(openDateStr);
            openDateStr = sdf1.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (storesArrayList.get(position).getStoreStatus().equals("Close")) {

//            holder.storeopen.setVisibility(View.VISIBLE);
//            holder.storeopen.setText("Next open at " + openDateStr);
//            holder.storeopen.setError("Store is close ");
        } else {
//            holder.storeopen.setVisibility(View.GONE);
        }

        if (storeDetails.getDiscountAmt() > 0) {
//            holder.disscount.setVisibility(View.VISIBLE);
        } else {
//            holder.disscount.setVisibility(View.GONE);
        }

//       if (storesArrayList.get(position).getBrands().get(i).getStardatetime()>)

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL + storeDetails.getStoreLogo_En())
                .into(holder.store_image);

//        Glide.with(context)
//                .load(Constants.IMAGE_URL + storeDetails.getBrands().get(i).getStoreLogo_En())
//                .into(holder.store_logo);

        if (language.equalsIgnoreCase("En")) {
            holder.short_desc.setText("" + storeDetails.getStoreType());
            holder.store_desc.setText("" + storeDetails.getBranchDescription_En());
            holder.can_serive.setText("can serve 1 to " + storeDetails.getServing() + " people");
            holder.rating.setText("" + storeDetails.getRating());
        } else {
            holder.short_desc.setText("" + storeDetails.getStoreType_Ar());
            holder.store_desc.setText("" + storeDetails.getBranchDescription_Ar());
            holder.can_serive.setText( " " + storeDetails.getServing() +"  أشخاص " );
            holder.rating.setText("" + storeDetails.getRating());
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "Arrylistsize" + storesArrayList.size());
        return storesArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_name, store_distance, short_desc, store_desc, can_serive, rating;
        ImageView store_image;
//        store_type
//                store_logo;
//        LinearLayout rating_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            store_name = (TextView) itemView.findViewById(R.id.store_name);
            short_desc = (TextView) itemView.findViewById(R.id.short_desc);
            store_desc = (TextView) itemView.findViewById(R.id.store_desc);
            store_distance = (TextView) itemView.findViewById(R.id.store_distance);
            can_serive = (TextView) itemView.findViewById(R.id.can_serive);
            store_image = (ImageView) itemView.findViewById(R.id.home_img);
            rating = (TextView) itemView.findViewById(R.id.rating);
//            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);
//            disscount = (TextView) itemView.findViewById(R.id.disscount);
//            storeopen = (TextView) itemView.findViewById(R.id.storeopen);
//            Changing backgroundcolor after shimmer effect
//            store_name.setBackgroundColor(Color.TRANSPARENT);
//            store_type.setBackgroundColor(Color.TRANSPARENT);
//            store_logo.setBackgroundColor(Color.TRANSPARENT);
//            store_delivery_time.setBackgroundColor(Color.TRANSPARENT);
//            store_name.setTypeface(Constants.getTypeFace(context));
//            store_type.setTypeface(Constants.getTypeFace(context));
//            store_delivery_time.setTypeface(Constants.getTypeFace(context));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent a = new Intent(context, MenuActivity.class);
                    a.putExtra("array", storesArrayList);
                    a.putExtra("pos", getAdapterPosition());
                    a.putExtra("banner", "banner");
                    context.startActivity(a);
                    Constants.store_lat = storesArrayList.get(getAdapterPosition()).getLatitude();
                    Constants.store_longi = storesArrayList.get(getAdapterPosition()).getLongitude();

                }
            });
        }
    }
}