package com.cs.chef;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

public class GroupsViewHolder extends GroupViewHolder {
    private TextView filter_group_name;



    public GroupsViewHolder(View itemView) {
        super(itemView);
         filter_group_name = (TextView) itemView
                .findViewById(R.id.filter_group_name);
    }

    public void setGenreTitle(ExpandableGroup genre) {

        filter_group_name.setText(genre.getTitle());

//        if (genre instanceof Genre) {
//            genreName.setText(genre.getTitle());
//            icon.setBackgroundResource(((Genre) genre).getIconResId());
//        }
//        if (genre instanceof MultiCheckGenre) {
//            genreName.setText(genre.getTitle());
//            icon.setBackgroundResource(((MultiCheckGenre) genre).getIconResId());
//        }
//        if (genre instanceof SingleCheckGenre) {
//            genreName.setText(genre.getTitle());
//            icon.setBackgroundResource(((SingleCheckGenre) genre).getIconResId());
//        }
    }


}

