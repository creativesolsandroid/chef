package com.cs.chef.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class MySeekBar extends android.support.v7.widget.AppCompatSeekBar {

    public MySeekBar(Context context) {
        super(context);
    }

    public MySeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MySeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);
        int thumb_x = (int) (((double) this.getProgress() / this.getMax()) * (double) this.getWidth() + 20);
        float middle = (float) (this.getHeight());
        int thumb_y = (int) (((double) this.getProgress() / this.getMax()) * (double) this.getHeight() + 20);

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(20);
        c.drawText("" + this.getProgress() + " KM", thumb_x, thumb_y, paint );
    }

}
