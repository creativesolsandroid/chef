package com.cs.chef.Activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.Fragments.HomeScreenFragment;
import com.cs.chef.R;
import com.cs.chef.Utils.GPSTracker;
import com.cs.chef.Utils.NetworkUtil;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.cs.chef.Constants.currentLatitude;
import static com.cs.chef.Constants.currentLongitude;

public class LocationActivity extends AppCompatActivity {

    String language;
    SharedPreferences LanguagePrefs;
    SharedPreferences LocationPrefs;
    SharedPreferences.Editor LocationPrefsEditor;
    String location_status;
    String locality_name;
    public static boolean isFromSettings = false;
    private boolean broadcastRegistered = false;

    TextView allow, manual;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1;

    GPSTracker gps;
    public static int REQUEST_CODE_AUTOCOMPLETE = 1;
    boolean locationUpdatedToActivity = false;

    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.location_layout);
        } else {
            setContentView(R.layout.location_layout_arabic);
        }

        LocationPrefs = getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
        LocationPrefsEditor = LocationPrefs.edit();
        location_status = LocationPrefs.getString("Location_status", null);

        allow = findViewById(R.id.allow);
        manual = findViewById(R.id.manual);

        allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        try {
                            gps = new GPSTracker(LocationActivity.this);
                            getGPSCoordinates();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    try {
                        gps = new GPSTracker(LocationActivity.this);
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAutocompleteActivity();
            }
        });
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(LocationActivity.this, perm));
    }

    public void getGPSCoordinates() throws IOException {

        if (gps != null) {
            if (gps.canGetLocation()) {
                currentLatitude = gps.getLatitude();
                currentLongitude = gps.getLongitude();
                location();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(LocationActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(LocationActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(LocationActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public void location() {
        if (currentLatitude != 0) {
            getLocality();
        }
        else {
            if(!broadcastRegistered) {
                Constants.showLoadingDialog(this);
                LocalBroadcastManager.getInstance(this).registerReceiver(
                        mMessageReceiver, new IntentFilter("GPSLocationUpdates1"));
                broadcastRegistered = true;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }

    public void getLocality() {
        Location mLocation = new Location("");
        mLocation.setLatitude(currentLatitude);
        mLocation.setLongitude(currentLongitude);

        Geocoder geocoder = null;
        try {
            geocoder = new Geocoder(LocationActivity.this, Locale.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Address found using the Geocoder.
        List<Address> addresses = null;
        String errorMessage = "";

        try {
            addresses = geocoder.getFromLocation(
                    mLocation.getLatitude(),
                    mLocation.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e("TAG", errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e("TAG", errorMessage + ". " +
                    "Latitude = " + mLocation.getLatitude() +
                    ", Longitude = " + mLocation.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e("TAG", errorMessage);
            }
        } else {
            Address address = addresses.get(0);

            if (address.getSubLocality() != null) {
                locality_name = address.getSubLocality();
            }
            else {
                locality_name = address.getLocality();
            }

            LocationPrefsEditor.putString("Location_status", "locationON");
            LocationPrefsEditor.putString("Latitute", String.valueOf(currentLatitude));
            LocationPrefsEditor.putString("Longitute", String.valueOf(currentLongitude));
            LocationPrefsEditor.putString("LocationName", locality_name);
            LocationPrefsEditor.commit();

            Constants.closeLoadingDialog();
            Intent i = new Intent(LocationActivity.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("class","splash");
            startActivity(i);
            finish();
        }
    }

    private void openAutocompleteActivity() {
        Log.d("TAG", "openAutocompleteActivity: ");
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            e.printStackTrace();
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(LocationActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_AUTOCOMPLETE && resultCode == RESULT_OK){
            Place place = PlaceAutocomplete.getPlace(LocationActivity.this, data);

            // TODO call location based filter
            LatLng latLong;
            latLong = place.getLatLng();
            Log.d("TAG", "locatinckeck "+latLong);
            currentLatitude = latLong.latitude;
            currentLongitude = latLong.longitude;

            getLocality();

//            LocationPrefsEditor.putString("Location_status", "locationON");
//            LocationPrefsEditor.putString("Latitute", String.valueOf(currentLatitude));
//            LocationPrefsEditor.putString("Longitute", String.valueOf(currentLongitude));
//            LocationPrefsEditor.putString("LocationName", locality_name);
//            LocationPrefsEditor.commit();
//
//            Intent i = new Intent(LocationActivity.this, MainActivity.class);
//            i.putExtra("class","splash");
//            startActivity(i);
//            Intent intent = new Intent(LocationActivity.this, MainActivity.class);
//            intent.putExtra("lat", latLong.latitude);
//            intent.putExtra("longi",  latLong.longitude);
//            intent.putExtra("loc", "");
//            setResult(RESULT_OK, i);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!locationUpdatedToActivity) {
                Bundle b = intent.getBundleExtra("Location");
                Location location = (Location) b.getParcelable("Location");
                if (location != null) {
                    if (location.getLatitude() != 0 && location.getLongitude() != 0) {
                        locationUpdatedToActivity = true;
                        // Get extra data included in the Intent

                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();
                        location();
                    }
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (isFromSettings && canAccessLocation()) {
            try {
                gps = new GPSTracker(LocationActivity.this);
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
