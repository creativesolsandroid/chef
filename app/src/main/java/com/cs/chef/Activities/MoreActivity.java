package com.cs.chef.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.chef.R;

public class MoreActivity extends AppCompatActivity {
    Context context;
    ImageView backbtn,twitter,privicy,trems, about_us;
    String language;
    SharedPreferences languagePrefs;
    TextView arabspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.moreactivity);
        } else {
            setContentView(R.layout.moreactivity_arabic);
        }
        context = this;

        backbtn=(ImageView)findViewById(R.id.back_btn);
//        twitter=(ImageView)findViewById(R.id.twitterimage);
        privicy=(ImageView)findViewById(R.id.more_privacypolicy);
        about_us = (ImageView) findViewById(R.id.more_about);
//        trems=(ImageView)findViewById(R.id.more_trems);
//        arabspace=(TextView)findViewById(R.id.arabspacelink);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(MoreActivity.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    fbIntent.putExtra("title", "About Us");
                } else {
                    fbIntent.putExtra("title", "نبذه عنا");
                }
                fbIntent.putExtra("url", "http://app.thechefapp.net/AboutUS");
                startActivity(fbIntent);
            }
        });
        privicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(MoreActivity.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    fbIntent.putExtra("title", "Privacy Policy");
                } else {
                    fbIntent.putExtra("title", "سياسة الخصوصية");
                }
                fbIntent.putExtra("url", "http://app.thechefapp.net/PrivacyPolicy");
                startActivity(fbIntent);
            }
        });
    }
}