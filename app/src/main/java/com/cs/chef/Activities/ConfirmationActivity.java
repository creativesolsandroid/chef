package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.InsertOrder;
import com.cs.chef.Models.MyCards;
import com.cs.chef.Models.Order;
import com.cs.chef.Models.SaveCardToDB;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.cs.chef.hyperpay.CheckoutIdRequestAsyncTask;
import com.cs.chef.hyperpay.CheckoutIdRequestListener;
import com.cs.chef.hyperpay.PaymentStatusRequestAsyncTask;
import com.cs.chef.hyperpay.PaymentStatusRequestListener;
import com.cs.chef.receiver.CheckoutBroadcastReceiver;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSkipCVVMode;
import com.oppwa.mobile.connect.checkout.meta.CheckoutStorePaymentDetailsMode;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.exception.PaymentException;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;
import com.oppwa.mobile.connect.service.ConnectService;
import com.oppwa.mobile.connect.service.IProviderBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import static com.cs.chef.Activities.CheckOutActivity.check_ordertype;
//import static com.cs.chef.Activities.CheckOutActivity.exp_date;
//import static com.cs.chef.Activities.CheckOutActivity.exp_time;

//import com.cs.chef.Fragments.AccountFragment;
//import com.cs.chef.Fragments.SearchFragment;

public class ConfirmationActivity extends AppCompatActivity implements
        CheckoutIdRequestListener, PaymentStatusRequestListener {

    TextView date, qty, price, name, mobileno, address, exp_date_time, cash, total_price, confirm, contact_info, order_type_date_time;
    public static TextView item_total, sub_total, mvat, servie_charges, mtotal_price, coupon_discount;
    ImageView card;
    boolean b_card = true, b_cash = false;
    public static float tax;
    AlertDialog customDialog;
    AlertDialog loadingDialog;
    SharedPreferences userPrefs;
    String userId, username, usermobileno, language;
    TextView service;

    SharedPreferences languagePrefs;

    float vat = (float) 5.0;

    public static int discount = 0, min_order_charges = 0;
    int maindiscount = 0;
    String offer_name, offer_desc;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    boolean offer;

    private static final int Date_id = 1;
    private static final int Time_id = 0;

    int PaymentType = 1;

    private static final int CONFIRM_ADDRESS_REQUEST = 1;

    RelativeLayout mcheck_out;
//    AlertDialog loaderDialog = null;

    Date cal_date1 = null, cal_time1 = null, cal_date3 = null;
    //    String ordertype, deliverytime;
    ImageView back_btn;
    TextView maddress_txt, mchange_address;

    SharedPreferences ConfirmPrefs;
    SharedPreferences.Editor ConfirmPrefsEditor;
    String moffername, mtotalprice, mservice, mservice_chargers, mcoupon_discount, mcheck_ordertype = "", mexpected_date, mexpected_time;


    //    Hyperpay
    private static final String STATE_RESOURCE_PATH = "STATE_RESOURCE_PATH";

    protected IProviderBinder providerBinder;
    protected String resourcePath;
    String merchantId = "";
    private boolean isPaymentStatusRequested = false;
    private MyCards myRegistrationCardDetails = new MyCards();

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ConfirmationActivity.this.onServiceConnected(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            providerBinder = null;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hyperpay
        if (savedInstanceState != null) {
            resourcePath = savedInstanceState.getString(STATE_RESOURCE_PATH);
            Log.d("TAG", "savedInstanceState: "+resourcePath);
        }

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        Log.i("TAG", "onlang: " + language);
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.confirmation);
        } else {
            setContentView(R.layout.confirmation_arabic);
        }

        mcheck_out = findViewById(R.id.check_out);

        mcheck_out.setVisibility(View.VISIBLE);


        myDbHelper = new DataBaseHelper(ConfirmationActivity.this);

        orderList = myDbHelper.getOrderInfo();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");
        username = userPrefs.getString("name", "-");
        usermobileno = userPrefs.getString("mobile", "-");

        ConfirmPrefs = getSharedPreferences("CONFIRM_PREFS", Context.MODE_PRIVATE);
        ConfirmPrefsEditor = ConfirmPrefs.edit();
        moffername = ConfirmPrefs.getString("Offername","");
        mtotalprice = ConfirmPrefs.getString("TotalPrice","");
        mservice = ConfirmPrefs.getString("Service","");
        mservice_chargers = ConfirmPrefs.getString("ServiceCharges","");
        mcoupon_discount = ConfirmPrefs.getString("CouponDiscount","");
        mcheck_ordertype = ConfirmPrefs.getString("CheckOrdertype","");
        mexpected_date = ConfirmPrefs.getString("ExpectedDate","");
        mexpected_time = ConfirmPrefs.getString("ExpectedTime","");

//        ordertype = getIntent().getStringExtra("ordertype");
//        deliverytime = getIntent().getStringExtra("deliverytime");


        date = findViewById(R.id.date);
        qty = findViewById(R.id.qty);
        price = findViewById(R.id.price);
        name = findViewById(R.id.name);
        mobileno = findViewById(R.id.mobile_no);
        address = findViewById(R.id.address);
        exp_date_time = findViewById(R.id.expected_date_time);
        cash = findViewById(R.id.cash);
        confirm = findViewById(R.id.confirm);
        total_price = findViewById(R.id.total_price);
        item_total = findViewById(R.id.item_total);
        sub_total = findViewById(R.id.sub_total);
        servie_charges = findViewById(R.id.service_charge);
        mvat = findViewById(R.id.vat);
        coupon_discount = findViewById(R.id.coupon_discount);
        mtotal_price = findViewById(R.id.total_price1);
        contact_info = findViewById(R.id.contact_info);
        order_type_date_time = findViewById(R.id.order_type_date_time);
        back_btn = findViewById(R.id.back_btn);
        service = findViewById(R.id.service);

        card = findViewById(R.id.card);

        maddress_txt = (TextView) findViewById(R.id.address_txt);
        mchange_address = (TextView) findViewById(R.id.change_address);

//        mitem_list = findViewById(R.id.item_list);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        try {
            String coupon_code = moffername;
            Log.i("TAG", "coupon code: " + coupon_code);
            if (language.equalsIgnoreCase("En")) {
                if (!coupon_code.equalsIgnoreCase("Apply Promo Code")) {

                    Log.i("TAG", "coupon code1: " + coupon_code);

                }
            } else {
                if (!coupon_code.equalsIgnoreCase("غير متوفر")) {

                    Log.i("TAG", "coupon code1: " + coupon_code);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (language.equalsIgnoreCase("En")) {
            if (myDbHelper.getTotalOrderQty() == 1) {
                qty.setText("" + myDbHelper.getTotalOrderQty() + " Item");
            } else {
                qty.setText("" + myDbHelper.getTotalOrderQty() + " Items");
            }

            price.setText("SAR " + mtotalprice);
        } else {
            if (myDbHelper.getTotalOrderQty() == 1) {
                qty.setText("" + myDbHelper.getTotalOrderQty() + " عنصر");
            } else {
                qty.setText("" + myDbHelper.getTotalOrderQty() + " عناصر");
            }

            price.setText("" + getResources().getString(R.string.price_format_ar) + " " + mtotalprice);
        }

        mchange_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.addressboolean = true;
                Constants.homeboolean = false;
                Intent a = new Intent(ConfirmationActivity.this, ChangeAddressActivity.class);
                startActivityForResult(a, CONFIRM_ADDRESS_REQUEST);

            }
        });

//        Log.i("TAG", "offer: " + OfferActivity.offer_selected);


//        if (OfferActivity.offer_selected) {
//
//            discount = getIntent().getIntExtra("discount", 0);
//            offer_name = getIntent().getStringExtra("offer_name");
//            offer_desc = getIntent().getStringExtra("offer_desc");
//
//        }

//        offer_layout.setVisibility(View.VISIBLE);

//        offer_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Log.i("TAG", "onClick: " + offer);
//
////                if (discount != 0 ) {
//                Intent a = new Intent(ConfirmationActivity.this, OfferActivity.class);
//                startActivity(a);
////                }
//
//            }
//        });

//        Log.i("TAG", "discount: " + discount);
//        if (discount != 0) {
//            Log.i("TAG", "discount1: " + discount);
//
//            moffer_name.setText("" + offer_name);
//            moffer_desc.setText("" + offer_desc);
//
//            cancel_offer.setText(">");
//
//        } else {
//
//            cancel_offer.setText("X");
//
//        }


//        for (int i = 0; i < orderList.size(); i++) {
//            item_total.setText(orderList.get(i).getTotalAmount());
//
//        }

        String orderstatus = null;
//        if (StoreMenuActivity.banner) {
//            brand_address.setText("" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getAddress());
//            Glide.with(ConfirmationActivity.this).load(Constants.IMAGE_URL + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getStoreLogo_En()).into(mbrand_logo);
//
//            brand_name.setText("" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBrandName_En());
//
//            map.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String geoUri = "http://maps.google.com/maps?q=loc:" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getLatitude() + "," + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getLongitude() + " (" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBranchName_En() + ")";
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                    startActivity(intent);
//                }
//            });
//
//
//            orderstatus = StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getOrderTypes();
//        } else {

        service.setText("" + mservice);

        for (int i = 0; i < orderList.size(); i++) {
            total_price.setText("" + mtotalprice);
            if (language.equalsIgnoreCase("En")) {
                if (mcheck_ordertype.equals("Dine-In") || mcheck_ordertype.equals("Pick Up")) {
                    maddress_txt.setText("PICKUP ADDRESS");
                    contact_info.setText("Resturant Info");
                    name.setText("" + orderList.get(i).getBranchName());
                    mobileno.setText("+" + orderList.get(i).getCategoryId());
                    address.setText("" + orderList.get(i).getAddress());
                    if (mcheck_ordertype.equals("Dine-In")) {
                        order_type_date_time.setText("DINE-IN DATE / TIME");
                    } else if (mcheck_ordertype.equals("Pick Up")) {
                        order_type_date_time.setText("PICKUP DATE / TIME");
                    } else if (mcheck_ordertype.equals("Delivery")) {
                        order_type_date_time.setText("DELIVERY DATE / TIME");
                    }
                    mchange_address.setVisibility(View.GONE);
                    confirm.setText("CONFIRM");
                } else {
                    maddress_txt.setText("DELIVERY ADDRESS");
                    contact_info.setText("Contact Info");
                    name.setText("" + username);
                    mobileno.setText("+" + usermobileno);
                    if (mcheck_ordertype.equals("Dine-In")) {
                        order_type_date_time.setText("DINE-IN DATE / TIME");
                    } else if (mcheck_ordertype.equals("Pick Up")) {
                        order_type_date_time.setText("PICKUP DATE / TIME");
                    } else if (mcheck_ordertype.equals("Delivery")) {
                        order_type_date_time.setText("DELIVERY DATE / TIME");
                    }
                    mchange_address.setVisibility(View.GONE);

                    Constants.store_lat = Double.valueOf(orderList.get(i).getLatitute());
                    Constants.store_longi = Double.valueOf(orderList.get(i).getLongitute());

                    if (Constants.address_id == 0) {

                        address.setText("" + Constants.address);
                        confirm.setText("SELECT ADDRESS");

                    } else {

                        mchange_address.setVisibility(View.VISIBLE);
                        address.setText("" + Constants.address);
                        confirm.setText("CONFIRM");

                    }

                }
            } else {
                if (mcheck_ordertype.equals("Dine-In") || mcheck_ordertype.equals("Pick Up")) {
                    contact_info.setText("معلومات المطعم");
                    maddress_txt.setText("عنوان الاستلام");
                    name.setText("" + orderList.get(i).getBranchName_Ar());
                    mobileno.setText("+" + orderList.get(i).getCategoryId());
                    address.setText("" + orderList.get(i).getAddress());
                    if (mcheck_ordertype.equals("Dine-In")) {
                        order_type_date_time.setText("DINE-IN DATE / TIME");
                    } else if (mcheck_ordertype.equals("Pick Up")) {
                        order_type_date_time.setText("تاريخ الاستلام / الوقت");
                    } else if (mcheck_ordertype.equals("Delivery")) {
                        order_type_date_time.setText("تاريخ التوصيل / الوقت");
                    }
                    mchange_address.setVisibility(View.GONE);

                    confirm.setText("تأكيد");
                } else {
                    contact_info.setText("معلومات الاتصال");
                    maddress_txt.setText("عنوان التوصيل");
                    name.setText("" + username);
                    mobileno.setText("+" + usermobileno);
                    if (mcheck_ordertype.equals("Dine-In")) {
                        order_type_date_time.setText("DINE-IN DATE / TIME");
                    } else if (mcheck_ordertype.equals("Pick Up")) {
                        order_type_date_time.setText("تاريخ الاستلام / الوقت");
                    } else if (mcheck_ordertype.equals("Delivery")) {
                        order_type_date_time.setText("تاريخ التوصيل / الوقت");
                    }
                    mchange_address.setVisibility(View.GONE);


                    Constants.store_lat = Double.valueOf(orderList.get(i).getLatitute());
                    Constants.store_longi = Double.valueOf(orderList.get(i).getLongitute());

                    if (Constants.address_id == 0) {

                        address.setText("" + Constants.address);
                        confirm.setText("اختيار عنوان");

                    } else {

                        mchange_address.setVisibility(View.VISIBLE);
                        address.setText("" + Constants.address);
                        confirm.setText("تأكيد");

                    }

                }
            }


            item_total.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
            servie_charges.setText("" + mservice_chargers);
            coupon_discount.setText("" + mcoupon_discount);
            mtotal_price.setText("" + mtotalprice);

            tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
            if (language.equalsIgnoreCase("En")) {
                mvat.setText("+ " + Constants.decimalFormat.format(tax));
            } else {
                mvat.setText("" + Constants.decimalFormat.format(tax) + " +");
            }
            float subtotal;
            subtotal = (myDbHelper.getTotalOrderPrice() + tax);
            sub_total.setText("" + Constants.decimalFormat.format(subtotal));

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy/hh:mm a", Locale.US);
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss", Locale.US);
            SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);
            SimpleDateFormat current_date = new SimpleDateFormat("MMMM dd, yyyy hh:mm a", Locale.US);


            String expected_date, expected_time;

            Date expect_date = null, expect_time = null;

            expected_date = mexpected_date;
            expected_time = mexpected_time;

            Log.i("TAG", "expectedtime1: " + expected_date);

            expected_date = expected_date + " " + expected_time;

            Log.i("TAG", "expectedtime2: " + expected_date);

            try {
                expect_date = df3.parse(expected_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expected_date = df1.format(expect_date);
            String currentdate;
            currentdate = current_date.format(expect_date);


            Log.i("TAG", "expectedtime3: " + expected_date);

            exp_date_time.setText("" + expected_date);
            date.setText("" + currentdate);


//            Glide.with(ConfirmationActivity.this).load(Constants.IMAGE_URL + orderList.get(i).getImage()).into(mbrand_logo);

//            brand_name.setText("" + orderList.get(i).getBrandName());
//
//            final int finalI = i;
//            map.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String geoUri = "http://maps.google.com/maps?q=loc:" + orderList.get(finalI).getLatitute() + "," + orderList.get(finalI).getLongitute() + " (" + orderList.get(finalI).getBranchName() + ")";
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                    startActivity(intent);
//                    Log.i("TAG", "onClick: " + orderList.get(finalI).getBranchName());
//                }
//            });
//
//            orderstatus = orderList.get(i).getOrderType();
//            maindiscount = Integer.parseInt(orderList.get(i).getDiscount());
//            min_order_charges = Integer.parseInt(orderList.get(i).getMin_amount()) - 1;


        }

        Log.i("TAG", "addressid: " + Constants.address_id);

        if (mcheck_ordertype.equals("Delivery")) {

            if (Constants.address_id != 0) {

                Location me = new Location("");
                Location dest = new Location("");

                me.setLatitude(Constants.user_lat);
                me.setLongitude(Constants.user_longi);

                dest.setLatitude(Constants.store_lat);
                dest.setLongitude(Constants.store_longi);

                float dist = (me.distanceTo(dest)) / 1000;
                if (dist <= Double.parseDouble(orderList.get(0).getDeliveryDistance())) {

                } else {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ConfirmationActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout;
                    if (language.equalsIgnoreCase("En")) {
                        layout = R.layout.alert_dialog;
                    } else {
                        layout = R.layout.alert_dialog_arabic;
                    }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if (language.equalsIgnoreCase("En")) {
                        yes.setText(getResources().getString(R.string.ok));
//                    no.setText("No");
                        desc.setText("The distance too far from restaurant please select near restaurant");
                    } else {
                        yes.setText(getResources().getString(R.string.ok_ar));
//                    no.setText("لا");
                        desc.setText("المسافة بعيده جداً عن المطعم. الرجاء اختيار مطعم أقرب");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Constants.addressboolean = true;
                            Constants.homeboolean = false;
                            Intent a = new Intent(ConfirmationActivity.this, ChangeAddressActivity.class);
                            startActivityForResult(a, CONFIRM_ADDRESS_REQUEST);
                            finish();
                            customDialog.dismiss();

                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                }
            }
        }

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (b_card) {

                    b_card = false;
                    Glide.with(ConfirmationActivity.this).load(R.drawable.card_checkout).into(card);
                    cash.setBackground(getResources().getDrawable(R.drawable.pay_order_type));
                    PaymentType = 2;

                } else {

                    b_card = true;
                    Glide.with(ConfirmationActivity.this).load(R.drawable.visa_master_card).into(card);
                    cash.setBackground(getResources().getDrawable(R.drawable.paymentbg));
                    PaymentType = 1;

                }


            }
        });

        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (b_cash) {

                    b_cash = false;
                    Glide.with(ConfirmationActivity.this).load(R.drawable.visa_master_card).into(card);
                    cash.setBackground(getResources().getDrawable(R.drawable.paymentbg));
                    PaymentType = 1;

                } else {

                    b_cash = true;
                    Glide.with(ConfirmationActivity.this).load(R.drawable.card_checkout).into(card);
                    cash.setBackground(getResources().getDrawable(R.drawable.pay_order_type));
                    PaymentType = 2;

                }


            }
        });
//        }


//        String[] orderstatus1 = orderstatus.split(",");
//        if (orderstatus1.length == 1) {
//            order_status_layout.setClickable(false);
//        } else if (orderstatus1.length == 2) {
//            order_status_layout.setClickable(true);
//            order_status_count = new String[]{orderstatus1[0], orderstatus1[1]};
//        } else if (orderstatus1.length == 3) {
//            order_status_layout.setClickable(true);
//            order_status_count = new String[]{orderstatus1[0], orderstatus1[1], orderstatus1[2]};
//        }

//        order_status_layout.setClickable(true);
//        order_status_count = new String[]{"Dine-In", "Pick Up"};
//
//        complete_bg.setVisibility(View.GONE);
//        morder_status_view.setVisibility(View.GONE);
//
//        order_status.setText("" + order_status_count[0]);
//
//        complete_bg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                complete_bg.setVisibility(View.GONE);
//
//            }
//        });
//
//        order_status_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                morder_status_view.setVisibility(View.VISIBLE);
//                complete_bg.setVisibility(View.VISIBLE);
//
//                LinearLayoutManager layoutmanager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
//                layoutmanager.setStackFromEnd(true);
//
//                morder_status_view.setLayoutManager(layoutmanager);
//
//                order_status_adapter = new OrderTypeListAdapter(ConfirmationActivity.this, order_status_count);
//                morder_status_view.setAdapter(order_status_adapter);
//
//            }
//        });


        String currentDate, currentTime;

        final Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("dd MM yyyy", Locale.US);
        SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat df5 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat current_date = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        currentDate = df3.format(c.getTime());
        currentTime = df5.format(c.getTime());

        Date time = null;

        try {
            time = df5.parse(currentTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(time);
        c.add(Calendar.MINUTE, 30);
        String pre_mins = df4.format(c.getTime());

        Date date1 = null;

        try {
            date1 = df3.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String currentdate;
        currentdate = current_date.format(date1);


//        exp_date.setText("" + currentDate);
//        exp_time.setText("" + pre_mins);

//        expected_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                final Calendar c = Calendar.getInstance();
//                mYear = c.get(Calendar.YEAR);
//                mMonth = c.get(Calendar.MONTH);
//                mDay = c.get(Calendar.DAY_OF_MONTH);
//
//
//                DatePickerDialog datePickerDialog = new DatePickerDialog(ConfirmationActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//
//                            @Override
//                            public void onDateSet(DatePicker view, int year,
//                                                  int monthOfYear, int dayOfMonth) {
//
//                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
//                                    isToday = true;
//                                } else {
//                                    isToday = false;
//                                }
//                                mYear = year;
//                                mDay = dayOfMonth;
//                                mMonth = monthOfYear;
//
//                                final int hour = c.get(Calendar.HOUR_OF_DAY);
//                                final int minute = c.get(Calendar.MINUTE);
//
//                                com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
//                                        ConfirmationActivity.this,
//                                        hour,
//                                        minute,
//                                        false
//                                );
//                                tpd.setThemeDark(true);
//                                tpd.vibrate(false);
//                                tpd.setAccentColor(Color.parseColor("#76C8FC"));
//                                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                                    @Override
//                                    public void onCancel(DialogInterface dialogInterface) {
//                                        Log.d("TimePicker", "Dialog was cancelled");
//                                    }
//                                });
//                                if (isToday) {
//                                    if (minute > 30) {
//                                        tpd.setMinTime(hour + 1, minute - 30, 00);
//                                    } else {
//                                        tpd.setMinTime(hour, (minute + 30), 00);
//                                        Log.i("TAG", "onDateSet: " + minute);
//                                        Log.i("TAG", "onDateSet1: " + (minute + 30));
//                                    }
//                                } else {
//                                    tpd.setMinTime(0, 0, 0);
//                                }
//                                tpd.show(getFragmentManager(), "Timepickerdialog");
//
//
//                            }
//                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//                long max = TimeUnit.DAYS.toMillis(1);
//                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + max);
//                datePickerDialog.setTitle("Select Date");
//                datePickerDialog.show();
//
////                showDialog(Time_id);
////                showDialog(Date_id);
//
//            }
//        });
//
//
//        item_total.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
//        if (maindiscount != 0) {
//            float maindiscount_value = 0;
//            for (int i = 0; i < orderList.size(); i++) {
//                maindiscount_value = myDbHelper.getMainTotalOrderPrice() * Float.parseFloat(orderList.get(i).getDiscount()) / 100;
//            }
////            offer_discount.setVisibility(View.VISIBLE);
////            moffer.setVisibility(View.VISIBLE);
//
////            ConfirmationActivity.offer_discount.setText("- " + Constants.decimalFormat.format(maindiscount_value));
//        } else {
//
//            offer_discount.setVisibility(View.GONE);
//            moffer.setVisibility(View.GONE);
//
//        }
//
//
//        tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
//        mvat.setText("+ " + Constants.decimalFormat.format(tax));


//        Log.i("TAG", "tax1: " + myDbHelper.getTotalOrderPrice() * Constants.vatper);
//
//        float discount_value = myDbHelper.getTotalOrderPrice() * ((float) discount / 100);
////            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;
//
//        Log.i("TAG", "discount: " + discount_value);
//
//        coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
//
//        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;
//        mtotal_price.setText("SAR " + Constants.decimalFormat.format(total_price));
//
//        final_qty.setText("" + myDbHelper.getTotalOrderQty() + " item");
//
//        final_price.setText("SAR " + Constants.decimalFormat.format(total_price));
//
//        proceed_to_pay.setBackgroundColor(Color.parseColor("#013950"));
//        proceed_to_pay.setClickable(true);
//        min_order_amount.setVisibility(View.GONE);
//        if (total_price < min_order_charges) {
//
//            proceed_to_pay.setClickable(false);
//            proceed_to_pay.setBackgroundColor(Color.parseColor("#778899"));
//            min_order_amount.setVisibility(View.VISIBLE);
//            min_order_amount.setText("Subtotal must be greater than SAR " + min_order_charges);
//        }


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (confirm.getText().toString().equals("SELECT ADDRESS") || confirm.getText().toString().equals("اختيار عنوان")) {
                    Constants.addressboolean = true;
                    Constants.homeboolean = false;
                    Intent a = new Intent(ConfirmationActivity.this, ChangeAddressActivity.class);
                    startActivityForResult(a, CONFIRM_ADDRESS_REQUEST);

                } else {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationActivity.this);

                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        if (PaymentType == 0) {

                            if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialog("Please select payment method", getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ConfirmationActivity.this);
                            } else {
                                Constants.showOneButtonAlertDialog("الرجاء اختيار طريقة الدفع", getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), ConfirmationActivity.this);
                            }

                        } else {
//                        userId = userPrefs.getString("userId","0");
//                        if (userId.equals("0")) {
//
////                            Intent a = new Intent(ConfirmationActivity.this, SignInActivity.class);
////                            startActivity(a);
//
//                        } else {

                            if (PaymentType == 1) {
                                new InsertOrderApi().execute();
                            } else if (PaymentType == 2) {
                                generateMerchantId();
                            }

                        }
//                    }

                    } else {

                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ConfirmationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ConfirmationActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }
        });

    }


    //    @Override
//    public void onTimeSet(RadialPickerLayout view, int selectedHour, int selectedMinute, int second) {
//
//        if (selectedHour == 0) {
////
//            selectedHour += 12;
//
//            format = "AM";
//        } else if (selectedHour == 12) {
//
//            format = "PM";
//
//        } else if (selectedHour > 12) {
//
//            selectedHour -= 12;
//
//            format = "PM";
//
//        } else {
//
//            format = "AM";
//        }
//        if (mMonth < 9) {
//            selectedDate = mDay + "-0" + (mMonth + 1) + "-" + mYear;
//        } else {
//            selectedDate = mDay + "-" + (mMonth + 1) + "-" + mYear;
//        }
//        selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
//        time1 = String.format("%02d:%02d:%02d", selectedHour, selectedMinute, second) + " " + format;
//
//        SimpleDateFormat df6 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
//        SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//
//        try {
//            cal_date1 = df6.parse(selectedDate);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        selectedDate = df3.format(cal_date1);
//
//        exp_time.setText(selectedTime);
//        exp_date.setText(selectedDate);
//
////        if(alertDialog1 != null){
////            alertDialog1.dismiss();
//////                        showDialog1(secs, false, "");
////        }
////
////        if(alertDialog != null){
////            alertDialog.dismiss();
//////                        showDialog1(secs, false, "");
////        }
//
//
//    }
//
////    public void showloaderAlertDialog(){
////        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ConfirmationActivity.this);
////        // ...Irrelevant code for customizing the buttons and title
////        LayoutInflater inflater = getLayoutInflater();
////        int layout = R.layout.loder_dialog;
////        View dialogView = inflater.inflate(layout, null);
////        dialogBuilder.setView(dialogView);
////        dialogBuilder.setCancelable(true);
////
////        loaderDialog = dialogBuilder.create();
////        loaderDialog.show();
////        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
////        Window window = loaderDialog.getWindow();
////        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////        lp.copyFrom(window.getAttributes());
////        //This makes the dialog take up the full width
////        Display display = getWindowManager().getDefaultDisplay();
////        Point size = new Point();
////        display.getSize(size);
////        int screenWidth = size.x;
////
////        double d = screenWidth*0.85;
////        lp.width = (int) d;
////        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
////        window.setAttributes(lp);
////    }
//
    private class InsertOrderApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(ConfirmationActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(ConfirmationActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertOrder> call = apiService.getInsertOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertOrder>() {
                @Override
                public void onResponse(Call<InsertOrder> call, Response<InsertOrder> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        InsertOrder orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();


                                myDbHelper.deleteOrderTable();
                                ConfirmPrefsEditor.clear();
                                ConfirmPrefsEditor.commit();
                                Intent a = new Intent(ConfirmationActivity.this, TrackOrderActivity.class);
                                a.putExtra("userid", orderItems.getData().getUserId());
                                a.putExtra("orderid", orderItems.getData().getOrderId());
                                a.putExtra("track_order", "check_out");
                                startActivity(a);


                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), ConfirmationActivity.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), ConfirmationActivity.this);
                                }
                            }
//                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ConfirmationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ConfirmationActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ConfirmationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ConfirmationActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<InsertOrder> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ConfirmationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ConfirmationActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ConfirmationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ConfirmationActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }


        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                JSONObject OrderDetails = new JSONObject();

                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String version = pInfo.versionName;
                JSONArray orderitem = new JSONArray();
                Log.i("TAG", "orderitem: " + orderitem.length());
                Log.i("TAG", "orderList: " + orderList.size());
//                for (int i = 0; i < orderitem.length(); i++) {
                for (int i = 0; i < orderList.size(); i++) {
                    JSONObject jo = new JSONObject();

                    jo.put("ItemId", orderList.get(i).getItemId());
                    jo.put("Comments", orderList.get(i).getComment());
                    jo.put("ItemPrice", orderList.get(i).getPrice());

                    JSONArray Additionalitem = new JSONArray();
                    if (orderList.get(i).getAdditionalsTypeId() != null) {
                        String ids = orderList.get(i).getAdditionalsTypeId();
                        String qty = orderList.get(i).getAdditionalitemQTY();
                        String price = orderList.get(i).getAdditionalsPrice();
                        List<String> idsList = Arrays.asList(ids.split(","));
                        List<String> qtyList = Arrays.asList(qty.split(","));
                        List<String> priceList = Arrays.asList(price.split(","));
                        for (int j = 0; j < idsList.size(); j++) {
                            if (!idsList.get(j).equals("")) {

                                JSONObject jo1 = new JSONObject();

                                jo1.put("AddItemId", idsList.get(j));
                                jo1.put("AddItemPrice", priceList.get(j));
                                jo1.put("Quantity", qtyList.get(j));

                                Additionalitem.put(jo1);
                            }
                        }
                    }


                    jo.put("Quantity", orderList.get(i).getQty());
                    jo.put("SizeId", orderList.get(i).getItemType());


                    jo.put("AdditionalItems", Additionalitem);
                    orderitem.put(jo);
                }
//                }

                int ordertype1 = 0;
                if (mcheck_ordertype.equals("Dine-In")) {
                    ordertype1 = 1;
                } else if (mcheck_ordertype.equals("Pick Up")) {
                    ordertype1 = 2;
                } else if (mcheck_ordertype.equals("Delivery")) {
                    ordertype1 = 3;
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss", Locale.US);
                SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);

                String expected_date, expected_time;

                Date expect_date = null, expect_time = null;

                expected_date = mexpected_date;
                expected_time = mexpected_time;

                Log.i("TAG", "expectedtime1: " + expected_date);

                expected_date = expected_date + " " + expected_time;

                Log.i("TAG", "expectedtime2: " + expected_date);

                try {
                    expect_date = df3.parse(expected_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                expected_date = dateFormat.format(expect_date);

                Log.i("TAG", "expectedtime3: " + expected_date);

                Log.i("TAG", "total_price_ar: " + Constants.convertToArabic(total_price.getText().toString()));

                JSONObject OrderDetails1 = new JSONObject();
                OrderDetails1.put("Version", "Android v" + version);
                OrderDetails1.put("VatPercentage", Constants.vatper);
                OrderDetails1.put("TotalPrice", Constants.convertToArabic(total_price.getText().toString()));
                OrderDetails1.put("ExpectedTime", expected_date);
                OrderDetails1.put("UserId", userId);
                OrderDetails1.put("AddressID", Constants.address_id);
                OrderDetails1.put("OrderType", ordertype1);
                if (language.equalsIgnoreCase("En")) {
                    OrderDetails1.put("DeliveryCharges", Constants.convertToArabic(mservice_chargers.replace("+ ", "")));
                } else {
                    OrderDetails1.put("DeliveryCharges", Constants.convertToArabic(mservice_chargers.replace(" +", "")));
                }
                OrderDetails1.put("CouponDiscount", "");

                String coupon_code = moffername;
                if (language.equalsIgnoreCase("En")){
                    if (!coupon_code.equals("Apply Promo Code")) {
                        OrderDetails1.put("CouponCode", moffername);
                    } else {
                        OrderDetails1.put("CouponCode", "");
                    }
                } else {
                    if (!coupon_code.equals("غير متوفر")) {
                        OrderDetails1.put("CouponCode", moffername);
                    } else {
                        OrderDetails1.put("CouponCode", "");
                    }
                }
                if (language.equalsIgnoreCase("En")) {
                    if (mcoupon_discount.equalsIgnoreCase("- 0.00")) {
                        OrderDetails1.put("CouponAmount", 0);
                    } else {
                        OrderDetails1.put("CouponAmount", Constants.convertToArabic(mcoupon_discount.replace("- ", "")));
                    }
                } else {
                    if (mcoupon_discount.equalsIgnoreCase("0.00 -")) {
                        OrderDetails1.put("CouponAmount", 0);
                    } else {
                        OrderDetails1.put("CouponAmount", Constants.convertToArabic(mcoupon_discount.replace(" -", "")));
                    }
                }

//                if (StoreMenuActivity.banner) {
//                    OrderDetails1.put("BrandId", StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBrandId());
//                    OrderDetails1.put("BranchId", StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBranchId());
//                } else {
                for (int i = 0; i < orderList.size(); i++) {
                    OrderDetails1.put("BrandId", orderList.get(i).getBrandId());
                    OrderDetails1.put("BranchId", orderList.get(i).getSubCatId());
                }
//                }
                OrderDetails1.put("SubTotal", Constants.convertToArabic(String.valueOf(myDbHelper.getTotalOrderPrice())));
                OrderDetails1.put("Devicetoken", SplashScreenActivity.regId);
                OrderDetails1.put("PaymentMode", PaymentType);
                OrderDetails1.put("VatCharges", Constants.convertToArabic(String.valueOf(tax)));
                OrderDetails1.put("VatPercentage", Constants.convertToArabic(String.valueOf(Constants.vatper)));

                OrderDetails.put("OrderItems", orderitem);
                OrderDetails.put("OrderDetails", OrderDetails1);
                parentObj.put("OrderDetails", OrderDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONFIRM_ADDRESS_REQUEST && resultCode == RESULT_OK) {

            if (language.equalsIgnoreCase("En")) {

                if (Constants.address_id == 0) {

                    address.setText("" + Constants.address);
                    confirm.setText("SELECT ADDRESS");

                } else {

                    mchange_address.setVisibility(View.VISIBLE);
                    address.setText("" + Constants.address);
                    confirm.setText("CONFIRM");

                }

            } else {
                if (Constants.address_id == 0) {

                    address.setText("" + Constants.address);
                    confirm.setText("اختيار عنوان");

                } else {

                    mchange_address.setVisibility(View.VISIBLE);
                    address.setText("" + Constants.address);
                    confirm.setText("تأكيد");

                }
            }

        }

        if (requestCode == CheckoutActivity.CHECKOUT_ACTIVITY) {
            switch (resultCode) {
                case CheckoutActivity.RESULT_OK:
                    /* Transaction completed. */
                    Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);

                    resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);

                    /* Check the transaction type. */
                    Log.d("TAG", "onActivityResult: " + transaction.getTransactionType());
                    if (transaction.getTransactionType() == TransactionType.SYNC) {
                        /* Check the status of synchronous transaction. */
                        requestPaymentStatus(resourcePath);
                    } else {
                        /* The on onNewIntent method may be called before onActivityResult
                           if activity was destroyed in the background, so check
                           if the intent already has the callback scheme */
                        if (hasCallbackScheme(getIntent())) {
                            requestPaymentStatus(resourcePath);
                            Log.d("TAG", "call back true");
                        } else {
                            Log.d("TAG", "call back false: ");
                            /* The on onNewIntent method wasn't called yet,
                               wait for the callback. */
//                            showProgressDialog(R.string.progress_message_please_wait);
                        }
                    }

                    break;
                case CheckoutActivity.RESULT_CANCELED:
                    hideProgressDialog();

                    break;
                case CheckoutActivity.RESULT_ERROR:
                    PaymentError error = data.getParcelableExtra(
                            CheckoutActivity.CHECKOUT_RESULT_ERROR);
                    Log.d("TAG", "onActivityResult: error " + error.getErrorInfo());
                    Log.d("TAG", "onActivityResult: error " + error.getErrorMessage());
                    Log.d("TAG", "onActivityResult: error " + error.getErrorCode());
                    showAlertDialog(R.string.error_message);
//                    Log.d("TAG", "onActivityResult: "+data.getData().toString());
            }
        }
    }

    protected boolean hasCallbackScheme(Intent intent) {
        String scheme = intent.getScheme();
        Log.d("TAG", "scheme: " + scheme);
        Log.d("TAG", "Constants.SHOPPER_RESULT_URL: " + Constants.SHOPPER_RESULT_URL);
        return Constants.SHOPPER_RESULT_URL.equals(scheme + "://result");
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent intent = new Intent(ConfirmationActivity.this, ConnectService.class);

        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG", "onStop called: ");
        unbindService(serviceConnection);
        stopService(new Intent(this, ConnectService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_RESOURCE_PATH, resourcePath);
    }

    protected void onServiceConnected(IBinder service) {
        providerBinder = (IProviderBinder) service;

        /* We have a connection to the service. */
        try {
            /* Initialize Payment Provider with test mode. */
            providerBinder.initializeProvider(Connect.ProviderMode.LIVE);
        } catch (PaymentException e) {
            showAlertDialog(R.string.error_message);
        }
    }

    protected void requestCheckoutId() {
        showProgressDialog(R.string.progress_message_checkout_id);
        String total_amt = total_price.getText().toString();

        new CheckoutIdRequestAsyncTask(this)
                .execute(Constants.priceFormat.format(Float.parseFloat(total_amt)), Constants.SHOPPER_RESULT_URL, "false",
                        merchantId, userPrefs.getString("email", ""), userId);
    }

    public void generateMerchantId() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("ddMMyyyy", Locale.US);
        SimpleDateFormat timeFormater = new SimpleDateFormat("HHmm", Locale.US);

        Calendar calendar = Calendar.getInstance();
        String timeStr = timeFormater.format(calendar.getTimeInMillis());
        String dateStr = dateFormater.format(calendar.getTimeInMillis());

        merchantId = userId + dateStr + timeStr;

        try {
            requestCheckoutId();
        } catch (Exception e) {
            Log.e("connect", "error creating the payment page", e);
        }
    }

    @Override
    public void onCheckoutIdReceived(String checkoutId) {
        hideProgressDialog();
        Log.d("TAG", "onCheckoutIdReceived: " + checkoutId);

        if (checkoutId == null) {
            showAlertDialog(R.string.error_message);
        }

        if (checkoutId != null) {
            isPaymentStatusRequested = false;
            openCheckoutUI(checkoutId);
        }
    }

    private void openCheckoutUI(String checkoutId) {
        CheckoutSettings checkoutSettings = createCheckoutSettings(checkoutId);
//        checkoutSettings.setShopperResultUrl("companyname://result");

        /* Set up the Intent and start the checkout activity. */
//        Intent intent = new Intent(getContext(), CheckoutActivity.class);
//        intent.putExtra(CheckoutActivity.CHECKOUT_SETTINGS, checkoutSettings);

//        getActivity().startActivityForResult(intent, CheckoutActivity.CHECKOUT_ACTIVITY);

        ComponentName componentName = new ComponentName(
                getPackageName(), CheckoutBroadcastReceiver.class.getName());

        /* Set up the Intent and start the checkout activity. */
        Intent intent = checkoutSettings.createCheckoutActivityIntent(this, componentName);

        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
    }

    @Override
    public void onErrorOccurred() {
        hideProgressDialog();
        showAlertDialog(R.string.error_message);
    }

    @Override
    public void onPaymentStatusReceived(MyCards paymentStatus) {
        hideProgressDialog();
        myRegistrationCardDetails = paymentStatus;
        if ("OK".equals(paymentStatus.getData().get(0).getCustomPayment())) {
            Log.d("TAG", "onPaymentStatusReceived: " + paymentStatus.getData().get(0).getRegistrationId());
//            if (!paymentStatus.getData().get(0).getRegistrationId().equals("")) {
//                new saveCardToDb().execute();
//            } else {
                new InsertOrderApi().execute();
//            }
            return;
        } else {
            showAlertDialog(R.string.message_unsuccessful_payment);
        }
    }

    protected void requestPaymentStatus(String resourcePath) {
        showProgressDialog(R.string.progress_message_payment_status);
        Log.d("TAG", "requestPaymentStatus: " + resourcePath);
        if (!isPaymentStatusRequested) {
            new PaymentStatusRequestAsyncTask(this).execute(resourcePath, userId);
            isPaymentStatusRequested = true;
        }
    }

    /**
     * Creates the new instance of {@link CheckoutSettings}
     * to instantiate the {@link CheckoutActivity}.
     *
     * @param checkoutId the received checkout id
     * @return the new instance of {@link CheckoutSettings}
     */
    protected CheckoutSettings createCheckoutSettings(String checkoutId) {
        return new CheckoutSettings(checkoutId, Constants.Config.PAYMENT_BRANDS,
                Connect.ProviderMode.LIVE)
                .setSkipCVVMode(CheckoutSkipCVVMode.FOR_STORED_CARDS)
                .setWebViewEnabledFor3DSecure(true)
                .setWindowSecurityEnabled(false)
                .setStorePaymentDetailsMode(CheckoutStorePaymentDetailsMode.PROMPT)
                .setShopperResultUrl(Constants.SHOPPER_RESULT_URL);

//        return new CheckoutSettings(checkoutId, Constants.Config.PAYMENT_BRANDS)
//                .setStorePaymentDetailsMode(CheckoutStorePaymentDetailsMode.PROMPT)
//                .setWindowSecurityEnabled(true)
//                .setWebViewEnabledFor3DSecure(true)
//                .setSkipCVVMode(CheckoutSkipCVVMode.FOR_STORED_CARDS)
//                .setShopperResultUrl(Constants.SHOPPER_RESULT_URL);
    }

    protected void showProgressDialog(int messageId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ConfirmationActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.progress_bar_alert;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView loadingText = (TextView) dialogView.findViewById(R.id.loading_text);
        loadingText.setText(getString(messageId));

        loadingDialog = dialogBuilder.create();
        loadingDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loadingDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void hideProgressDialog() {
        if (loadingDialog == null) {
            return;
        }

        loadingDialog.dismiss();
    }

    protected void showAlertDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, null)
                .setCancelable(false)
                .show();
    }

    protected void showAlertDialog(int messageId) {
        showAlertDialog(getString(messageId));
    }

    @Override
    protected void onNewIntent(Intent intent) {
//        Log.d("TAG", "onNewIntent: ");
        super.onNewIntent(intent);
        setIntent(intent);

        Log.d("TAG", "onNewIntent: "+intent.getScheme());
        /* Check if the intent contains the callback scheme. */
        if (resourcePath != null && hasCallbackScheme(intent)) {
            requestPaymentStatus(resourcePath);
        }
    }

    private class saveCardToDb extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSaveCardJSON();
            Constants.showLoadingDialog(ConfirmationActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SaveCardToDB> call = apiService.insertCreditCard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SaveCardToDB>() {
                @Override
                public void onResponse(Call<SaveCardToDB> call, Response<SaveCardToDB> response) {
                    Constants.closeLoadingDialog();
                    new InsertOrderApi().execute();
                }

                @Override
                public void onFailure(Call<SaveCardToDB> call, Throwable t) {
                    Constants.closeLoadingDialog();
                    new InsertOrderApi().execute();
                }
            });
            return null;
        }

        private String prepareSaveCardJSON() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("userId", userId);
                parentObj.put("token", myRegistrationCardDetails.getData().get(0).getRegistrationId());
                parentObj.put("last4Digits", myRegistrationCardDetails.getData().get(0).getCard().getLast4Digits());
                parentObj.put("cardHolder", myRegistrationCardDetails.getData().get(0).getCard().getHolder());
                parentObj.put("expireMonth", myRegistrationCardDetails.getData().get(0).getCard().getExpiryMonth());
                parentObj.put("expireYear", myRegistrationCardDetails.getData().get(0).getCard().getExpiryYear());
                parentObj.put("cardBrand", myRegistrationCardDetails.getData().get(0).getPaymentBrand());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }
}
