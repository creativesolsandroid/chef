package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.cs.chef.Adapter.OffersAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.Promotions;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.cs.chef.Models.Promotions;

public class OfferActivity extends AppCompatActivity {

    ListView offers_list;
    OffersAdapter mAdapter;
    public static boolean offer_selected;
    ArrayList<Promotions.Data> data = new ArrayList<Promotions.Data>();
    ArrayList<Promotions.PromotionDays> promodata = new ArrayList<Promotions.PromotionDays>();
    int branchid = 0;
    int brandid = 0;
    ImageView back_btn;

    String language;
    SharedPreferences languagePrefs;
    String userId;
    SharedPreferences userPrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.offers);
        } else {
            setContentView(R.layout.offers_arabic);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");
        branchid = Constants.branch_id;
        brandid = Constants.brand_id;

        offers_list = findViewById(R.id.offers_list);
        back_btn = findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        String networkStatus1 = NetworkUtil.getConnectivityStatusString(OfferActivity.this);
        if (!networkStatus1.equalsIgnoreCase("Not connected to Internet")) {
            new OfferApi().execute();
        } else {
            Toast.makeText(OfferActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    private class OfferApi extends AsyncTask<String, String, String> {

        AlertDialog dialog = null;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (userId.equalsIgnoreCase("0")){
                inputStr = prepareAccountloginJson();
            } else if (!userId.equalsIgnoreCase("0") && branchid == 0) {
                inputStr = prepareAccountJson();
            } else {
                inputStr = prepareVerifyMobileJson();
            }

//            dialog = new ACProgressFlower.Builder(OfferActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();

            Constants.showLoadingDialog(OfferActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OfferActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Promotions> call = apiService.getPromotions(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Promotions>() {
                @Override
                public void onResponse(Call<Promotions> call, Response<Promotions> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Promotions storeMenuItems = response.body();
                        data = storeMenuItems.getData();
                        try {
                            if (storeMenuItems.getStatus()) {
                                String message = storeMenuItems.getMessage();


                                mAdapter = new OffersAdapter(OfferActivity.this, data, language, OfferActivity.this);
                                offers_list.setAdapter(mAdapter);


                            } else {
                                //                          status false case
                                String failureResponse = storeMenuItems.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), OfferActivity.this);
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(OfferActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(OfferActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Promotions> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(OfferActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OfferActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                   Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareAccountloginJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("BranchId", 0);
                parentObj.put("BrandId", 0);
                parentObj.put("DeviceToken", 1);
                parentObj.put("UserId", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

        private String prepareAccountJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("BranchId", 0);
                parentObj.put("BrandId", 0);
                parentObj.put("DeviceToken", SplashScreenActivity.regId);
                parentObj.put("UserId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("BranchId", branchid);
                parentObj.put("BrandId", brandid);
                parentObj.put("DeviceToken", SplashScreenActivity.regId);
                parentObj.put("UserId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

}
