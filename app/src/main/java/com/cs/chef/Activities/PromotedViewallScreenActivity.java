package com.cs.chef.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.chef.Adapter.BannerslistAdapter;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.util.ArrayList;

//import com.cs.chef.Adapter.BannerslistAdapter;
//import com.cs.chef.Adapter.PViewallAdapter;

public class PromotedViewallScreenActivity extends AppCompatActivity {
    TextView header;
    ImageView backbtn;
    RecyclerView listview;
    public static final String TAG = "TAG";
    private ArrayList<StoresList.StoresDetails> storeItemsArrayList = new ArrayList<>();
//    PViewallAdapter mViewalldAdapter;
    BannerslistAdapter mBannerlistAdapter;
    ArrayList<StoresList.StoreDetails> storeDetails = new ArrayList<>();

    String language;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.viewallactiviry);
        } else {
            setContentView(R.layout.viewallactivity_arabic);
        }


        header =(TextView)findViewById(R.id.headertext);
        backbtn =(ImageView)findViewById(R.id.back_btn);
        listview =(RecyclerView)findViewById(R.id.list_item);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        header.setText(intent.getStringExtra("viewaall"));
        String viewall =intent.getStringExtra("viewaall");

        storeDetails = (ArrayList<StoresList.StoreDetails>) getIntent().getSerializableExtra("array");

//        if (viewall.equals("Promoted")){
//            mViewalldAdapter = new PViewallAdapter(PromotedViewallScreenActivity.this, HomeScreenFragment.promotedList, PromotedViewallScreenActivity.this);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PromotedViewallScreenActivity.this);
//            listview.setLayoutManager(mLayoutManager);
//            listview.setAdapter(mViewalldAdapter);
//    }
//        if (viewall.equals("Around")){
//            mViewalldAdapter = new PViewallAdapter(PromotedViewallScreenActivity.this, HomeScreenFragment.threekmList, PromotedViewallScreenActivity.this);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PromotedViewallScreenActivity.this);
//            listview.setLayoutManager(mLayoutManager);
//            listview.setAdapter(mViewalldAdapter);
//        }
//        if (viewall.equals("Popular")){
//            mViewalldAdapter = new PViewallAdapter(PromotedViewallScreenActivity.this, HomeScreenFragment.popularList, PromotedViewallScreenActivity.this);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PromotedViewallScreenActivity.this);
//            listview.setLayoutManager(mLayoutManager);
//            listview.setAdapter(mViewalldAdapter);
//        }
//        if (viewall.equals("Itsnew")){
//            mViewalldAdapter = new PViewallAdapter(PromotedViewallScreenActivity.this, HomeScreenFragment.itsnewList, PromotedViewallScreenActivity.this);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PromotedViewallScreenActivity.this);
//            listview.setLayoutManager(mLayoutManager);
//            listview.setAdapter(mViewalldAdapter);
//        }
//        if (viewall.equals("Banners") || viewall.equals("لافتات")){
        if (viewall.equals("Banners")){
            mBannerlistAdapter = new BannerslistAdapter(PromotedViewallScreenActivity.this, storeDetails, getIntent().getIntExtra("seletpos", 0), PromotedViewallScreenActivity.this, language);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PromotedViewallScreenActivity.this);
            listview.setLayoutManager(mLayoutManager);
            listview.setAdapter(mBannerlistAdapter);
        }

    }
}

