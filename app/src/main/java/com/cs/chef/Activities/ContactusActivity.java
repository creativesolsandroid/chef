package com.cs.chef.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cs.chef.R;

import java.util.List;

public class ContactusActivity extends AppCompatActivity {

    LinearLayout mail, twitter, snap_chat, whatsapp, vendor_registration, customer_care;
    ImageView back_btn;
    SharedPreferences languagePrefs;
    String language;
    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.contact_us);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.contact_us_arabic);
        }

        mail = (LinearLayout) findViewById(R.id.mail);
        twitter = (LinearLayout) findViewById(R.id.twitter);
        snap_chat = (LinearLayout) findViewById(R.id.snap_chat);
        whatsapp = (LinearLayout) findViewById(R.id.whatapp);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        vendor_registration = (LinearLayout)findViewById(R.id.vendor_registration);
        customer_care = (LinearLayout)findViewById(R.id.customer_care);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("plain/text");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@arab-space.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, "About The Chef App");
                    i.putExtra(Intent.EXTRA_TITLE  , "About The Chef App");
                    final PackageManager pm = getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;

                            if(className != null && !className.isEmpty()){
                                break;
                            }
                        }
                    }
                    i.setClassName("com.google.android.gm", className);
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(ContactusActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ContactusActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent fbIntent = new Intent(ContactusActivity.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    fbIntent.putExtra("title", "Twitter");
                } else {
                    fbIntent.putExtra("title", "تويتر");
                }
                fbIntent.putExtra("url", "https://www.twitter.com/thechefapp");
                startActivity(fbIntent);

            }
        });

        snap_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent fbIntent = new Intent(ContactusActivity.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    fbIntent.putExtra("title", "SnapChat");
                } else {
                    fbIntent.putExtra("title", "سناب شات ");
                }
                fbIntent.putExtra("url", "https://www.snapchat.com/add/thechefapp");
                startActivity(fbIntent);

            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String contact = "+966550364268"; // use country code with your phone number
                String url = "https://api.whatsapp.com/send?phone=" + contact;
                try {
                    PackageManager pm = getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ContactusActivity.this, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        });

        vendor_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent fbIntent = new Intent(ContactusActivity.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    fbIntent.putExtra("title", "Vendor Registration");
                } else {
                    fbIntent.putExtra("title", "لتسجيل معنا كشريك ");
                }
                fbIntent.putExtra("url", "https://docs.google.com/forms/d/e/1FAIpQLSeKLQNwPqqlzBaq4_G-IQtoD6SqJmbBqercA8VpftkAT_wFGg/viewform?usp=sf_link");
                startActivity(fbIntent);

            }
        });


        customer_care.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "920009685"));
                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    Activity#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for Activity#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"  + "920009685"));
                    startActivity(intent);
                }

            }
        });

    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ContactusActivity.this, perm));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "920009685"));
                    if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    Activity#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                }
                else {
                    Toast.makeText(ContactusActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

}
