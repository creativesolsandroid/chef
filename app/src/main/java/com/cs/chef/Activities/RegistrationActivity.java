package com.cs.chef.Activities;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.Dialogs.VerifyOtpDialog;
import com.cs.chef.Models.VerifyMobileResponse;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    EditText inputName, inputEmail, inputMobile, input_confirm_password, inputPassword;
    String strName, strEmail, strmobile, strConfirmPassword, strPassword;
    //    CheckBox checkBoxTerms;
    TextView buttonSignUp;
    LinearLayout textLogin;
//    ImageView imagePasswordEye;

    Context context;
    AlertDialog customDialog;
    private String serverOtp;
//    AlertDialog loaderDialog = null;

    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;
    private static final String TAG = "TAG";
    public static boolean isOTPVerified = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        inputName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email_id);
        inputMobile = (EditText) findViewById(R.id.mobile_number);
        input_confirm_password = (EditText) findViewById(R.id.confirm_password);
        inputPassword = (EditText) findViewById(R.id.password);

//        inputName.setText("Sudheer");
//        inputEmail.setText("sudheer@gmail.com");
//        inputMobile.setText("987654326");
//        inputPassword.setText("bg");
        inputMobile.setText(Constants.Country_Code);

//        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
//        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
//        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
//        inputLayoutPassword = (TextInputLayout) findViewById(R.id.layout_password);

//        checkBoxTerms = (CheckBox) findViewById(R.id.checkbox_terms);
        buttonSignUp = (TextView) findViewById(R.id.sign_up);
        textLogin = (LinearLayout) findViewById(R.id.login_layout);
//        imagePasswordEye = (ImageView) findViewById(R.id.singup_image_password);

        setFilters();
        setTypeface();
        requestSmsPersmissions();

//        textLogin.setPaintFlags(textLogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        buttonSignUp.setOnClickListener(this);
//        imagePasswordEye.setOnClickListener(this);

        inputName.addTextChangedListener(new TextWatcher(inputName));
        inputMobile.addTextChangedListener(new TextWatcher(inputMobile));
        input_confirm_password.addTextChangedListener(new TextWatcher(input_confirm_password));
        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up:
                if (validations()) {
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.password:
                if (inputPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                    inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    imagePasswordEye.setImageDrawable(getResources().getDrawable(R.drawable.signup_password_visible));
                } else {
                    inputPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    imagePasswordEye.setImageDrawable(getResources().getDrawable(R.drawable.signup_password_invisible));
                }
                inputPassword.setSelection(inputPassword.length());
                break;
        }
    }

    private void setTypeface() {
        inputName.setTypeface(Constants.getTypeFace(context));
        inputEmail.setTypeface(Constants.getTypeFace(context));
        input_confirm_password.setTypeface(Constants.getTypeFace(context));
        inputPassword.setTypeface(Constants.getTypeFace(context));
        inputMobile.setTypeface(Constants.getTypeFace(context));
//        checkBoxTerms.setTypeface(Constants.getTypeFace(context));
        buttonSignUp.setTypeface(Constants.getTypeFace(context));
//        textLogin.setTypeface(Constants.getTypeFace(context));
    }

    private void requestSmsPersmissions() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(SMS_RECIEVER, SMS_REQUEST);
            }
        }
    }

    private boolean validations() {
        strName = inputName.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        strConfirmPassword = input_confirm_password.getText().toString();
        strPassword = inputPassword.getText().toString();
        strmobile = inputMobile.getText().toString();
        strmobile = strmobile.replace("+966 ", "");

        if (strName.length() == 0) {
            inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            Constants.requestEditTextFocus(inputName, RegistrationActivity.this);
            return false;
        } else if (strEmail.length() == 0) {
            inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            Constants.requestEditTextFocus(inputEmail, RegistrationActivity.this);
            return false;
        } else if (!Constants.isValidEmail(strEmail)) {
            inputEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            Constants.requestEditTextFocus(inputEmail, RegistrationActivity.this);
            return false;
        } else if (strmobile.length() == 0){
            inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            Constants.requestEditTextFocus(inputMobile, RegistrationActivity.this);
            return false;
        } else if (strmobile.length() != 9){
            inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            Constants.requestEditTextFocus(inputMobile, RegistrationActivity.this);
            return false;
        } else if (strPassword.length() == 0) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputPassword, RegistrationActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            Constants.requestEditTextFocus(inputPassword, RegistrationActivity.this);
            return false;
        } else if (strConfirmPassword.length() == 0) {
            input_confirm_password.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(input_confirm_password, RegistrationActivity.this);
            return false;
        } else if (!strConfirmPassword.equals(strPassword)) {
            input_confirm_password.setError(getResources().getString(R.string.signup_msg_confirm_password));
            Constants.requestEditTextFocus(input_confirm_password, RegistrationActivity.this);
            return false;
        }else {
            return true;
        }
//        else if (!checkBoxTerms.isChecked()) {
//            Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_accept_terms),
//                    getResources().getString(R.string.alert_terms), getResources().getString(R.string.ok), RegistrationActivity.this);
//            return false;
//        }
    }

//    private void clearErrors() {
//        inputLayoutName.setErrorEnabled(false);
//        inputLayoutMobile.setErrorEnabled(false);
//        inputLayoutEmail.setErrorEnabled(false);
//        inputLayoutPassword.setErrorEnabled(false);
//    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.name:
                    if (editable.toString().startsWith(" ")) {
                        inputName.setText("");
                    }
//                    clearErrors();
                    break;
                case R.id.mobile_number:
                    String enteredMobile = editable.toString();
                    if (!enteredMobile.contains(Constants.Country_Code)) {
                        if (enteredMobile.length() > Constants.Country_Code.length()) {
                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                            inputMobile.setText(Constants.Country_Code + enteredMobile);
                        } else {
                            inputMobile.setText(Constants.Country_Code);
                        }
                        inputMobile.setSelection(inputMobile.length());
                    }
//                    clearErrors();
                    break;
                case R.id.email_id:
//                    clearErrors();
                    break;
                case R.id.password:
//                    clearErrors();
                    if (editable.length() > 20) {
                        inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.confirm_password:
//                    clearErrors();
                    if (editable.length() > 20) {
                        input_confirm_password.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

//    public void showloaderAlertDialog() {
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth * 0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(RegistrationActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(RegistrationActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RegistrationActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                serverOtp = verifyMobileResponse.getData().getOtp();
                                Log.i(TAG, "onResponse: " + serverOtp);
                                showVerifyDialog();
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), RegistrationActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(RegistrationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RegistrationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(RegistrationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RegistrationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void showVerifyDialog() {
        Bundle args = new Bundle();
        args.putString("name", strName);
        args.putString("email", strEmail);
        args.putString("mobile", strmobile);
        args.putString("password", strPassword);
        args.putString("otp", serverOtp);

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "register");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (newFragment != null) {
                    newFragment.dismiss();

                    if (!isOTPVerified) {
                        Constants.requestEditTextFocus(inputEmail, RegistrationActivity.this);
                        inputEmail.setSelection(inputEmail.length());
                    } else {
                        Toast.makeText(RegistrationActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                }
            }
        });
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Mobile",  strmobile);
            mobileObj.put("Email", strEmail);
            parentObj.put("VerifyMobile", mobileObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, " prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }


    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        title.setText(R.string.opt_msg_verify);
        yes.setText(getResources().getString(R.string.ok));
        no.setText(getResources().getString(R.string.edit));
        desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1) + strEmail + getResources().getString(R.string.signup_alert_mobile_verify2));

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(RegistrationActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.requestEditTextFocus(inputEmail, RegistrationActivity.this);
                inputEmail.setSelection(inputEmail.length());
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void setFilters() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
            }
        };

        inputName.setFilters(new InputFilter[]{filter});
    }
}
