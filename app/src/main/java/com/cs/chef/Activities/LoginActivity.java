package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.Models.UserRegistrationResponse;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    String language;
    Toolbar toolbar;
    Context context;
    TextView buttonSignIn;
    LinearLayout textRegister;
    //    ImageButton imagePasswordEye;
    SharedPreferences userPrefs;
    String strEmail, strPassword;
    SharedPreferences LanguagePrefs;
    EditText inputPassword;
    EditText inputEmail;
    SharedPreferences.Editor userPrefsEditor;
    TextView registerLayout, forgotPasswordLayout;
    //    TextInputLayout inputLayoutMobile, inputLayoutPassword;
    AlertDialog loaderDialog = null;
    ACProgressFlower dialog;
//        String inputStr;

    private static int SIGNUP_REQUEST = 1;
    private static int FORGOT_PASSWORD_REQUEST = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userPrefsEditor = userPrefs.edit();

        buttonSignIn = (TextView) findViewById(R.id.login_btn);
        textRegister = (LinearLayout) findViewById(R.id.sign_up_layout);
        inputEmail = (EditText) findViewById(R.id.email_id);
        inputPassword = (EditText) findViewById(R.id.password);
//        imagePasswordEye = (ImageButton) findViewById(R.id.signin_image_password);l;'
//        registerLayout = (LinearLayout) findViewById(R.id.signin_register_layout);
        forgotPasswordLayout = (TextView) findViewById(R.id.forget_password);

//        inputEmail.setText(Constants.Country_Code);

        setTypeface();

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userSignIn(v);

            }
        });

        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface() {
//        inputEmail.setTypeface(Constants.getTypeFace(context));
//        inputPassword.setTypeface(Constants.getTypeFace(context));
//        buttonSignIn.setTypeface(Constants.getTypeFace(context));
//        textRegister.setTypeface(Constants.getTypeFace(context));
    }

    public void userSignIn(View view) {
        if (validations()) {
            String networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                userLoginApi();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

//    public void launchSignUpActivity(View view) {
//        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
//        startActivityForResult(intent, SIGNUP_REQUEST);
//    }
//
//    public void launchForgotPasswordActivity(View view) {
//        Intent intent1 = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
//        startActivityForResult(intent1, FORGOT_PASSWORD_REQUEST);
//    }
//
//    public void tooglePasswordMode(View view) {
//        if (inputPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
//            inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//        } else {
//            inputPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
//        }
//        inputPassword.setSelection(inputPassword.length());
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private boolean validations() {
        strEmail = inputEmail.getText().toString();
        strPassword = inputPassword.getText().toString();
        strEmail = strEmail.replace("+966 ", "");

        if (strEmail.length() == 0) {
//            if (language.equalsIgnoreCase("En")) {
                inputEmail.setError("Please enter email");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                inputEmail.setError("من فضلك ادخل البريد الالكتروني");
//            }
        } else if (!strEmail.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}")) {
//            if (language.equalsIgnoreCase("En")) {
                inputEmail.setError("Please use email format (example - abc@abc.com");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                inputEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
//            }

        } else if (!Constants.isValidEmail(strEmail)) {
//            if (language.equalsIgnoreCase("En")) {
                inputEmail.setError("Please use email format (example - abc@abc.com");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                inputEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
//            }
        } else if (strPassword.length() == 0) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputPassword, LoginActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            Constants.requestEditTextFocus(inputPassword, LoginActivity.this);
            return false;
        }
        return true;
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
//                case R.id.signin_input_mobile:
//                    String enteredMobile = editable.toString();
//                    if (!enteredMobile.contains(Constants.Country_Code)) {
//                        if (enteredMobile.length() > Constants.Country_Code.length()) {
//                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
//                            inputEmail.setText(Constants.Country_Code + enteredMobile);
//                        } else {
//                            inputEmail.setText(Constants.Country_Code);
//                        }
//                        inputEmail.setSelection(inputEmail.length());
//                    }
//                    clearErrors();
//                    break;
                case R.id.password:
//                    clearErrors();
                    if (editable.length() > 20) {
                        inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

//    private void clearErrors() {
//        inputLayoutMobile.setErrorEnabled(false);
//        inputLayoutPassword.setErrorEnabled(false);
//    }

//    private class userLoginApi extends AsyncTask<String, Integer, String>{
//
//        ACProgressFlower dialog;
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareLoginJson();
//            dialog = new ACProgressFlower.Builder(LoginActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<UserRegistrationResponse> call = apiService.userLogin(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<UserRegistrationResponse>() {
//                @Override
//                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
//                    if(response.isSuccessful()){
//                        UserRegistrationResponse registrationResponse = response.body();
//                        try {
//                            if(registrationResponse.getStatus()){
//    //                          status true case
//                                String userId = registrationResponse.getData().getUserid();
//                                userPrefsEditor.putString("userId", userId);
//                                userPrefsEditor.putString("name", registrationResponse.getData().getName());
//                                userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
//                                userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
//                                userPrefsEditor.commit();
//                                Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
//                                setResult(RESULT_OK);
//                                finish();
//                            }
//                            else {
//    //                          status false case
//                                String failureResponse = registrationResponse.getMessage();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
//                                        getResources().getString(R.string.ok), LoginActivity.this);
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(LoginActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else{
//                        Toast.makeText(LoginActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        Toast.makeText(LoginActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//                    else {
//                        Toast.makeText(LoginActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(dialog != null){
//                dialog.dismiss();
//            }
//        }
//    }

//    public void showloaderAlertDialog() {
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth * 0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private void userLoginApi() {

        String inputStr = prepareLoginJson();
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(LoginActivity.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();;
//        dialog.show();
        Constants.showLoadingDialog(LoginActivity.this);
//        showloaderAlertDialog();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<UserRegistrationResponse> call = apiService.userLogin(
                RequestBody.create(MediaType.parse("application/json"), inputStr));

        call.enqueue(new Callback<UserRegistrationResponse>() {
            @Override
            public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
                if (response.isSuccessful()) {
                    UserRegistrationResponse registrationResponse = response.body();
                    try {
                        if (registrationResponse.getStatus()) {
                            //status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getName());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
                            userPrefsEditor.commit();
                            Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            //status false case
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = registrationResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), LoginActivity.this);
                            } else {
                                String failureResponse = registrationResponse.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), LoginActivity.this);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(LoginActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                Constants.closeLoadingDialog();
            }

            @Override
            public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(LoginActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                Log.i("TAG", "onFailure: " + t);

                Constants.closeLoadingDialog();
            }
        });
    }

    private String prepareLoginJson() {
        JSONObject parentObj = new JSONObject();
        JSONObject userDetailsObj = new JSONObject();
        JSONObject userAuthObj = new JSONObject();

        try {
            userDetailsObj.put("Mobile", "966" + strEmail);
            userDetailsObj.put("Password", strPassword);
            userAuthObj.put("DeviceToken", "-1");

            parentObj.put("UserDetails", userDetailsObj);
            parentObj.put("UserAuthActivity", userAuthObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", " prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }
}
