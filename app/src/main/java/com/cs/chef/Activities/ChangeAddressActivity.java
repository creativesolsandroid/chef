package com.cs.chef.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.cs.chef.Adapter.ChangeAddressAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.MyAddress;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeAddressActivity extends Activity implements View.OnClickListener{

    private ImageView searchAddress, backButton;
    private ArrayList<MyAddress.Data> myAddressArrayList = new ArrayList<>();
    private ChangeAddressAdapter mAddressAdapter;
    private ShimmerRecyclerView addressList;
    private TextView addressAlert;
    private RelativeLayout currentLocationLayout;
//    ACProgressFlower dialog;
    String userId;
    SharedPreferences userPrefs;
    private static String TAG = "TAG";
    public static int ADD_ADDRESS_INTENT = 2;
    public static int CONFIRMATION_INTENT = 3;

    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };

    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_change_address);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_change_address_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");

        backButton = (ImageView) findViewById(R.id.back_btn1);
        searchAddress = (ImageView) findViewById(R.id.search_address);
        addressAlert = (TextView) findViewById(R.id.no_address_alert);
        addressList = (ShimmerRecyclerView) findViewById(R.id.address_list);
        currentLocationLayout = (RelativeLayout) findViewById(R.id.current_location_layout);
        addressList.showShimmerAdapter();

        String networkStatus = NetworkUtil.getConnectivityStatusString(ChangeAddressActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new GetMyAddressApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

        searchAddress.setOnClickListener(this);
        backButton.setOnClickListener(this);
        currentLocationLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.search_address:
                openAutocompleteActivity();
//                finish();
                break;


            case R.id.back_btn1:
                finish();
                break;

            case R.id.current_location_layout:
                if (Constants.addressboolean){
                    Intent intent = new Intent(ChangeAddressActivity.this, AddAddressActivity.class);
                    startActivityForResult(intent, ADD_ADDRESS_INTENT);
                } else {
                    Constants.address_id = 0;
                    Intent intent = new Intent();
                    intent.putExtra("lat", 0);
                    intent.putExtra("longi", 0);
                    intent.putExtra("loc", "");
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
        }
    }

    private class GetMyAddressApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareDisplayProfileJSON();
//            dialog = new ACProgressFlower.Builder(ChangeAddressActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();

            Constants.showLoadingDialog(ChangeAddressActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<MyAddress> call = apiService.myAddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<MyAddress>() {
                @Override
                public void onResponse(Call<MyAddress> call, Response<MyAddress> response) {
                    if(response.isSuccessful()){
                        MyAddress myAddress = response.body();
                        if(myAddress.getStatus()){
                            myAddressArrayList = myAddress.getData();
//                            if(dialog != null){
//                                dialog.hide();
//                            }
                            if(myAddressArrayList.size() > 0) {
                                mAddressAdapter = new ChangeAddressAdapter(ChangeAddressActivity.this, myAddressArrayList, ChangeAddressActivity.this, language);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChangeAddressActivity.this);
                                addressList.setLayoutManager(mLayoutManager);
                                addressList.setAdapter(mAddressAdapter);
                                mAddressAdapter.notifyDataSetChanged();
                                addressAlert.setVisibility(View.GONE);
                            }
                            else {
                                addressAlert.setVisibility(View.VISIBLE);
                            }
                        }
                        else{
                            addressAlert.setVisibility(View.VISIBLE);
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = myAddress.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ChangeAddressActivity.this);
                            } else {
                                String failureResponse = myAddress.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), ChangeAddressActivity.this);
                            }
                        }
                    }
                    else{
                        addressAlert.setVisibility(View.VISIBLE);
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangeAddressActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangeAddressActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<MyAddress> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangeAddressActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangeAddressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangeAddressActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangeAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangeAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    addressAlert.setVisibility(View.VISIBLE);
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareDisplayProfileJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId",userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_ADDRESS_INTENT && resultCode == RESULT_OK){
//            Collections.reverse(myAddressArrayList);
//            myAddressArrayList.addAll((ArrayList<MyAddress.Data>)data.getSerializableExtra("newAddress"));
//            Collections.reverse(myAddressArrayList);
//            mAddressAdapter.notifyDataSetChanged();
//            addressList.scrollTo(0,0);
            String networkStatus = NetworkUtil.getConnectivityStatusString(ChangeAddressActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new GetMyAddressApi().execute();
            }
            else{

                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if(requestCode == REQUEST_CODE_AUTOCOMPLETE && resultCode == RESULT_OK){
            Place place = PlaceAutocomplete.getPlace(ChangeAddressActivity.this, data);

            // TODO call location based filter
            LatLng latLong;
            latLong = place.getLatLng();
            Log.d(TAG, "locatinckeck "+latLong);
            Intent intent = new Intent();
            intent.putExtra("lat", latLong.latitude);
            intent.putExtra("longi",  latLong.longitude);
            intent.putExtra("loc", "");
            setResult(RESULT_OK, intent);
            finish();
        }
        else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(ChangeAddressActivity.this, data);
            Log.d(TAG, "onActivityResult: ");

        } else if (requestCode == CONFIRMATION_INTENT && resultCode == RESULT_OK){



        }
    }

    @Override
    protected void onDestroy() {
        Constants.closeLoadingDialog();
        super.onDestroy();
    }
    private void openAutocompleteActivity() {
        Log.d(TAG, "openAutocompleteActivity: ");
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            e.printStackTrace();
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(ChangeAddressActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }


}
