package com.cs.chef.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.cs.chef.Firebase.Config;
import com.cs.chef.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Locale;

public class SplashScreenActivity extends AppCompatActivity {

    public static String regId = "";
    BroadcastReceiver mRegistrationBroadcastReceiver;
    String mLoginStatus;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    SharedPreferences LocationPrefe;
    SharedPreferences.Editor LocationPrefsEditor;
    String LocationStatus;
    private FirebaseAnalytics mFirebaseAnalytics;
    String userId, language;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        userId = userPrefs.getString("userId", "0");
//        if(languagePrefs.getBoolean("isAppLoadingFirstTime", true)) {
//            if (Locale.getDefault().getLanguage().equalsIgnoreCase("ar")) {
//                languagePrefsEditor.putString("language", "Ar");
//            }
//            languagePrefsEditor.putBoolean("isAppLoadingFirstTime", false);
//            languagePrefsEditor.commit();
//        }
//
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        LocationPrefe = getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
        LocationStatus = LocationPrefe.getString("Location_status", null);


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regId = pref.getString("regId", "-1");

                    Log.i("TAG", "Firebase reg id: " + regId);
                }
            }
        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", "-1");

        Log.i("TAG", "Firebase reg id: " + regId);

        String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";
        
        Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
        Crashlytics.setString(AppLanguage, language/* string value */);
        Crashlytics.setString("Device Token", regId);

        if (!userId.equals("0")) {
            Crashlytics.setString(UserId, userId/* string value */);
            Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
            Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
        }

        Crashlytics.getInstance();
//        Crashlytics.getInstance().crash();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
//                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                if (LocationStatus == null){

                    Intent i = new Intent(SplashScreenActivity.this, LocationActivity.class);
                    startActivity(i);

                } else {

                    Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                    i.putExtra("class","splash");
                    startActivity(i);
                }

//                } else {
//                    Intent i = new Intent(SplashScreenActivity.this, SignInActivity.class);
//                    startActivity(i);
//                }

                // close this activity
                finish();
            }
        }, 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
