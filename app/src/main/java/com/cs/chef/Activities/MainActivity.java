package com.cs.chef.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Fragments.AccountFragment;
import com.cs.chef.Fragments.HomeScreenFragment;
import com.cs.chef.Fragments.StoreSearchFragment;
import com.cs.chef.Models.Order;
import com.cs.chef.R;
import com.cs.chef.Utils.GPSTracker;
import com.cs.chef.Utils.OnBackPressed;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.cs.chef.Constants.currentLatitude;
import static com.cs.chef.Constants.currentLongitude;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int ACCOUNT_INTENT = 1;
    String userId;
    SharedPreferences userPrefs;
    ArrayList<Order> orders = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    public static BottomNavigationView navigation;
    String format;
    String language;
    SharedPreferences languagePrefs;

    SharedPreferences LanguagePrefs;
    SharedPreferences LocationPrefs;
    SharedPreferences.Editor LocationPrefsEditor;
    String location_status;
    String locality_name;

    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1;

    GPSTracker gps;
    public static int REQUEST_CODE_AUTOCOMPLETE = 1;

    private FusedLocationProviderClient mFusedLocationClient;

    SharedPreferences LocationPrefe;
    String LocationStatus;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressLint("ResourceType")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    Fragment mainFragment = new HomeScreenFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

                    item.setIcon(R.drawable.home_icon);
                    Menu menu1 = navigation.getMenu();

                    if (language.equalsIgnoreCase("En")) {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu1.getItem(2);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu1.getItem(2);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    } else {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu1.getItem(1);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu1.getItem(1);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    }
                    return true;
                case R.id.navigation_search:

                    Fragment searchFragment = new StoreSearchFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, searchFragment).commit();
                    Menu menu2 = navigation.getMenu();


                    if (language.equalsIgnoreCase("En")) {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu2.getItem(2);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu2.getItem(2);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    } else {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu2.getItem(1);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu2.getItem(1);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    }
                    return true;

                case R.id.navigation_cart:

                    item.setIcon(R.drawable.check_out);
                    Fragment checkoutFragment = new CheckOutActivity();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, checkoutFragment).commit();

                    Menu menu3 = navigation.getMenu();


                    if (language.equalsIgnoreCase("En")) {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu3.getItem(2);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu3.getItem(2);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    } else {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu3.getItem(1);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu3.getItem(1);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    }

                    Log.i("TAG", "onNavigationItemSelected: " + item.getItemId());

                    return true;
                case R.id.navigation_more:

                    item.setIcon(R.drawable.account);
                    userId = userPrefs.getString("userId", "0");
                    if (userId.equals("0")) {
                        Fragment accountFragment = new AccountSignInActivity();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();
                    } else {
                        Fragment accountFragment = new AccountFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();
                    }
                    Menu menu4 = navigation.getMenu();


                    if (language.equalsIgnoreCase("En")) {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu4.getItem(2);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu4.getItem(2);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    } else {
                        if (myDbHelper.getTotalOrderQty() == 0) {

                            MenuItem menuItem = menu4.getItem(1);
                            removeBadge(navigation, menuItem.getItemId());

                        } else {
                            MenuItem menuItem = menu4.getItem(1);
                            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                        }
                    }
                    return true;
            }
            return false;
        }
    };

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_arabic);
        }

        myDbHelper = new DataBaseHelper(MainActivity.this);

        orders = myDbHelper.getOrderInfo();


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        MenuItem item = findViewById(R.id.navigation_cart);
//        item.getItemId();

//        disableShiftMode(navigation);

//        LocationPrefe = getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
//        LocationStatus = LocationPrefe.getString("Location_status", null);
//
//        if (LocationStatus == null) {
//
//            Constants.showLoadingDialog(MainActivity.this);
//
//            int currentapiVersion = Build.VERSION.SDK_INT;
//            if (currentapiVersion >= Build.VERSION_CODES.M) {
//                if (!canAccessLocation()) {
//                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
//                } else {
//                    try {
//                        gps = new GPSTracker(MainActivity.this);
//                        getGPSCoordinates();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else {
//                try {
//                    gps = new GPSTracker(MainActivity.this);
//                    getGPSCoordinates();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        Menu menu = navigation.getMenu();

        if (language.equalsIgnoreCase("En")) {
            if (myDbHelper.getTotalOrderQty() == 0) {

                MenuItem menuItem = menu.getItem(2);
                removeBadge(navigation, menuItem.getItemId());

            } else {
                MenuItem menuItem = menu.getItem(2);
                showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
            }
        } else {
            if (myDbHelper.getTotalOrderQty() == 0) {

                MenuItem menuItem = menu.getItem(1);
                removeBadge(navigation, menuItem.getItemId());

            } else {
                MenuItem menuItem = menu.getItem(1);
                showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
            }
        }

        if (language.equalsIgnoreCase("En")) {

            if (getIntent().getStringExtra("class").equals("checkout")) {
                MenuItem menuItem = menu.getItem(2);
                navigation.setSelectedItemId(menuItem.getItemId());

                Fragment mainFragment = new CheckOutActivity();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            } else {

                Fragment mainFragment = new HomeScreenFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            }
        } else {

            if (getIntent().getStringExtra("class").equals("checkout")) {
                MenuItem menuItem = menu.getItem(1);
                navigation.setSelectedItemId(menuItem.getItemId());

                Fragment mainFragment = new CheckOutActivity();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            } else {
                MenuItem menuItem = menu.getItem(3);
                navigation.setSelectedItemId(menuItem.getItemId());
                Fragment mainFragment = new HomeScreenFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MainActivity.this, perm));
    }

    public void getGPSCoordinates() throws IOException {

        if (gps != null) {
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude();
                currentLongitude = gps.getLongitude();

                Log.i("TAG", "fused lat " + currentLatitude);
                Log.i("TAG", "fused long " + currentLongitude);

                try {

                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mFusedLocationClient.getLastLocation().addOnFailureListener(MainActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG", "fused lat null");
                        }
                    })
                            .addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        currentLatitude = location.getLatitude();
                                        currentLongitude = location.getLongitude();
                                        Log.i("TAG", "fused lat " + location.getLatitude());
                                        Log.i("TAG", "fused long " + location.getLongitude());
                                    } else {
                                        Log.i("TAG", "fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                location();

            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(MainActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(MainActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public void location() {

        if (currentLatitude != 0) {

            Constants.closeLoadingDialog();

            getLocality();

        } else {

            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                } else {
                    try {
                        gps = new GPSTracker(MainActivity.this);
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    gps = new GPSTracker(MainActivity.this);
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }

    public void getLocality() {
        Location mLocation = new Location("");
        mLocation.setLatitude(currentLatitude);
        mLocation.setLongitude(currentLongitude);

        Geocoder geocoder = null;
        try {
            geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Address found using the Geocoder.
        List<Address> addresses = null;
        String errorMessage = "";

        try {
            addresses = geocoder.getFromLocation(
                    mLocation.getLatitude(),
                    mLocation.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e("TAG", errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e("TAG", errorMessage + ". " +
                    "Latitude = " + mLocation.getLatitude() +
                    ", Longitude = " + mLocation.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e("TAG", errorMessage);
            }
        } else {
            Address address = addresses.get(0);

            if (address.getSubLocality() != null) {
                locality_name = address.getSubLocality();
            } else {
                locality_name = address.getLocality();
            }

            LocationPrefsEditor.putString("Location_status", "locationON");
            LocationPrefsEditor.putString("Latitute", String.valueOf(currentLatitude));
            LocationPrefsEditor.putString("Longitute", String.valueOf(currentLongitude));
            LocationPrefsEditor.putString("LocationName", locality_name);
            LocationPrefsEditor.commit();

            Intent i = new Intent(MainActivity.this, MainActivity.class);
            i.putExtra("class", "splash");
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCOUNT_INTENT && resultCode == RESULT_OK) {
//            Fragment accountFragment = new AccountFragment();
//            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
//            ft.commit();
        } else if (resultCode == RESULT_CANCELED) {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment currentFragment = getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 1);
        if (currentFragment instanceof OnBackPressed) {
            ((OnBackPressed) currentFragment).onBackPressed();
        }
    }

    // Method for disabling ShiftMode of BottomNavigationView
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.count_layout, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);
        text.setText(value);
        itemView.addView(badge);
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 2) {
            itemView.removeViewAt(1);
        }
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onResume() {
        super.onResume();

        Menu menu = navigation.getMenu();

        if (language.equalsIgnoreCase("En")) {
            if (myDbHelper.getTotalOrderQty() == 0) {

                MenuItem menuItem = menu.getItem(2);
                removeBadge(navigation, menuItem.getItemId());

            } else {
                MenuItem menuItem = menu.getItem(2);
                showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
            }
        } else {
            if (myDbHelper.getTotalOrderQty() == 0) {

                MenuItem menuItem = menu.getItem(1);
                removeBadge(navigation, menuItem.getItemId());

            } else {
                MenuItem menuItem = menu.getItem(1);
                showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
            }

        }

    }

}
