package com.cs.chef.hyperpay;


import com.cs.chef.Models.CardRegistration;
import com.cs.chef.Models.MyCards;

public interface CardRegistrationStatusRequestListener {

    void onErrorOccurred();
    void onCardRegistrationStatusReceived(CardRegistration paymentStatus);
}
