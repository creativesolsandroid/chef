package com.cs.chef.hyperpay;


public interface CheckoutIdRequestListener {

    void onCheckoutIdReceived(String checkoutId);
}
