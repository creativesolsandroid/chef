package com.cs.chef.hyperpay;


import com.cs.chef.Models.MyCards;

public interface PaymentStatusRequestListener {

    void onErrorOccurred();
    void onPaymentStatusReceived(MyCards paymentStatus);
}
