package com.cs.chef.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.cs.chef.Activities.ForgotPasswordActivity;
import com.cs.chef.Activities.SplashScreenActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.UserRegistrationResponse;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.cs.chef.Activity.ForgotPasswordActivity;

public class ResetPasswordDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextInputLayout inputLayoutPassword, inputLayoutConfirmPassword;
    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword;
    Button buttonSubmit, buttonVerify;
    String strMobile;
    View rootView;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String language;
    SharedPreferences languagePrefs;

    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.dialog_reset_password, container, false);
        } else {
            rootView = inflater.inflate(R.layout.dialog_reset_password_arabic, container, false);
        }
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getArguments().getString("mobile");

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputLayoutPassword = (TextInputLayout) rootView.findViewById(R.id.layout_new_password);
        inputLayoutConfirmPassword = (TextInputLayout) rootView.findViewById(R.id.layout_confirm_password);
        inputPassword = (EditText) rootView.findViewById(R.id.reset_input_password);
        inputConfirmPassword = (EditText) rootView.findViewById(R.id.reset_input_retype_password);

        buttonSubmit = (Button) rootView.findViewById(R.id.reset_submit_button);

        setTypeface();

        buttonSubmit.setOnClickListener(this);

        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica.ttf");

        inputPassword.setTypeface(typeface);
        inputConfirmPassword.setTypeface(typeface);
        buttonSubmit.setTypeface(typeface);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reset_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, getActivity());
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 8){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, getActivity());
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 8){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.reset_input_password:
                    clearErrors();
                    if(editable.length() > 8){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.reset_input_retype_password:
                    clearErrors();
                    if(editable.length() > 8){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject forgotPasswordObj = new JSONObject();

        try {
            forgotPasswordObj.put("Mobile","966"+strMobile);
            forgotPasswordObj.put("NewPassword",strPassword);
            parentObj.put("SetNewPassword", forgotPasswordObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }



    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {

//        ACProgressFlower dialog;
        String inputStr;
        AlertDialog loaderDialog = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UserRegistrationResponse> call = apiService.resetPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserRegistrationResponse>() {
                @Override
                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
                    if(response.isSuccessful()){
                        UserRegistrationResponse resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getUserid();
                                userPrefsEditor.putString("userId", userId);
                                userPrefsEditor.putString("name", resetPasswordResponse.getData().getName());
                                userPrefsEditor.putString("email", resetPasswordResponse.getData().getEmail());
                                userPrefsEditor.putString("mobile", resetPasswordResponse.getData().getMobile());
                                userPrefsEditor.commit();
                                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";

                                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
                                Crashlytics.setString(AppLanguage, language/* string value */);
                                Crashlytics.setString("Device Token", SplashScreenActivity.regId);

                                if (!userId.equals("0")) {
                                    Crashlytics.setString(UserId, userId/* string value */);
                                    Crashlytics.setString("Name", resetPasswordResponse.getData().getName()/* string value */);
                                    Crashlytics.setString("Mobile", resetPasswordResponse.getData().getMobile()/* string value */);
                                }

                                Crashlytics.getInstance();
                                if (language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getActivity(), R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.reset_success_msg_ar, Toast.LENGTH_SHORT).show();
                                }
                                ForgotPasswordActivity.isResetSuccessful = true;
                                getDialog().dismiss();
                            }
                            else {
//                                status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = resetPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
