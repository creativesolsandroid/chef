package com.cs.chef.Dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chef.Adapter.StoreDialogAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.Brands;
import com.cs.chef.Models.Filter;
import com.cs.chef.R;

import java.util.ArrayList;
import java.util.Collections;

public class StoresListDialog extends BottomSheetDialogFragment {

    TextView storename,summarytext ;
    ImageView storelogo;
    View rootView;;
    RecyclerView listView;
    private StoreDialogAdapter mstorediolgadapter;
    private RecyclerView Storelist;
    private ArrayList<Filter> mStorelist = new ArrayList<>();
    int pos = 0;

    String language;
    SharedPreferences languagePrefs;

    public static StoresListDialog newInstance() {
        return new StoresListDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.storesview_dialog, container, false);
        } else {
            rootView = inflater.inflate(R.layout.storesview_dialog, container, false);
        }
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        mStorelist = (ArrayList<Filter>) getArguments().getSerializable("array");
        pos = getArguments().getInt("pos", 0);

        listView = (RecyclerView)rootView.findViewById(R.id.stores_list_dialog) ;
        storename = (TextView) rootView.findViewById(R.id.storename);
        summarytext = (TextView) rootView.findViewById(R.id.subtext);
        storelogo =(ImageView)rootView.findViewById(R.id.storelogo);


        storename.setText(mStorelist.get(pos).getBradNameEn());
        Log.i("TAG", "onCreateView: " + mStorelist.get(pos).getBradNameEn());
        summarytext.setText(mStorelist.get(pos).getBrands().size()+" outlets near by you");


        Glide.with(getContext())
                .load(Constants.IMAGE_URL+mStorelist.get(pos).getBrandimageEn())
                .into(storelogo);

        Collections.sort(mStorelist.get(pos).getBrands(), Brands.distanceSort);
        mstorediolgadapter = new StoreDialogAdapter(getContext(), mStorelist, pos, getActivity(), language );
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        listView.setLayoutManager(layoutManager);
        listView.addItemDecoration(new DividerItemDecoration(listView.getContext(), DividerItemDecoration.HORIZONTAL));
//
//
        listView.setAdapter(mstorediolgadapter);
        return rootView;

    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica.ttf");

        storename.setTypeface(typeface);
        summarytext.setTypeface(typeface);

    }

    }

